'''
This script needs a pkl file of (all the) data measured for a vehicle and will analyse this in bulk
- plot NOx, NH3, Temp, velocity for all data
- plot emission factors for speed bin
- plot NOx, NH3, Temp, velocity for each trip

'''

import glob
import os
import time

import seaborn as sns
from path import Path
import matplotlib.pyplot as plt
import pandas as pd

from user_functions.func_download_one_session import download_one_session
from user_functions_analysis.func_plot_nox_nh3_over_all_time import plot_nox_nh3, plot_nox_nh3_ect
from user_functions_analysis.func_plot_efs_rde_sections import *

dict_limit = {
    'M1': 114.4,
    'N1 Cl2': 150.15,  # Caddy
    'N1 Cl3': 178.75  # Expert

}
# === Import data ==============================================================

mysettingssw = Path('../settings') / 'myreadonly.cnf'
project_name = 'sems-rde'

input_path = Path(f'../data/{project_name}/input')
out_path = Path(f'../data/{project_name}/output')
if not out_path.exists():
    raise Exception(f'Path {out_path} does not exist')

# the_file = 'VW_CA_VJG59N_EUR6_out_220321-090421.pkl'
# list_rde_sessions = [175753]
# NOX_LIMIT = dict_limit['N1 Cl2']
# title_vehicle = 'Volkswagen Caddy'

the_file = 'PE_EX_VJR86F_EUR6_out_150421-070521.pkl'
NOX_LIMIT = dict_limit['N1 Cl3']
list_rde_sessions = [176431]
title_vehicle = 'Peugeot Expert'

list_vehicle_file = glob.glob(input_path / the_file)

i_time = 300

col_time = 'Time [s]'
col_egt = 'after_cat_EGT'
col_nox = 'after_cat_NOx_mF'
col_nh3 = 'after_cat_NH3_mF'
col_ect = 'ECT'

list_pollutant_cols = [
    'after_cat_NH3_mF',
    'after_cat_NOx_mF'
]

list_error = []
list_out = []
list_comparison_out = []

for a_file in list_vehicle_file:
    file_input_path = a_file
    file_name = os.path.splitext(os.path.basename(file_input_path))[0]
    vehicle_file_prefix = file_name[:17]
    title_prefix = vehicle_file_prefix
    print(f'====== {title_prefix} =====')

    print('--- {}'.format(
        time.strftime("%H:%M:%S", time.localtime(time.time()))))

    df = pd.read_pickle(file_input_path)
    df.sort_values(by='datetime', inplace=True)
    df.reset_index(drop=True, inplace=True)
    df['timecount'] = df.index

    print('--- {}'.format(
        time.strftime("%H:%M:%S", time.localtime(time.time()))))

    # print(f'{file_name} - {df.session.unique()}')

    df['Time [h]'] = df['timecount'] / 3600

    # --- plot all data vs time and datetime -----------------------------------

    plot_nox_nh3(df, f'{vehicle_file_prefix} - All Data',
                 f'{file_name}_all',
                 'Time [h]', col_egt, col_ect, col_nox, col_nh3, list_pollutant_cols,
                 out_path, 1)

    plot_nox_nh3(df, f'{vehicle_file_prefix} - All Data',
                 f'{file_name}_all_datetime',
                 'datetime', col_egt, col_ect, col_nox, col_nh3, list_pollutant_cols,
                 out_path, 1)

    # --- look at RDE speed sections -------------------------------------------
    plot_efs_rde_sections_vs_normal_driving(df, list_rde_sessions,
                                            title_vehicle,
                                            '{}-rde_vs_normal'.format(file_name),
                                            out_path, NOX_LIMIT)
    plot_efs_rde_sections_vs_normal_driving_no_limits(df, list_rde_sessions,
                                                      title_vehicle,
                                                      '{}-rde_vs_normal'.format(file_name),
                                                      out_path, NOX_LIMIT)
    plot_efs_rde_sections_without_limit(df, 'SEMS RDE',
                                        f'{title_vehicle} - All Data',
                                        '{}-all'.format(file_name),
                                        out_path, 'C0')

    # --- plot all trips separately --------------------------------------------

    for i_session in list(df['session'].unique()):
        plt.close('all')

        df_temp = df.loc[df['session'] == i_session].copy()
        if (df_temp['velocity_filtered'].cumsum() / 3600).max() > 1:
            df_temp['Distance [km]'] = df_temp['velocity_filtered'].cumsum() / 3600
            df_temp['Time [s]'] = df_temp['timecount'] - df_temp.groupby('session')[
                'timecount'].transform('min')

            plot_nox_nh3_ect(df_temp,
                             '{} - {}'.format(vehicle_file_prefix,
                                              df_temp['session'].unique()[0]),
                             'trips/{}-{}'.format(vehicle_file_prefix,
                                                  df_temp['session'].unique()[0]),
                             'Time [s]', col_egt, col_ect, col_nox, col_nh3,
                             list_pollutant_cols, out_path, 1)
