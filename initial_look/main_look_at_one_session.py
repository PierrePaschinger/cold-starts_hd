import glob
import os
import time

import seaborn as sns
from path import Path
import matplotlib.pyplot as plt
import pandas as pd

from user_functions.func_download_one_session import download_one_session
from user_functions_analysis.func_plot_nox_nh3_over_all_time import plot_nox_nh3, plot_nox_nh3_ect
from user_functions_analysis.func_analyse_cold_start import analyse_cold_start
from user_functions_analysis.func_plot_efs_rde_sections import *
from user_functions_analysis.func_plot_vel_nox_cel import *

# === Import data ==============================================================

mysettingssw = Path('../settings') / 'myreadonly.cnf'
project_name = 'sems-rde'

input_path = Path(f'../data/{project_name}/input')
out_path = Path(f'../data/{project_name}/output')
if not out_path.exists():
    raise Exception(f'Path {out_path} does not exist')

dict_limit = {
    'M1': 114.4,
    'N1 Cl2': 150.15,  # Caddy
    'N1 Cl3': 178.75  # Expert

}

NOX_LIMIT = dict_limit['N1 Cl3']

# the_session = 'VW_CA_VJG59N_EUR6_1_2103231708'  # the RDE trip
# the_session = 'VW_CA_VJG59N_EUR6_1_2103251447'  # the cold start full load
# the_vehicle = ['VW_CA_VJG59N_EUR6']
the_session = 'PE_EX_VJR86F_EUR6_1_2104291548'  # the RDE trip
# the_session = 'PE_EX_VJR86F_EUR6_1_2105041425'  # the cold start full load
the_vehicle = ['PE_EX_VJR86F_EUR6']

vehicle_file_prefix = f'{the_vehicle[0]}'

col_egt = 'after_cat_EGT'
col_nox = 'after_cat_NOx_mF'
col_nh3 = 'after_cat_NH3_mF'
col_ect = 'ECT'

list_pollutant_cols = [
    'after_cat_NH3_mF',
    'after_cat_NOx_mF'
]

# === Start ===
list_out = []
list_comparison_out = []

df, list_error = download_one_session(the_vehicle, the_session, mysettingssw)

df['Time [s]'] = df['timecount'] - df.groupby('session')['timecount'].transform('min')
df['Distance [km]'] = df['velocity_filtered'].cumsum() / 3600
col_time = 'Time [s]'

# --- first plot what it's doing -----------------------------------------------

plot_vel_nox_cel_ect_egt(df, '{} - {}'.format(vehicle_file_prefix, df['session'].unique()[0]),
                 '{}-{}'.format(vehicle_file_prefix, df['session'].unique()[0]),
                         col_time, col_egt, col_ect, col_nox, out_path, )

plot_nox_nh3(df, '{} - {}'.format(vehicle_file_prefix, df['session'].unique()[0]),
             '{}-{}'.format(vehicle_file_prefix, df['session'].unique()[0]),
             col_time,
             col_egt, col_ect, col_nox, col_nh3, list_pollutant_cols, out_path, 1)
plot_nox_nh3_ect(df, '{} - {}'.format(vehicle_file_prefix, df['session'].unique()[0]),
                 '{}-{}'.format(vehicle_file_prefix, df['session'].unique()[0]),
                 col_time,
                 col_egt, col_ect, col_nox, col_nh3, list_pollutant_cols, out_path, 1)

# # --- zoom in on first 8 km to look at potential cold starts ---
df_1st_8km = df.loc[df['Distance [km]'] < 8].copy()
plot_nox_nh3_ect(df_1st_8km,
                 '{} - {} - 1st 8 km'.format(vehicle_file_prefix, df['session'].unique()[0]),
                 '{}-{}-1st8km'.format(vehicle_file_prefix, df['session'].unique()[0]),
                 col_time, col_egt, col_ect, col_nox, col_nh3, list_pollutant_cols, out_path, 1)

# --- Look at average emissions per RDE speed section/road type ----------------
#
plot_efs_rde_sections(df, 'SEMS RDE',
                      '{} - {}'.format(vehicle_file_prefix, df['session'].unique()[0]),
                      '{}-{}'.format(vehicle_file_prefix, df['session'].unique()[0]), out_path,
                      NOX_LIMIT, 'grey')
plot_efs_rde_sections_without_limit(df, 'SEMS RDE',
                                    '{} - {}'.format(vehicle_file_prefix,
                                                     df['session'].unique()[0]),
                                    '{}-{}'.format(vehicle_file_prefix, df['session'].unique()[0]),
                                    out_path, 'grey')
plot_efs_rde_sections_without_limit(df, 'SEMS RDE',
                                    '{} - Valid RDE trip'.format(vehicle_file_prefix,
                                                                 df['session'].unique()[0]),
                                    '{}-{}'.format(vehicle_file_prefix, df['session'].unique()[0]),
                                    out_path, 'grey')

analyse_cold_start(df, 'velocity_filtered', col_time, vehicle_file_prefix, '',
                   list_out, list_comparison_out, list_error, col_ect,
                   list_pollutant_cols, out_path)

analyse_cold_start(df_1st_8km, 'velocity_filtered', col_time, vehicle_file_prefix, '1st 8 km',
                   list_out, list_comparison_out, list_error, col_ect,
                   list_pollutant_cols, out_path)

print('stop')
