import pandas as pd
import numpy as np
from matplotlib import pyplot as plt
from user_functions_analysis import func_xymatrix as xymatrix

df_raw= pd.read_csv('../data/sems-rde/input/setdata_RE_TA_SF805V_EUR6.csv')
df_raw = df_raw.sort_values('datetime')

df_raw['newtime'] = pd.to_datetime(df_raw['datetime'], unit = 'ms')
df_raw['vel_filtered_mps'] = df_raw['velocity_filtered']*1000/3600 # convert velocity from km/h to m/s
df_raw['NOx_per_km']=np.divide(df_raw['after_cat_NOx_mF'],df_raw['vel_filtered_mps'])
df_raw['NOx_per_CO2'] = np.divide(df_raw['after_cat_NOx_mF'],df_raw['CO2_mF'])
df_raw['NOx_per_km'] = df_raw['NOx_per_km'].replace([np.inf,-np.inf],np.nan)
df_raw['dist_cumsum_km'] = df_raw['vel_filtered_mps'].cumsum()/1000 # distance by cumulative sum of vel_filter[m/s]
df_raw['acceleration_mps'] = df_raw['acceleration_filtered'].fillna(0)

df = df_raw.copy()
# First filter: remove 0 RPMS and only when engine is on
df = df.loc[(df['RPM']!=0) & (df['engine_on']==1)]

# second filter: drop NaN values
df = df.replace([np.inf,-np.inf],np.nan).dropna(subset=['after_cat_O2','after_cat_NOx',
                                    'CO2_mF','after_cat_NOx_mF','after_cat_O2_mF',
                                    'RPM','ECT','AAT','velocity_filtered'])

# third filter: only consider when when sensor is ready
df = df.loc[(df['after_cat_rdyNOx']==1) & (df['after_cat_rdyO2']==1)]

### Bin defintions
vel_bins = np.arange(0,180,5)
acc_bins = np.arange(-2,2,0.05)
AAT_bins = np.arange(-10,40,1)
EGT_bins = np.arange(0,525,10)
NOx_bins = np.arange(0,3100,10)
NOx_mF_bins = np.arange(0.1,10,0.1)
O2_bins = np.arange(-1,20.8,0.2)
CO2_bins = np.arange(0,15,1)
fuel_bins = np.arange(0,4,0.1)
NOxperCO2_bins = np.arange(0,10,0.2)
vapos_bins = np.arange(0,50,1)

df_O2_bins = df.groupby(np.digitize(df['after_cat_O2_uncalib'],O2_bins), as_index=True)
df_vel_bins = df.groupby(np.digitize(df['velocity_filtered'],vel_bins),as_index=True)



weights_df_uncalib = np.ones_like(df['after_cat_NOx_uncalib'])/len(df)
weights_df = np.ones_like(df['after_cat_NOx_mF'])/len(df)

#=== xymatrix plot - individual nox/co2 = f(velocity,CO2)
y_sig_CO2 = 'CO2_mF' ## use filtered acceleration instead? TODO vh
x_sig_CO2 = 'velocity_filtered'
z_sig_CO2 = 'NOx_per_CO2'
min_y_CO2 = 0
max_y_CO2 = np.nanmax(df['CO2_mF'])
min_x_CO2 = 0
max_x_CO2 = 180
y_scale_CO2 = 0.2
x_scale_CO2 = 5
N= 2

y_bins_CO2 = int((max_x_CO2-min_x_CO2) /float(x_scale_CO2))
x_bins_CO2 = int((max_y_CO2-min_y_CO2) /float(y_scale_CO2)) 
x_axis_CO2 = np.linspace(min_y_CO2,max_y_CO2,y_bins_CO2)
y_axis_CO2 = np.linspace(min_x_CO2,max_x_CO2,x_bins_CO2)

z_var_CO2,count_var_CO2 ,*dummy= xymatrix.getmatrix(df, z_sig_CO2, x_sig_CO2, y_sig_CO2, min_x_CO2,max_x_CO2, min_y_CO2, max_y_CO2, x_bins_CO2, y_bins_CO2, N)
z_var_smooth_CO2, count_var_smooth_CO2 = xymatrix.getsmoothedmatrix(z_var_CO2, count_var_CO2, x_bins_CO2, y_bins_CO2, N)

z1_sig_CO2 = 'after_cat_NOx_mF'
z2_sig_CO2 = 'CO2_mF' 

z1_var_CO2,count1_var_CO2,*dummy = xymatrix.getmatrix(df, z1_sig_CO2, x_sig_CO2, y_sig_CO2, min_x_CO2,max_x_CO2, min_y_CO2, max_y_CO2, x_bins_CO2, y_bins_CO2, N)
z1_var_smooth_CO2, count1_var_smooth_CO2 = xymatrix.getsmoothedmatrix(z1_var_CO2, count1_var_CO2, x_bins_CO2, y_bins_CO2, N)

z2_var_CO2,count2_var_CO2,*dummy = xymatrix.getmatrix(df, z2_sig_CO2, x_sig_CO2, y_sig_CO2, min_x_CO2,max_x_CO2, min_y_CO2, max_y_CO2, x_bins_CO2, y_bins_CO2, N)
z2_var_smooth_CO2, count2_var_smooth_CO2 = xymatrix.getsmoothedmatrix(z2_var_CO2, count2_var_CO2, x_bins_CO2, y_bins_CO2, N)

z1z2_var_smooth_CO2, count12_var_smooth_CO2 = xymatrix.getsmoothedmatrix(np.divide(z1_var_CO2,z2_var_CO2), count1_var_CO2,  x_bins_CO2, y_bins_CO2, N)


# clippig nox
df_clippingNOx = df.loc[df['after_cat_NOx_uncalib']>=2990]

# O2 Threshold
O2_threshold = -0.3

# split data to O2 <=threshold and O2>threshold
df_O2_low = df.loc[df['after_cat_O2_uncalib']<= O2_threshold]
df_O2_high = df.loc[df['after_cat_O2_uncalib']> O2_threshold]

## only take rising DPF > 320 deg, 30 seconds windo
condition_DPF = df['after_cat_EGT']>320
idx_DPF = df.index.get_indexer_for(df[condition_DPF].index)
n_rows_DPF = 15
df_temp= df.iloc[np.unique(np.concatenate([np.arange(max(i-n_rows_DPF,0), min(i+n_rows_DPF+1, len(df)))
                                            for i in idx_DPF]))]
df_DPFregen = df.copy()
choice_DPF = df.isin(df_temp)
df_DPFregen[~choice_DPF] =np.nan

## 1 minute average
df_ave_1min = df.groupby(np.arange(len(df))//60).mean()
df_first_1min = df.groupby(np.arange(len(df))//60).first()
df_DPFregen_ave_1min = df_DPFregen.groupby(np.arange(len(df))//60).mean()

## 1 minute average
df_ave_1hr = df.groupby(np.arange(len(df))//3600).mean()
df_first_1hr = df.groupby(np.arange(len(df))//3600).first()

df_raw_ave_1hr = df_raw.groupby(np.arange(len(df_raw))//3600).mean()
df_raw_first_1hr = df_raw.groupby(np.arange(len(df_raw))//3600).first()


df_DPFregen_ave_1hr = df_DPFregen.groupby(np.arange(len(df))//3600).mean()


# Monthly mean
df_monthly_sum = df[['after_cat_NOx_uncalib','after_cat_NOx_mF','velocity_filtered','AAT']].groupby(df['newtime'].dt.to_period('M')).sum()
df_monthly_mean = df[['after_cat_NOx_uncalib','after_cat_NOx_mF','velocity_filtered','AAT']].groupby(df['newtime'].dt.to_period('M')).mean()
df_monthly_count = df[['after_cat_NOx_uncalib','after_cat_NOx_mF','velocity_filtered','AAT']].groupby(df['newtime'].dt.to_period('M')).count()
df_monthly_mean['NOx_per_km'] = np.divide(df_monthly_sum['after_cat_NOx_mF'], df_monthly_sum['velocity_filtered']/3600) # mg/m --> mg/km
df_monthly_mean['NOx_ppm_per_km'] = np.divide(df_monthly_sum['after_cat_NOx_uncalib'], df_monthly_sum['velocity_filtered']/3600) # mg/m --> mg/km
df_monthly_size = df[['after_cat_NOx_mF','velocity_filtered','AAT']].groupby(df['newtime'].dt.to_period('M')).size()

df_O2_high_monthly_sum = df_O2_high[['after_cat_NOx_uncalib','after_cat_NOx_mF','velocity_filtered']].groupby(df_O2_high['newtime'].dt.to_period('M')).sum()
df_O2_high_monthly_mean = df_O2_high[['after_cat_NOx_uncalib','after_cat_NOx_mF','velocity_filtered']].groupby(df_O2_high['newtime'].dt.to_period('M')).mean()

df_monthly_mean['NOx_per_km_nonegO2'] = np.divide(df_O2_high_monthly_sum['after_cat_NOx_mF'], df_O2_high_monthly_sum['velocity_filtered']/3600) # mg/m --> mg/km
df_monthly_mean['NOx_ppm_per_km_nonegO2'] = np.divide(df_O2_high_monthly_sum['after_cat_NOx_uncalib'], df_O2_high_monthly_sum['velocity_filtered']/3600) # mg/m --> mg/km

df_monthly_mean['after_cat_NOx_uncalib_nonegO2'] = df_O2_high_monthly_mean['after_cat_NOx_uncalib'].copy()

######Plots
## Plot nox flow and o2
plt.rcParams.update({'font.size': 14})
plt.figure()
ax2=plt.subplot(311)
plt.plot(df_ave_1min['velocity_filtered'])
plt.ylabel('Vehicle Speed [km/h]')
plt.subplot(312,sharex=ax2)
plt.plot(df_ave_1min['after_cat_NOx_uncalib'])
plt.ylabel('Raw NOx signal [ppm]')
plt.subplot(313,sharex=ax2)
plt.plot(df_ave_1min['after_cat_O2_uncalib'])
plt.ylabel('Raw O2 signal [%]')
plt.xlabel('Driving time [min]')
plt.rcParams.update({'font.size': 12})

## Nox = f (vel,co2)
plt.rcParams.update({'font.size': 14})
xymatrix.plot(np.divide(z1_var_smooth_CO2,count_var_smooth_CO2), 
              min_x_CO2, max_x_CO2, min_y_CO2, max_y_CO2,1e-1,250, 
              'Vehicle Speed [km/h]','CO2 emission [g/s]' ,'average NOx emission [mg/s]','log')

## nox/co2 vs co2
plt.rcParams.update({'font.size': 14})
xymatrix.plot(np.divide(z1_var_smooth_CO2,z2_var_smooth_CO2), 
              min_x_CO2, max_x_CO2, min_y_CO2, max_y_CO2,1,40, 
             'Vehicle Speed [km/h]','CO2 emission [g/s]' ,'average NOX/CO2 ratio [mg/g]','log')



# plot of distance vs driving time
plt.rcParams.update({'font.size': 14})
plt.figure()
plt.plot(df_raw_first_1hr['newtime'],df_raw_first_1hr['dist_cumsum_km'])
plt.ylabel('Driving distance [km]')
plt.xlabel('Timestamp [-]')
plt.rcParams.update({'font.size': 12})

# Average NOx concentration [ppm] vs binned O2
plt.rcParams.update({'font.size': 14})
f_0,ax_0 = plt.subplots(nrows=1,ncols=1)
f_0.set_size_inches((19.2 ,  9.49))
ax_0.hist(df['after_cat_O2_uncalib'],bins = O2_bins, alpha=0.5,weights=weights_df_uncalib)
ax_0.set_xlabel('O2 Tailpipe [%]')
ax_0.set_ylabel('Normalized frequency [-]')
ax_ab = ax_0.twinx()
ax_ab.plot(df_O2_bins['after_cat_O2_uncalib'].mean(),df_O2_bins['after_cat_NOx_uncalib'].mean())
ax_ab.set_ylabel('Average NOx concentration [ppm]')
ax_ab.set_ylim(0,3050)
plt.tight_layout()
plt.rcParams.update({'font.size': 12})

###
# Average NOx flow [mg/s] vs binned O2
plt.rcParams.update({'font.size': 14})
f_0,ax_0 = plt.subplots(nrows=1,ncols=1)
f_0.set_size_inches((19.2 ,  9.49))
ax_0.hist(df['after_cat_O2_uncalib'],bins = O2_bins, alpha=0.5,weights=weights_df_uncalib)
ax_0.set_xlabel('Raw O2 tailpipe [%]')
ax_0.set_ylabel('Normalized frequency [-]')
ax_ab = ax_0.twinx()
ax_ab.plot(df_O2_bins['after_cat_O2_uncalib'].mean(),df_O2_bins['after_cat_NOx_mF'].mean())
ax_ab.set_ylabel('Average NOx emission[mg/s]')
ax_ab.set_ylim(0,100)
plt.tight_layout()
plt.rcParams.update({'font.size': 12})

## Montly average
plt.rcParams.update({'font.size': 14})
plt.figure(figsize=[19.2 ,  9.49])
ax=plt.subplot(111)
df_monthly_mean['NOx_per_km'].plot(kind='bar',ax=ax,color='C0')
ax.set_xlabel('Time [-]')
ax.set_ylabel('Average NOx emissions [mg/km]')
plt.rcParams.update({'font.size': 12})

# Plot monthly calc
plt.rcParams.update({'font.size': 14})
plt.figure()
ax = plt.subplot(111)
df_monthly_mean[['NOx_per_km','NOx_per_km_nonegO2']].plot(kind='bar',ax=ax)
ax.set_ylabel('Monthly average NOx emission [mg/km]')
ax.set_xlabel('Monthly time stamp')
ax.legend(['all data','data with O2>'+str(O2_threshold)+'%'])
plt.rcParams.update({'font.size': 12})

## Binned example
plt.rcParams.update({'font.size': 14})
f_0,ax_0 = plt.subplots(nrows=1,ncols=1)
f_0.set_size_inches((19.2 ,  9.49))
ax_0.hist(df['velocity_filtered'],bins = vel_bins, alpha=0.5,weights=weights_df)
ax_0.set_xlabel('Vehicle Speed[km/h]')
ax_0.set_ylabel('Normalized frequency [-]')
ax_ab = ax_0.twinx()
ax_ab.plot(df_vel_bins['velocity_filtered'].mean(),
           df_vel_bins['after_cat_NOx_mF'].sum()/(df_vel_bins['velocity_filtered'].sum()/3600))
ax_ab.set_ylabel('Average NOx emission [mg/km]')
ax_ab.set_ylim(0,1200)
plt.tight_layout()
plt.rcParams.update({'font.size': 12})


# Plot monthly calc ppm
plt.rcParams.update({'font.size': 14})
plt.figure()
ax = plt.subplot(111)
df_monthly_mean[['after_cat_NOx_uncalib','after_cat_NOx_uncalib_nonegO2']].plot(kind='bar',ax=ax)
ax.set_ylabel('Monthly average NOx emission [ppm]')
ax.set_xlabel('Monthly time stamp')
ax.legend(['all data','data with O2>'+str(O2_threshold)+'%'])
plt.rcParams.update({'font.size': 12})


plt.rcParams.update({'font.size': 14})
plt.figure()
ax = plt.subplot(111)
df_monthly_mean[['NOx_ppm_per_km','NOx_ppm_per_km_nonegO2']].plot(kind='bar',ax=ax)
ax.set_ylabel('Monthly average NOx emission [ppm/km]')
ax.set_xlabel('Monthly time stamp')
ax.legend(['all data','data with O2>'+str(O2_threshold)+'%'])
plt.rcParams.update({'font.size': 12})

# Plot O2 negative
plt.figure()
plt.plot(df['after_cat_O2_uncalib'],df['after_cat_NOx_uncalib'],'o', alpha= 0.1)
plt.xlim([-2,2])
plt.xlabel('Raw O2 [%]')
plt.ylabel('Raw NOx [ppm]')
plt.title('Raw emission data from Renault Talisman')

# plot clipping Nox
plt.figure()
plt.hist(df_clippingNOx['after_cat_O2_uncalib'],bins=O2_bins)
plt.xlabel('Raw O2 bins [%]')
plt.ylabel('Frequency [-]')
plt.title('Distribution of raw O2 when raw NOx >= 2990 ppm')
plt.xlim([-1.5,10])