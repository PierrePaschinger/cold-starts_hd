import pandas as pd
from matplotlib import pyplot as plt
from user_functions_analysis import func_xymatrix as xymatrix
import numpy as np

plt.rcParams.update({'font.size': 12})

df_raw_V800DH = pd.read_csv('../data/sems-rde/input/setdata_VW_CA_V800DH_EUR6.csv')
df_raw = pd.read_csv('../data/sems-rde/input/setdata_VW_CA_V152NG_EUR6_May28.csv')

df_raw =df_raw.sort_values('datetime')
df_raw['newtime'] = pd.to_datetime(df_raw['datetime'], unit = 'ms')
df_raw['vel_filtered_mps'] = df_raw['velocity_filtered']*1000/3600 # convert velocity from km/h to m/s
df_raw['NOx_per_CO2'] = np.divide(df_raw['after_cat_NOx_mF'],df_raw['CO2_mF'])
df_raw['dist_cumsum_km'] = df_raw['vel_filtered_mps'].cumsum()/1000 # distance by cumulative sum of vel_filter[m/s]
df_raw['acceleration_mps'] = df_raw['acceleration_filtered'].fillna(0)


df_raw_V800DH =df_raw_V800DH.sort_values('datetime')
df_raw_V800DH['newtime'] = pd.to_datetime(df_raw_V800DH['datetime'], unit = 'ms')
df_raw_V800DH['vel_filtered_mps'] = df_raw_V800DH['velocity_filtered']*1000/3600 # convert velocity from km/h to m/s
df_raw_V800DH['NOx_per_CO2'] = np.divide(df_raw_V800DH['after_cat_NOx_mF'],df_raw_V800DH['CO2_mF'])
df_raw_V800DH['dist_cumsum_km'] = df_raw_V800DH['vel_filtered_mps'].cumsum()/1000 # distance by cumulative sum of vel_filter[m/s]
df_raw_V800DH['acceleration_mps'] = df_raw_V800DH['acceleration_filtered'].fillna(0)

df = df_raw.copy()
df_V800DH = df_raw_V800DH.copy()

# First filter: remove 0 RPMS and only when engine is on
df = df.loc[(df['RPM']!=0) & (df['engine_on']==1)]
df_V800DH = df_V800DH.loc[(df_V800DH['RPM']!=0) & (df_V800DH['engine_on']==1)]

# second filter: drop NaN values
df = df.replace([np.inf,-np.inf],np.nan).dropna(subset=['after_cat_O2','after_cat_NOx',
                                    'CO2_mF','after_cat_NOx_mF','after_cat_O2_mF',
                                    'RPM','ECT','AAT','velocity_filtered'])

df_V800DH = df_V800DH.replace([np.inf,-np.inf],np.nan).dropna(subset=['after_cat_O2','after_cat_NOx',
                                    'CO2_mF','after_cat_NOx_mF','after_cat_O2_mF',
                                    'RPM','ECT','AAT','velocity_filtered'])

# third filter: only consider when when sensor is ready
df = df.loc[(df['after_cat_rdyNOx']==1) & (df['after_cat_rdyO2']==1)]
df_V800DH = df_V800DH.loc[(df_V800DH['after_cat_rdyNOx']==1) & (df_V800DH['after_cat_rdyO2']==1)]

## Make a new subset during RDE test
df_RDE = df.loc[(df['newtime']>=pd.to_datetime('2018-07-04')) &\
                (df['newtime']<=pd.to_datetime('2018-07-07'))]

# no acceleration
df_no_acc = df.loc[df['acceleration_filtered'].abs()<=0.5]

# filterd around DPF regeneration
condition = df['after_cat_EGT']>=320
idx = df.index.get_indexer_for(df[condition].index)
n_rows= 15 #how many data around the spikes are included
df_temp= df.iloc[np.unique(np.concatenate([np.arange(max(i-n_rows,0), min(i+n_rows+1, len(df)))
                                            for i in idx]))]
df_DPFregen_window = df.copy()
choice = df.isin(df_temp)
df_DPFregen_window[~choice] =np.nan

# Normal usage without DPF regen
df_normal = df.loc[df['after_cat_EGT']<320]

df_DPFregen = df.loc[df['after_cat_EGT']>=320]

df_DPFregen_series = df.copy()
df_DPFregen_series[df_DPFregen_series['after_cat_EGT']<320]= np.nan

# 280 < EGT < 350
df_EGT_1 = df.loc[(df['after_cat_EGT']>280)&(df['after_cat_EGT']<=350)]

# EGT >= 350
df_EGT_2 = df.loc[(df['after_cat_EGT']>=350)]

# NOX >= 150 ppm
df_NOx150ppm = df.loc[df['after_cat_NOx']>=150]


# NOX >= 150 ppm urban - cold and hot
df_NOx150ppm_urban = df.loc[(df['after_cat_NOx']>=150)&
                      (df['velocity_filtered']>=20)&
                      (df['velocity_filtered']<=40)]

df_NOx150ppm_urban_cold = df.loc[(df['after_cat_NOx']>=150)&
                      (df['velocity_filtered']>=20)&
                      (df['velocity_filtered']<=40)&
                      (df['after_cat_EGT']<320)]

df_NOx150ppm_urban_hot = df.loc[(df['after_cat_NOx']>=150)&
                      (df['velocity_filtered']>=20)&
                      (df['velocity_filtered']<=40)&
                      (df['after_cat_EGT']>=320)]

# NOx >=150 ppm motorway - cold and hot
df_NOx150ppm_motorway = df.loc[(df['after_cat_NOx']>=150)&
                      (df['velocity_filtered']>=90)&
                      (df['velocity_filtered']<=135)]

df_NOx150ppm_motorway_cold = df.loc[(df['after_cat_NOx']>=150)&
                      (df['velocity_filtered']>=90)&
                      (df['velocity_filtered']<=135)&
                      (df['after_cat_EGT']<320)]

df_NOx150ppm_motorway_hot = df.loc[(df['after_cat_NOx']>=150)&
                      (df['velocity_filtered']>=95)&
                      (df['velocity_filtered']<=135)&
                      (df['after_cat_EGT']>=320)]
# heatwave AAT >=30
df_heatwave = df.loc[(df['AAT']>=30)]

# heatwave AAT>= 30 and NOx>1.8mg/s
df_heatwave_NOx2mgps = df.loc[(df['AAT']>=30)&(df['after_cat_NOx_mF']>=1.8)]


# urban
df_urban = df.loc[df['velocity_filtered']<=55]
df_urban_no_acc = df.loc[(df['velocity_filtered']<=55)&(df['acceleration_filtered'].abs()<=0.5)]

### Bin defintions
vel_bins = np.arange(0,180,5)
acc_bins = np.arange(-2,2,0.05)
AAT_bins = np.arange(-10,40,2)
EGT_bins = np.arange(0,525,10)
NOx_bins = np.arange(0,3100,10)
NOx_mF_bins = np.arange(0,200,0.1)
O2_bins = np.arange(0,21,1)
CO2_bins = np.arange(0,15,1)
fuel_bins = np.arange(0,4,0.1)
NOxperCO2_bins = np.arange(0,10,0.2)
vapos_bins = np.arange(0,50,1)

df_vel_bins = df.groupby(np.digitize(df['velocity_filtered'],vel_bins), as_index=True)
df_no_acc_vel_bins = df_no_acc.groupby(np.digitize(df_no_acc['velocity_filtered'],vel_bins), as_index=True)
df_vapos_bins = df.groupby(np.digitize(df['vapos'],vapos_bins), as_index=True)
df_AAT_bins = df.groupby(np.digitize(df['AAT'],AAT_bins), as_index=True)
df_EGT_bins = df.groupby(np.digitize(df['after_cat_EGT'],EGT_bins), as_index=True)
df_fuel_bins = df.groupby(np.digitize(df['fuel_mF'],fuel_bins), as_index=True)

df_DPFregen_window_vel_bins = df_DPFregen_window.groupby(np.digitize(df_DPFregen_window['velocity_filtered'],vel_bins), as_index=True)
df_DPFregen_window_acc_bins = df_DPFregen_window.groupby(np.digitize(df_DPFregen_window['acceleration_filtered'],acc_bins), as_index=True)
df_DPFregen_window_AAT_bins = df_DPFregen_window.groupby(np.digitize(df_DPFregen_window['AAT'],AAT_bins), as_index=True)
df_DPFregen_window_EGT_bins = df_DPFregen_window.groupby(np.digitize(df_DPFregen_window['after_cat_EGT'],EGT_bins), as_index=True)
df_DPFregen_window_fuel_bins = df_DPFregen_window.groupby(np.digitize(df_DPFregen_window['fuel_mF'],fuel_bins), as_index=True)

df_NOx150ppm_vel_bins = df_NOx150ppm.groupby(np.digitize(df_NOx150ppm['velocity_filtered'],vel_bins), as_index=True)
df_NOx150ppm_acc_bins = df_NOx150ppm.groupby(np.digitize(df_NOx150ppm['acceleration_filtered'],acc_bins), as_index=True)
df_NOx150ppm_AAT_bins = df_NOx150ppm.groupby(np.digitize(df_NOx150ppm['AAT'],AAT_bins), as_index=True)
df_NOx150ppm_EGT_bins = df_NOx150ppm.groupby(np.digitize(df_NOx150ppm['after_cat_EGT'],EGT_bins), as_index=True)
df_NOx150ppm_fuel_bins = df_NOx150ppm.groupby(np.digitize(df_NOx150ppm['fuel_mF'],fuel_bins), as_index=True)

df_RDE_vel_bins = df_RDE.groupby(np.digitize(df_RDE['velocity_filtered'],vel_bins), as_index=True)
df_RDE_acc_bins = df_RDE.groupby(np.digitize(df_RDE['acceleration_filtered'],acc_bins), as_index=True)
df_RDE_AAT_bins = df_RDE.groupby(np.digitize(df_RDE['AAT'],AAT_bins), as_index=True)
df_RDE_EGT_bins = df_RDE.groupby(np.digitize(df_RDE['after_cat_EGT'],EGT_bins), as_index=True)
df_RDE_fuel_bins = df_RDE.groupby(np.digitize(df_RDE['fuel_mF'],fuel_bins), as_index=True)

## weights for histogram
weights_df = np.ones_like(df['after_cat_NOx_mF'])/len(df)
weights_df_no_acc = np.ones_like(df_no_acc['after_cat_NOx_mF'])/len(df_no_acc)
weights_df_normal=np.ones_like(df_normal['after_cat_NOx_mF'])/len(df_normal)
weights_df_DPFregen=np.ones_like(df_DPFregen['after_cat_NOx_mF'])/len(df_DPFregen)
weights_df_V800DH=np.ones_like(df_V800DH['after_cat_NOx_mF'])/len(df_V800DH)
weights_df_RDE=np.ones_like(df_RDE['after_cat_NOx_mF'])/len(df_RDE)


#=== xymatrix plot - nox/co2 = f(velocity,acc)
y_sig = 'acceleration_filtered' ## use filtered acceleration instead? TODO vh
x_sig = 'velocity_filtered'
z_sig = 'after_cat_NOx_mF'
min_y = -4
max_y = 4
min_x = 0
max_x = 180
y_scale = 0.2
x_scale = 5
N= 2

y_bins = int((max_x-min_x) /float(x_scale))
x_bins = int((max_y-min_y) /float(y_scale)) 
x_axis = np.linspace(min_y,max_y,y_bins)
y_axis = np.linspace(min_x,max_x,x_bins)

z_var,count_var,*dump = xymatrix.getmatrix(df, z_sig, x_sig, y_sig, min_x,max_x, min_y, max_y, x_bins,y_bins, N)
z_var_smooth, count_var_smooth = xymatrix.getsmoothedmatrix(z_var, count_var, x_bins,y_bins, N)

z1_sig = 'after_cat_NOx_mF'
z2_sig = 'CO2_mF' 

z1_var,count1_var,*dump = xymatrix.getmatrix(df, z1_sig, x_sig, y_sig, min_x,max_x, min_y, max_y, x_bins,y_bins, N)
z1_var_smooth, count1_var_smooth = xymatrix.getsmoothedmatrix(z1_var, count1_var,x_bins,y_bins, N)

z2_var,count2_var,*dump = xymatrix.getmatrix(df, z2_sig, x_sig, y_sig, min_x,max_x, min_y, max_y, x_bins,y_bins, N)
z2_var_smooth, count2_var_smooth = xymatrix.getsmoothedmatrix(z2_var, count2_var,  x_bins,y_bins, N)

z1z2_var_smooth, count12_var_smooth = xymatrix.getsmoothedmatrix(np.divide(z1_var,z2_var), count1_var, x_bins,y_bins, N)


#=== xymatrix plot - individual nox/co2 = f(velocity,CO2)
y_sig_CO2 = 'CO2_mF' ## use filtered acceleration instead? TODO vh
x_sig_CO2 = 'velocity_filtered'
z_sig_CO2 = 'NOx_per_CO2'
min_y_CO2 = 0
max_y_CO2 = np.nanmax(df['CO2_mF'])
min_x_CO2 = 0
max_x_CO2 = 180
y_scale_CO2 = 0.2
x_scale_CO2 = 5
N= 2

y_bins_CO2 = int((max_x_CO2-min_x_CO2) /float(x_scale_CO2))
x_bins_CO2 = int((max_y_CO2-min_y_CO2) /float(y_scale_CO2)) 
x_axis_CO2 = np.linspace(min_y_CO2,max_y_CO2,y_bins_CO2)
y_axis_CO2 = np.linspace(min_x_CO2,max_x_CO2,x_bins_CO2)

z_var_CO2,count_var_CO2,*dump = xymatrix.getmatrix(df, z_sig_CO2, x_sig_CO2, y_sig_CO2, min_x_CO2,max_x_CO2, min_y_CO2, max_y_CO2, x_bins_CO2,y_bins_CO2, N)
z_var_smooth_CO2, count_var_smooth_CO2 = xymatrix.getsmoothedmatrix(z_var_CO2, count_var_CO2, x_bins_CO2,y_bins_CO2, N)

z1_sig_CO2 = 'after_cat_NOx_mF'
z2_sig_CO2 = 'CO2_mF' 

z1_var_CO2,count1_var_CO2,*dump = xymatrix.getmatrix(df, z1_sig_CO2, x_sig_CO2, y_sig_CO2, min_x_CO2,max_x_CO2, min_y_CO2, max_y_CO2, x_bins_CO2,y_bins_CO2, N)
z1_var_smooth_CO2, count1_var_smooth_CO2 = xymatrix.getsmoothedmatrix(z1_var_CO2, count1_var_CO2, x_bins_CO2,y_bins_CO2, N)

z2_var_CO2,count2_var_CO2,*dump = xymatrix.getmatrix(df, z2_sig_CO2, x_sig_CO2, y_sig_CO2, min_x_CO2,max_x_CO2, min_y_CO2, max_y_CO2, x_bins_CO2,y_bins_CO2, N)
z2_var_smooth_CO2, count2_var_smooth_CO2 = xymatrix.getsmoothedmatrix(z2_var_CO2, count2_var_CO2, x_bins_CO2,y_bins_CO2, N)

z1z2_var_smooth_CO2, count12_var_smooth_CO2 = xymatrix.getsmoothedmatrix(np.divide(z1_var_CO2,z2_var_CO2), count1_var_CO2, x_bins_CO2,y_bins_CO2, N)

#=== xymatrix plot - nox = f(velocity,EGT)
y_sig_EGT = 'after_cat_EGT' ## use filtered acceleration instead? TODO vh
x_sig_EGT = 'velocity_filtered'
z_sig_EGT = 'after_cat_NOx_mF'
min_y_EGT =0
max_y_EGT = np.nanmax(df['after_cat_EGT'])
min_x_EGT = 0
max_x_EGT = 180
y_scale_EGT = 10
x_scale_EGT = 5
N= 2

y_bins_EGT = int((max_x_EGT-min_x_EGT) /float(x_scale_EGT))
x_bins_EGT = int((max_y_EGT-min_y_EGT) /float(y_scale_EGT)) 
x_axis_EGT = np.linspace(min_y_EGT,max_y_EGT,y_bins_EGT)
y_axis_EGT = np.linspace(min_x_EGT,max_x_EGT,x_bins_EGT)

z_var_EGT,count_var_EGT,*dump = xymatrix.getmatrix(df, z_sig_EGT, x_sig_EGT, y_sig_EGT, min_x_EGT,max_x_EGT, min_y_EGT, max_y_EGT, x_bins_EGT,y_bins_EGT, N)
z_var_smooth_EGT, count_var_smooth_EGT = xymatrix.getsmoothedmatrix(z_var_EGT, count_var_EGT, x_bins_EGT,y_bins_EGT, N)

#=== 1 hour averae
df_ave_1hr = df.groupby(np.arange(len(df))//3600).mean()
df_first_1hr= df.groupby(np.arange(len(df))//3600).first()

df_raw_ave_1hr = df_raw.groupby(np.arange(len(df_raw))//3600).mean()
df_raw_first_1hr= df_raw.groupby(np.arange(len(df_raw))//3600).first()

df_DPFregen_window_ave_1hr = df_DPFregen_window.groupby(np.arange(len(df_DPFregen_window))//3600).mean()
df_DPFregen_window_first_1hr= df_DPFregen_window.groupby(np.arange(len(df_DPFregen_window))//3600).first()

##----  1 minute average
df_ave_1min = df.groupby(np.arange(len(df))//60).mean()
df_first_1min= df.groupby(np.arange(len(df))//60).first()

df_DPFregen_window_ave_1min = df_DPFregen_window.groupby(np.arange(len(df_DPFregen_window))//60).mean()
df_DPFregen_window_first_1min= df_DPFregen_window.groupby(np.arange(len(df_DPFregen_window))//60).first()

df_DPFregen_ave_1min = df_DPFregen.groupby(np.arange(len(df_DPFregen))//60).mean()
df_DPFregen_first_1min= df_DPFregen.groupby(np.arange(len(df_DPFregen))//60).first()

df_DPFregen_series_ave_1min = df_DPFregen_series.groupby(np.arange(len(df_DPFregen_series))//60).mean()
df_DPFregen_series_first_1min= df_DPFregen_series.groupby(np.arange(len(df_DPFregen_series))//60).first()

df_heatwave_ave_1min = df_heatwave.groupby(np.arange(len(df_heatwave))//60).mean()
df_heatwave_first_1min= df_heatwave.groupby(np.arange(len(df_heatwave))//60).first()

## Monthly average
df_monthly_sum = df[['after_cat_NOx_mF','velocity_filtered']].groupby(df['newtime'].dt.to_period('M')).sum()
df_monthly_mean = df[['after_cat_NOx_mF','velocity_filtered']].groupby(df['newtime'].dt.to_period('M')).mean()
df_monthly_mean['NOx_per_km'] = np.divide(df_monthly_sum['after_cat_NOx_mF'], df_monthly_sum['velocity_filtered']/3600) # mg/m --> mg/km
df_monthly_count = df[['after_cat_NOx_mF','velocity_filtered']].groupby(df['newtime'].dt.to_period('M')).count()

df_RDE_monthly_sum = df_RDE[['after_cat_NOx_mF','velocity_filtered']].groupby(df_RDE['newtime'].dt.to_period('M')).sum()
df_RDE_monthly_mean = df_RDE[['after_cat_NOx_mF','velocity_filtered']].groupby(df_RDE['newtime'].dt.to_period('M')).mean()
df_monthly_mean['RDE_NOx_per_km'] = np.divide(df_RDE_monthly_sum['after_cat_NOx_mF'], df_RDE_monthly_sum['velocity_filtered']/3600) # mg/m --> mg/km

## PLOTS
# plot of distance vs driving time
plt.figure()
plt.plot(df_raw_first_1hr['newtime'],df_raw_first_1hr['dist_cumsum_km'])
plt.ylabel('Driving distance [km]')
plt.xlabel('Timestamp [-]')

# plot of distance vs driving time
plt.figure()
plt.plot(df_raw_first_1hr['newtime'],df_raw_first_1hr.index.values)
plt.ylabel('Driving hours [hr]')
plt.xlabel('Timestamp [-]')

## nox/co2 vs acceleration
xymatrix.plot(np.divide(z1_var_smooth,z2_var_smooth), 
              min_x, max_x, min_y, max_y,1e-2,5,
              'Vehicle Speed [km/h]','Acceleration [m/s2]' ,'average NOx/CO2 ratio [mg/g]','log')

## nox/co2 vs co2
xymatrix.plot(np.divide(z1_var_smooth_CO2,z2_var_smooth_CO2), 
              min_x_CO2, max_x_CO2, min_y_CO2, max_y_CO2,1e-2,5, 
              'Vehicle Speed [km/h]','CO2 emission [g/s]' ,'average NOX/CO2 ratio [mg/g]','log')

## Nox = f (vel,co2)
xymatrix.plot(np.divide(z1_var_CO2,count_var_CO2), 
              min_x_CO2, max_x_CO2, min_y_CO2, max_y_CO2,1e-2,50,
              'Vehicle Speed [km/h]','CO2 emission [g/s]' ,'average NOx emission [mg/s]','log')


## Nox = f (vel,co2)
xymatrix.plot(np.divide(z1_var_smooth_CO2,count_var_smooth_CO2), 
              min_x_CO2, max_x_CO2, min_y_CO2, max_y_CO2,1e-2,50,
              'Vehicle Speed [km/h]','CO2 emission [g/s]' ,'average NOx emission [mg/s]','log')

## Nox = f (vel,EGT)
xymatrix.plot(np.divide(z_var_smooth_EGT,count_var_smooth_EGT), 
              min_x_EGT, max_x_EGT, min_y_EGT, max_y_EGT,1e-2,50,
              'Vehicle Speed [km/h]','Exhaust Gas temp [deg C]' ,'average NOx emission [mg/s]','log')

xymatrix.plot(count_var_smooth_EGT, 
              min_x_EGT, max_x_EGT, min_y_EGT, max_y_EGT,0.1,2e4,
              'Vehicle Speed [km/h]','Exhaust Gas temp [deg C]' ,'count[-]','log')


####################################
# RDE vs monitoring - AAT
plt.rcParams.update({'font.size': 14})
plt.figure(figsize=[19.2 ,  9.49])
plt.subplot(2,1,1)
plt.plot(df_ave_1hr['after_cat_NOx_mF'])
plt.axvline(x=df_ave_1hr.loc[df_first_1hr['newtime']>='2018-07-04'].index.values[0],color='r',linestyle='--')
plt.axvline(x=df_ave_1hr.loc[df_first_1hr['newtime']>='2018-07-07'].index.values[0],color='r',linestyle='--')
plt.ylabel('Hourly average NOx emission[mg/s]')
plt.subplot(2,1,2)
plt.plot(df_ave_1hr['AAT'])
plt.ylabel('Hourly average ambient temp. [deg C]')
plt.xlabel('Driving time [hr]')
plt.axvline(x=df_ave_1hr.loc[df_first_1hr['newtime']>='2018-07-04'].index.values[0],color='r',linestyle='--')
plt.axvline(x=df_ave_1hr.loc[df_first_1hr['newtime']>='2018-07-07'].index.values[0],color='r',linestyle='--')
plt.tight_layout()
plt.rcParams.update({'font.size': 12})

# RDE vs monitoring - DPF
plt.rcParams.update({'font.size': 14})
plt.figure(figsize=[19.2 ,  9.49])
plt.subplot(2,1,1)
plt.plot(df_ave_1min['after_cat_NOx_mF'])
plt.axvline(x=df_ave_1min.loc[df_first_1min['newtime']>='2018-07-04'].index.values[0],color='r',linestyle='--')
plt.axvline(x=df_ave_1min.loc[df_first_1min['newtime']>='2018-07-07'].index.values[0],color='r',linestyle='--')
plt.ylabel('Minutely average NOx emission[mg/s]')
plt.subplot(2,1,2)
plt.plot(df_ave_1min['after_cat_EGT'])
plt.ylabel('Minutely average exhaust temp. [deg C]')
plt.xlabel('Driving time [min]')
plt.axvline(x=df_ave_1min.loc[df_first_1min['newtime']>='2018-07-04'].index.values[0],color='r',linestyle='--')
plt.axvline(x=df_ave_1min.loc[df_first_1min['newtime']>='2018-07-07'].index.values[0],color='r',linestyle='--')
plt.tight_layout()
plt.rcParams.update({'font.size': 12})

## Montly average
plt.rcParams.update({'font.size': 14})
plt.figure(figsize=[19.2 ,  9.49])
ax=plt.subplot(111)
df_monthly_mean[['NOx_per_km','RDE_NOx_per_km']].plot(kind='bar',ax=ax)
ax.legend(['Monitoring data','RDE test'])
ax.set_xlabel('Time [-]')
ax.set_ylabel('Average NOx emissions [mg/km]')
plt.rcParams.update({'font.size': 12})
plt.tight_layout()


## Binned example
plt.rcParams.update({'font.size': 14})
f_0,ax_0 = plt.subplots(nrows=1,ncols=1)
f_0.set_size_inches((19.2 ,  9.49))
ax_0.hist(df['velocity_filtered'],bins = vel_bins, alpha=0.5,weights=weights_df)
ax_0.set_xlabel('Vehicle Speed[km/h]')
ax_0.set_ylabel('Normalized frequency [-]')
ax_ab = ax_0.twinx()
ax_ab.plot(df_vel_bins['velocity_filtered'].mean(),
           df_vel_bins['after_cat_NOx_mF'].sum()/(df_vel_bins['velocity_filtered'].sum()/3600))
ax_ab.set_ylabel('Average NOx emission [mg/km]')
ax_ab.set_ylim(0,120)
plt.tight_layout()
plt.rcParams.update({'font.size': 12})


## data vs no acceleration [2 subplots]
plt.rcParams.update({'font.size': 14})
f_a,ax_a = plt.subplots(nrows=2,ncols=1)
f_a.set_size_inches((19.2 ,  9.49))
ax_a[0].hist(df['velocity_filtered'],bins = vel_bins, alpha=0.3,weights=weights_df)
ax_a[0].hist(df_no_acc['velocity_filtered'],bins = vel_bins, alpha=0.3,weights=weights_df_no_acc)
ax_a[0].set_xlabel('Vehicle speed [km/h]')
ax_a[0].set_ylabel('Normalized frequency [-]')
ax_ab = ax_a[0].twinx()
ax_ab.plot(df_vel_bins['velocity_filtered'].mean(),df_vel_bins['after_cat_NOx_mF'].mean())
ax_ab.plot(df_no_acc_vel_bins['velocity_filtered'].mean(), df_no_acc_vel_bins['after_cat_NOx_mF'].mean())
ax_ab.set_ylabel('Average NOx emission [mg/s]')
ax_ab.set_ylim(0,15)

ax_a[1].hist(df['velocity_filtered'],bins = vel_bins, alpha=0.3,weights=weights_df)
ax_a[1].hist(df_no_acc['velocity_filtered'],bins = vel_bins, alpha=0.3,weights=weights_df_no_acc)
ax_a[1].set_xlabel('Vehicle speed [km/h]')
ax_a[1].set_ylabel('Normalized frequency [-]')
ax_abc = ax_a[1].twinx()
ax_abc.plot(df_vel_bins['velocity_filtered'].mean(),
           df_vel_bins['after_cat_NOx_mF'].sum()/(df_vel_bins['velocity_filtered'].sum()/3600))
ax_abc.plot(df_no_acc_vel_bins['velocity_filtered'].mean(),
           df_no_acc_vel_bins['after_cat_NOx_mF'].sum()/(df_no_acc_vel_bins['velocity_filtered'].sum()/3600))
ax_abc.set_ylabel('Average NOx emission [mg/km]')
ax_abc.set_ylim(0,120)
plt.tight_layout()
plt.rcParams.update({'font.size': 12})
plt.legend(['All data','no acceleration'])

## data vs no acc
plt.rcParams.update({'font.size': 14})
f_a,ax_aa = plt.subplots(nrows=1,ncols=1)
ax_aa.hist(df['velocity_filtered'],bins = vel_bins, alpha=0.3,weights=weights_df)
ax_aa.hist(df_no_acc['velocity_filtered'],bins = vel_bins, alpha=0.3,weights=weights_df_no_acc)
ax_aa.set_xlabel('Vehicle speed [km/h]')
ax_aa.set_ylabel('Normalized frequency [-]')
ax_abc = ax_aa.twinx()
ax_abc.plot(df_vel_bins['velocity_filtered'].mean(),
           df_vel_bins['after_cat_NOx_mF'].sum()/(df_vel_bins['velocity_filtered'].sum()/3600))
ax_abc.plot(df_no_acc_vel_bins['velocity_filtered'].mean(),
           df_no_acc_vel_bins['after_cat_NOx_mF'].sum()/(df_no_acc_vel_bins['velocity_filtered'].sum()/3600))
ax_abc.set_ylabel('Average NOx emission [mg/km]')
ax_abc.set_ylim(0,120)
plt.tight_layout()
plt.rcParams.update({'font.size': 12})
plt.legend(['All data','no acceleration'])


## Binned nox vs AAT
plt.rcParams.update({'font.size': 14})
f_b,ax_b = plt.subplots(nrows=1,ncols=1)
ax_b.hist(df['AAT'],bins = AAT_bins, alpha=0.3,weights=weights_df)
ax_b.hist(df_RDE['AAT'],bins = AAT_bins, alpha=0.3,weights=weights_df_RDE)
ax_b.set_xlabel('Ambient temperature [deg C]')
ax_b.set_ylabel('Normalized Frequency [-]')
ax_b.set_xlim(-5,40)
ax_ab_2 = ax_b.twinx()
ax_ab_2.plot(df_AAT_bins['AAT'].mean(),df_AAT_bins['after_cat_NOx_mF'].mean())
ax_ab_2.plot(df_RDE_AAT_bins['AAT'].mean(),df_RDE_AAT_bins['after_cat_NOx_mF'].mean())
ax_ab_2.set_ylabel('Average NOx flow [mg/s]')
ax_ab_2.set_ylim(0,5)
plt.legend(['monitoring','RDE'])
plt.rcParams.update({'font.size': 12})

## Histogram v152ng rde vs v152ng monitoring
plt.rcParams.update({'font.size': 14})
plt.figure()
plt.subplot(2,2,1)
plt.hist(df['velocity_filtered'],bins= vel_bins,weights=weights_df,alpha=0.5)
plt.hist(df_RDE['velocity_filtered'],bins= vel_bins,weights=weights_df_RDE,alpha=0.5)
#plt.hist(df_V800DH['velocity_filtered'],bins= vel_bins,density=True,alpha=0.3)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('Vehicle speed [km/h]')
plt.subplot(2,2,2)
plt.hist(df['AAT'],bins= AAT_bins,weights=weights_df,alpha=0.5)
plt.hist(df_RDE['AAT'],bins= AAT_bins,weights=weights_df_RDE,alpha=0.5)
#plt.hist(df_V800DH['AAT'],bins= AAT_bins,density=True,alpha=0.3)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('Ambient Temp. [deg C]')
plt.subplot(2,2,3)
plt.hist(df['vapos'],bins= vapos_bins,weights=weights_df,alpha=0.5)
plt.hist(df_RDE['vapos'],bins= vapos_bins,weights=weights_df_RDE,alpha=0.5)
#plt.hist(df_V800DH['vapos'],bins= vapos_bins,density=True,alpha=0.3)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('v.a+[m2/s3]')
plt.xlim([0,40])
plt.subplot(2,2,4)
plt.hist(df['after_cat_EGT'],bins= EGT_bins,weights=weights_df,alpha=0.5)
plt.hist(df_RDE['after_cat_EGT'],bins= EGT_bins,weights=weights_df_RDE,alpha=0.5)
#plt.hist(df_V800DH['after_cat_EGT'],bins= EGT_bins,density=True,alpha=0.3)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('EGT [deg C]')
plt.legend(['monitoring','RDE'])
plt.rcParams.update({'font.size': 12})

# Histogram v800dh rde vs v152ng rde
plt.rcParams.update({'font.size': 14})
plt.figure()
plt.subplot(2,2,1)
plt.hist(df_V800DH['velocity_filtered'],bins= vel_bins,weights=weights_df_V800DH,alpha=0.5)
plt.hist(df_RDE['velocity_filtered'],bins= vel_bins,weights=weights_df_RDE,alpha=0.5)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('Vehicle speed [km/h]')
plt.subplot(2,2,2)
plt.hist(df_V800DH['AAT'],bins= AAT_bins,weights=weights_df_V800DH,alpha=0.5)
plt.hist(df_RDE['AAT'],bins= AAT_bins,weights=weights_df_RDE,alpha=0.5)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('Ambient Temp. [deg C]')
plt.subplot(2,2,3)
plt.hist(df_V800DH['vapos'],bins= vapos_bins,weights=weights_df_V800DH,alpha=0.5)
plt.hist(df_RDE['vapos'],bins= vapos_bins,weights=weights_df_RDE,alpha=0.5)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('vapos[m2/s3]')
plt.xlim([0,40])
plt.subplot(2,2,4)
plt.hist(df_V800DH['after_cat_EGT'],bins= EGT_bins,weights=weights_df_V800DH,alpha=0.5)
plt.hist(df_RDE['after_cat_EGT'],bins= EGT_bins,weights=weights_df_RDE,alpha=0.5)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('EGT [deg C]')
plt.legend(['RDE - V-800-DH','RDE - V-152-NG'])
plt.rcParams.update({'font.size': 12})



plt.rcParams.update({'font.size': 14})
plt.figure()
plt.subplot(2,2,1)
plt.hist(df_normal['velocity_filtered'],bins= vel_bins,weights=np.ones_like(df_normal['velocity_filtered'])/len(df_normal),alpha=0.5)
plt.hist(df_DPFregen['velocity_filtered'],bins= vel_bins,weights=np.ones_like(df_DPFregen['velocity_filtered'])/len(df_DPFregen),alpha=0.5)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('Vehicle speed [km/h]')
plt.subplot(2,2,2)
plt.hist(df_normal['after_cat_NOx_mF'],bins=NOx_mF_bins,weights=np.ones_like(df_normal['after_cat_NOx_mF'])/len(df_normal),alpha=0.5)
plt.hist(df_DPFregen['after_cat_NOx_mF'],bins= NOx_mF_bins,weights=np.ones_like(df_DPFregen['after_cat_NOx_mF'])/len(df_DPFregen),alpha=0.5)
plt.xlim([0,5])
plt.ylabel('Normalized frequency [-]')
plt.xlabel('NOx emission flow [mg/s]')
plt.subplot(2,2,3)
plt.hist(df_normal['vapos'],bins= vapos_bins,weights=np.ones_like(df_normal['vapos'])/len(df_normal),alpha=0.5)
plt.hist(df_DPFregen['vapos'],bins= vapos_bins,weights=np.ones_like(df_DPFregen['vapos'])/len(df_DPFregen),alpha=0.5)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('vapos[m2/s3]')
plt.xlim([0,40])
plt.subplot(2,2,4)
plt.hist(df_normal['after_cat_EGT'],bins= EGT_bins,weights=np.ones_like(df_normal['after_cat_EGT'])/len(df_normal),alpha=0.5)
plt.hist(df_DPFregen['after_cat_EGT'],bins= EGT_bins,weights=np.ones_like(df_DPFregen['after_cat_EGT'])/len(df_DPFregen),alpha=0.5)
plt.ylabel('Normalized frequency [-]')
plt.xlabel('EGT [deg C]')
plt.legend(['EGT <320 deg C','EGT >=320 deg C'])
plt.rcParams.update({'font.size': 12})