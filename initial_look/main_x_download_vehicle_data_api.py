"""
Download data from api -> modified to match sql layout
jessica.deruiter@tno.nl
"""

import getopt
import os
import sys
import time
import pandas as pd

import sqlalchemy
import matplotlib.pyplot as plt
from sqlalchemy.engine.url import URL
from pathlib import Path

from user_functions.func_OSRM_postprocessing import *
from user_functions.func_download_data_via_sql import *
from user_functions.config import MySettings
from user_functions.func_download_data_via_api import *


def main(vehicles, mysettingsfile, out_path, rawcolumns, proccolumns, calccolumns,
         limit_dates=None, overwrite=0):
    if limit_dates is None:
        limit_dates = []
    print(f'###################################################################\n'
          f'START: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')

    list_errors = []

    # === read settings file ===================================================
    my_settings = MySettings(mysettingsfile)
    db = my_settings.dbschema
    engine, db_sess = make_database_session_connection(my_settings, db)

    for vehicle in vehicles:

        print('Reading session table and identifying trips for vehicle %s' % vehicle)

        # === Get Session data =====================================================

        o = sems_rest_api.get_wrapped_api()
        num_sessions = o.sessions.get(params={'vehicle': vehicle}).count
        sessions = o.sessions.get(params={'vehicle': vehicle, "page_size": num_sessions}).results

        session_ids = [i.sessionid for i in sessions]  # use this line to extract ALL sessions

        if len(limit_dates) > 1:
            session_ids = limit_dates_list(limit_dates[0], limit_dates[1], sessions)

        if not session_ids:
            print(' Empty session list')
            continue

        chunk_size = 5
        session_sublists = sems_rest_api.chunks(session_ids,
                                                chunk_size)  # control the number of sessions

        try:
            setdata = get_df_from_online(session_sublists, rawcolumns, proccolumns, calccolumns)

            # --- check if file exists ---
            new_file_name = '{}_out_{}-{}.pkl'.format(
                vehicle,
                pd.to_datetime(setdata['datetime'].min(), unit='ms').strftime(
                    format='%d%m%y'),
                pd.to_datetime(setdata['datetime'].max(), unit='ms').strftime(format='%d%m%y'))
            new_file_check = out_path / new_file_name

            if not overwrite:
                if new_file_check.is_file():  # NB there's something funny happening here.
                    # Sometimes its a Path object (which is path.isfile()), but here it's a
                    # windowspath which is path.is_file().
                    print(f'{new_file_name} exists')
                    continue

        except Exception as e:
            dict_error = {
                'vehicle': vehicle,
                'error': e
            }
            list_errors.append(dict_error.copy())
            print('------ Error Occurred ------')
            continue

        print(f' ====== OSRM: {time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}')

        try:
            setdata = clean_up_gps_coordinates(setdata)
            # setdata = get_location_data(setdata)
            setdata, osrm_fail_suffix = osrm_postprocessing(setdata)
            # osrm_fail_suffix = '_no_limits'
        except Exception as e:
            dict_error = {
                'vehicle': vehicle,
                'osrm error': e
            }
            list_errors.append(dict_error.copy())
            continue

        setdata.to_pickle(out_path / '{}_out_{}-{}_incl_additional_signals{}.pkl'.format(
            vehicle,
            setdata.datetime.min().strftime(format='%d%m%y'),
            setdata.datetime.max().strftime(format='%d%m%y'),
            osrm_fail_suffix))  # write pickle file

    error_df = pd.DataFrame(list_errors)
    error_df.to_csv(((out_path / 'errors_download_{}.csv'.format(
        time.strftime("%d-%m-%y_%H-%M-%S", time.localtime(time.time()))
    ))), sep=',', index=False)

    print(f'###################################################################\n'
          f'END: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')


### Main for standalone running
if __name__ == "__main__":
    ## dummy variables

    mysettingssw = Path('../settings') / 'myreadonly.cnf'
    project_name = 'sems-rde'

    out_path = Path(f'../data/{project_name}/input')

    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    list_vehicles = [

        # first two days

        'VW_CA_VJG59N_EUR6'

    ]
    raw_columns = [
        'id',
        'datetime',
        'session',
        # 'RPM',
        'GPSalt',
        'GPSfix',
        'GPSlat',
        'GPSlon',
        'GPSsats',
        'after_cat_rdyNOx',
        'after_cat_rdyO2',
        'NFT',
        'CEL'
    ]
    proc_columns = [
        'id',
        'datetime',
        # 'after_cat_NOx',
        # 'after_cat_O2',
        'RPM',
        'ECT',
        # 'MAF',
        # 'LFE',
        'AET',
        'after_cat_EGT',
        'LFE'
    ]
    calc_columns = [
        'id',
        'datetime',
        # 'total_mF',
        'after_cat_NOx_mF',
        'after_cat_NH3_mF',
        'CO2_mF',
        # 'engine_on',
        # 'engine_hot',
        # 'engine_power',
        'velocity_filtered',
        'acceleration_filtered'
    ]

    # === Start ===

    main(list_vehicles, mysettingssw, out_path, raw_columns, proc_columns, calc_columns)
