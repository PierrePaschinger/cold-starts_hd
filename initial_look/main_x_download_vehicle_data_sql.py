"""
Download data via SQL
jessica.deruiter@tno.nl
"""

import getopt
import os
import sys
import time
import pandas as pd

import sqlalchemy
import matplotlib.pyplot as plt
from sqlalchemy.engine.url import URL
from pathlib import Path

from user_functions.func_OSRM_postprocessing import *
from user_functions.func_download_data_via_sql import *


def main(vehicles, mysettingsfile, out_path, rawcolumns, proccolumns, calccolumns,
         limit_dates=None, overwrite=0):
    if limit_dates is None:
        limit_dates = []
    print(f'###################################################################\n'
          f'START: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')

    list_errors = []
    lasttrip = -1

    rawcolumns = str(rawcolumns).replace("'", "").strip('[]')

    proccolumns = str(proccolumns).replace("'", "").strip('[]')

    calccolumns = str(calccolumns).replace("'", "").strip('[]')

    # === read settings file ================================================

    mysettings, db = read_settings_file(mysettingsfile)

    for vehicle in vehicles:

        try:
            mydb = URL(drivername='mysql+pymysql', host=mysettings.dbhost,
                       username=mysettings.dbuser,
                       password=mysettings.dbpassword, database=db)
        except Exception as e:
            print("Cannot define database because: ", e)
        engine = sqlalchemy.create_engine(name_or_url=mydb)
        connection = engine.connect()
        engine.execute("USE " + db)

        # === Get Dataframes ===============================================

        sessions = get_sessions(vehicle, engine)
        if len(sessions.index) == 0: continue

        if len(limit_dates) > 1:
            sessions = limit_dates_time(limit_dates[0], limit_dates[1], sessions)

        new_file_name = '{}_out_{}-{}.pkl'.format(
            vehicle,
            pd.to_datetime(sessions['datetime_start'].min(), unit='ms').strftime(format='%d%m%y'),
            pd.to_datetime(sessions['datetime_end'].max(), unit='ms').strftime(format='%d%m%y'))

        new_file_check = out_path / new_file_name

        if not overwrite:
            if new_file_check.isfile():
                print(f'{new_file_name} exists')
                continue

        try:
            setdata = get_dataframe(engine, vehicle, sessions, lasttrip,
                                    rawcolumns, proccolumns, calccolumns)

            # --- check again, just in case ---
            new_file_name = '{}_out_{}-{}.pkl'.format(
                vehicle,
                pd.to_datetime(setdata['datetime'].min(), unit='ms').strftime(
                    format='%d%m%y'),
                pd.to_datetime(setdata['datetime'].max(), unit='ms').strftime(format='%d%m%y'))
            new_file_check = out_path / new_file_name

            if not overwrite:
                if new_file_check.isfile():
                    print(f'{new_file_name} exists')
                    continue

        except Exception as e:
            dict_error = {
                'vehicle': vehicle,
                'error': e
            }
            list_errors.append(dict_error.copy())
            continue

        connection.close()  # to avoid timeout. Next vehicle the connection will be restored.

        print(f' ====== OSRM: {time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}'
              )

        try:
            setdata = clean_up_gps_coordinates(setdata)
            # setdata = get_location_data(setdata)
            setdata, osrm_fail_suffix = osrm_postprocessing(setdata)
            # osrm_fail_suffix = 'no_limits'
        except Exception as e:
            dict_error = {
                'vehicle': vehicle,
                'osrm error': e
            }
            list_errors.append(dict_error.copy())
            continue

        setdata.to_pickle(out_path / '{}_out_{}-{}{}.pkl'.format(
            vehicle,
            setdata.datetime.min().strftime(format='%d%m%y'),
            setdata.datetime.max().strftime(format='%d%m%y'),
            osrm_fail_suffix))  # write pickle file

    error_df = pd.DataFrame(list_errors)
    error_df.to_csv(((out_path / 'errors_download_{}.csv'.format(
        time.strftime("%d-%m-%y_%H-%M-%S", time.localtime(time.time()))
    ))), sep=',', index=False)

    print(f'###################################################################\n'
          f'END: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')


### Main for standalone running
if __name__ == "__main__":
    ## dummy variables

    mysettingssw = Path('../settings') / 'myreadonly.cnf'
    project_name = 'sems-rde'

    out_path = Path(f'../data/{project_name}/input')

    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    # === Get list of vehicles ==============================================
    #
    # if vehicle:
    #     list_vehicles = [vehicle]  # command line parameter filled? use it
    # else:
    #     list_vehicles = [
    #         'VW_CA_V152NG_EUR6',  # Caddy
    #     ]
    # list_vehicles = [
    #     # 'PE_30_XJ417X_EUR6',
    #     # 'VO_X6_XK461J_EUR6',
    #     # 'ME_B1_ZT566T_EUR6',
    #     # 'SK_OC_ZD852P_EUR6',
    #     'RE_MA_VDR83F_EUR6'
    #
    # ]
    #   list_vehicles = [
    #   'DA_XF_7920KXB_EURVI', # Getru 1
    #   'DA_XF_3100KVF_EURVI', # Getru 2
    #   'DA_XF_18BFT5_EURVI', # Starmans 1
    #   'DA_XF_58BJN6_EURVI', # Starmans 2
    #   'DA_XF_77BGH7_EURVI', # De Rijke
    #   'DA_XF_88BLD6_EURVI', # Overbeek
    #   ]

    # # === Getting LD vehicle list ==============================================
    # mysettings, db = read_settings_file(mysettingssw)
    #
    # try:
    #     mydb = URL(drivername='mysql+pymysql', host=mysettings.dbhost,
    #                username=mysettings.dbuser,
    #                password=mysettings.dbpassword, database=db)
    # except Exception as e:
    #     print("Cannot define database because: ", e)
    # engine = sqlalchemy.create_engine(name_or_url=mydb)
    # connection = engine.connect()
    # engine.execute("USE " + db)
    #
    # list_vehicles = pd.read_sql('SELECT vehicleid from vehicle where id IN (SELECT '
    #                             'vehicle FROM hd)', engine)
    # list_vehicles = list_vehicles.loc[~list_vehicles['vehicleid'].str.contains(
    #     'TEST|test|BOOT|VEH|THERMO')][
    #     'vehicleid']
    #
    # connection.close()  # we have the list, so close connection

    list_vehicles = [
        # 'VW_CA_VJG59N_EUR6'
        'PE_EX_VJR86F_EUR6'
    ]
    raw_columns = [
        'id',
        'datetime',
        'session',
        'RPM',
        'GPSalt',
        'GPSfix',
        'GPSlat',
        'GPSlon',
        'GPSsats',
        'after_cat_rdyNOx',
        # 'after_cat_NOx',
        'after_cat_rdyO2',
        # 'after_cat_O2',
        'NFT',
        'CEL'
    ]
    proc_columns = [
        'id',
        'datetime',
        'after_cat_NOx',
        'after_cat_O2',
        'after_cat_NH3',
        'RPM',
        'ECT',
        # 'MAF',
        # 'LFE',
        'AET',
        'after_cat_EGT',
        'LFE'
    ]
    calc_columns = [
        'id',
        'datetime',
        # 'total_mF',
        'after_cat_NOx_mF',
        'after_cat_NH3_mF',
        'CO2_mF',
        # 'engine_on',
        # 'engine_hot',
        # 'engine_power',
        'velocity_filtered',
        'acceleration_filtered'
    ]

    # === Start ===

    main(list_vehicles, mysettingssw, out_path, raw_columns, proc_columns, calc_columns)
