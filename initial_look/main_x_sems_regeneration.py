import glob
import os
import time

import seaborn as sns
from path import Path
import matplotlib.pyplot as plt
import pandas as pd

# from user_functions.func_clean_up_dataframe import calc_average_per_x_km_via_rolling_one_pollutant
from user_functions_analysis.func_dpf_regen import *
from user_functions_analysis.func_analyse_cold_start import get_cold_start_regression

from user_functions.func_change_axis_colour import *
from user_functions.func_add_tno_watermark_to_figure import add_tno_watermark_to_figure
from user_functions.labl_dicts_to_rename_or_colour import *


def find_locations_of_peaks(df_temp, col_threshold, col_to_compare, the_pollutant, col_cum_time,
                            col_cum_dist):
    label_pollutant = dict_pollutant_names[the_pollutant]

    # --- find where signal crosses to longest peak ---
    df_temp['time'] = df_temp[col_cum_time].copy()
    df_temp['distance'] = df_temp[col_cum_dist].copy()
    df_temp['higher_than_predicted'] = (
            df_temp[col_to_compare] > df_temp[col_threshold])
    df_temp['peak_number'] = (df_temp['higher_than_predicted'] != df_temp[
        'higher_than_predicted'].shift()).cumsum()
    df_temp['peak_length'] = df_temp.groupby(['higher_than_predicted', 'peak_number']
                                             ).cumcount(ascending=False) + 1
    df_temp.loc[df_temp['higher_than_predicted'] == False, 'peak_length'] = 0
    df_temp.loc[df_temp['higher_than_predicted'] == False, 'peak_number'] = np.nan
    df_temp.loc[df_temp['peak_length'] < 600, 'peak_number'] = np.nan
    df_temp.loc[df_temp['peak_number'].notnull(),
                'peak_number'] = (df_temp.loc[df_temp['peak_number'].notnull()]['peak_number'] !=
                                  df_temp.loc[df_temp['peak_number'].notnull()][
                                      'peak_number'].shift()).cumsum()

    list_sessions = list(df_temp.loc[df_temp['peak_number'].notnull()]['session'].unique())

    df_peak_limits = df_temp.loc[df_temp['peak_number'].notnull()].groupby('session').agg(
        {'time': ['min', 'max'],
         'distance': ['min', 'max'],
         the_pollutant: ['max', 'sum'],
         'session': 'max'
         }
    ).reset_index()
    df_peak_limits.columns = ['_'.join(col) for col in df_peak_limits.columns]
    for i_col in ['time', 'distance']:
        df_peak_limits[f'{i_col}_duration'] = (df_peak_limits[f'{i_col}_max']
                                               - df_peak_limits[f'{i_col}_min'])

    df_peak_limits.rename(columns={
        f'{the_pollutant}_max': 'pollutant_max',
        f'{the_pollutant}_sum': 'pollutant_sum',

    }, inplace=True)

    df_peak_limits['pollutant'] = label_pollutant

    # f, ax = plt.subplots()
    # ax.scatter(
    #     df_temp.loc[df_temp['peak_number'] > 1].index,
    #     df_temp.loc[df_temp['peak_number'] > 1][col_to_compare]
    # )
    # ax.scatter(
    #     df_temp.index,
    #     df_temp[col_threshold]
    # )
    # ax.plot(
    #     df_temp.index,
    #     df_temp[col_to_compare]
    # )

    return list_sessions, df_peak_limits


def calc_per_km_rolling_pollutant_velocity(df_, window_size_integer, vel_signal, y_signal
                                           ):
    df_temp = df_.copy()
    df_temp = df_temp.dropna(subset=[vel_signal])
    df_temp['cum_distance_sid'] = df_temp[vel_signal].cumsum() / 3600
    df_temp['time_index'] = pd.to_datetime(df_temp['cum_distance_sid'], unit='s')
    df_temp.set_index('time_index', inplace=True)

    window_size = '{}s'.format(window_size_integer)

    # how many km per window?
    df_temp['km_rolled'] = (df_temp[vel_signal].rolling(window_size).sum()) / 3600
    df_temp['km_rolled'] = df_temp['km_rolled'].mask(
        df_temp['km_rolled'] <= window_size_integer / 1000)

    # pollutant per window -> pollutant per km
    df_temp['y_rolled'] = df_temp[y_signal].rolling(window_size).sum()
    df_temp[f'{y_signal}_km_rolled'] = df_temp['y_rolled'] / df_temp['km_rolled']

    # average speed
    # df_temp['av_vel'] = (df_temp[vel_signal].rolling(window_size).mean())
    # df_temp['av_vel_0dp'] = df_temp['av_vel'].round(0)

    df_out = df_temp.reset_index(drop=True).drop(columns=['cum_distance_sid',
                                                          'km_rolled',
                                                          'y_rolled'
                                                          ])

    return df_out


def plot_nox_nh3(df, suffix):
    font_size = 9

    f, ax = plt.subplots(nrows=3, sharex=True)
    ax[0].plot(df[col_time], df['velocity_filtered'],
               color='gray', alpha=0.3)
    ax[0].set_ylabel('Speed [km/h]', fontsize=font_size)
    change_axis_colour(ax[0], 'gray')

    if col_egt in df.columns:
        ax_1 = ax[0].twinx()
        ax_1.set_ylabel('Exhaust T. [°C]')
        change_axis_colour(ax_1, 'C1')
        # ax_1.plot(df[col_time], df[col_egt],
        #           color='C1', alpha=0.5)
        ax_1.plot(df[col_time], df['rolling_temp'],
                  color='C1', alpha=0.5)
        # ax_1.axhline(df[col_egt].quantile(0.90), color='C1')

    ax[1].plot(df[col_time], df['after_cat_NOx_mF_cum'], color='C0')
    ax[1].set_ylabel('NOx [mg]', fontsize=font_size)

    ax1_1 = ax[1].twinx()
    change_axis_colour(ax1_1, 'C0')
    ax1_1.set_ylabel('NOx [mg/s]', fontsize=font_size)
    ax1_1.plot(df[col_time], df[col_nox], color='C0', alpha=0.5)

    ax[2].plot(df[col_time], df['after_cat_NH3_mF_cum'], color='C8')
    ax[2].set_ylabel('NH3 [mg]', fontsize=font_size)

    ax2_1 = ax[2].twinx()
    change_axis_colour(ax2_1, 'C8')
    ax2_1.set_ylabel('NH3 [mg/s]', fontsize=font_size)
    ax2_1.plot(df[col_time], df[f'after_cat_NH3_mF'], color='C8', alpha=0.5)

    d_driven = df['velocity_filtered'].sum() / 3600

    ax[0].set_title(f'{vehicle_file_prefix} -{suffix} - {d_driven:.0f} km')
    ax[2].set_xlabel('Time [s]')

    add_tno_watermark_to_figure(f, ax[2], 'input/TNO_zwart.png')
    f.align_ylabels()
    f.subplots_adjust(hspace=0.3)

    filename = f'{vehicle_file_prefix}_nox_nh3_{i_window}_dpf_{suffix}.png'
    # f.show()
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)


def plot_nox_nh3_peaks(df, suffix):
    font_size = 9

    f, ax = plt.subplots(nrows=3, sharex=True)
    ax[0].plot(df[col_time], df['velocity_filtered'],
               color='gray', alpha=0.3)
    ax[0].set_ylabel('Speed [km/h]', fontsize=font_size)
    change_axis_colour(ax[0], 'gray')

    if col_egt in df.columns:
        ax_1 = ax[0].twinx()
        ax_1.set_ylabel('Exhaust T. [°C]')
        change_axis_colour(ax_1, 'C1')
        # ax_1.plot(df[col_time], df[col_egt],
        #           color='C1', alpha=0.5)
        ax_1.plot(df[col_time], df['rolling_temp'],
                  color='C1', alpha=0.5)
        ax_1.scatter(df.loc[df['high_temp'] == 1][col_time],
                     df.loc[df['high_temp'] == 1]['rolling_temp'],
                     color='C1', alpha=0.5)
        # ax_1.axhline(df[col_egt].quantile(0.90), color='C1')

    ax[1].plot(df[col_time], df['after_cat_NOx_mF_cum'], color='C0')
    ax[1].set_ylabel('NOx [mg]', fontsize=font_size)

    ax1_1 = ax[1].twinx()
    change_axis_colour(ax1_1, 'C0')
    ax1_1.set_ylabel('NOx [mg/s]', fontsize=font_size)
    ax1_1.plot(df[col_time], df[col_nox], color='C0', alpha=0.5)

    ax[2].plot(df[col_time], df['after_cat_NH3_mF_cum'], color='C8')
    ax[2].set_ylabel('NH3 [mg]', fontsize=font_size)

    ax2_1 = ax[2].twinx()
    change_axis_colour(ax2_1, 'C8')
    ax2_1.set_ylabel('NH3 [mg/s]', fontsize=font_size)
    ax2_1.plot(df[col_time], df[f'after_cat_NH3_mF'], color='C8', alpha=0.5)

    d_driven = df['velocity_filtered'].sum() / 3600

    ax[0].set_title(f'{vehicle_file_prefix} -{suffix} - {d_driven:.0f} km')
    ax[2].set_xlabel('Time [s]')

    add_tno_watermark_to_figure(f, ax[2], 'input/TNO_zwart.png')
    f.align_ylabels()
    f.subplots_adjust(hspace=0.3)

    filename = f'{vehicle_file_prefix}_nox_nh3_{i_window}_dpf_{suffix}_peaks.png'
    # f.show()
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)


def plot_nox_nh3_annotated(df, suffix):
    font_size = 9

    f, ax = plt.subplots(nrows=3, sharex=True)
    ax[0].plot(df[col_time], df['velocity_filtered'],
               color='gray', alpha=0.3)
    ax[0].set_ylabel('Speed [km/h]', fontsize=font_size)
    change_axis_colour(ax[0], 'gray')

    if col_egt in df.columns:
        ax_1 = ax[0].twinx()
        ax_1.set_ylabel('Exhaust T. [°C]')
        change_axis_colour(ax_1, 'C1')
        # ax_1.plot(df[col_time], df[col_egt],
        #           color='C1', alpha=0.5)
        ax_1.plot(df[col_time], df['rolling_temp'],
                  color='C1', alpha=0.5)
        # ax_1.axhline(df[col_egt].quantile(0.90), color='C1')

    ax[1].plot(df[col_time], df['after_cat_NOx_mF_cum'], color='C0')
    ax[1].set_ylabel('NOx [mg]', fontsize=font_size)

    ax1_1 = ax[1].twinx()
    change_axis_colour(ax1_1, 'C0')
    ax1_1.set_ylabel('NOx [mg/s]', fontsize=font_size)
    ax1_1.plot(df[col_time], df[col_nox], color='C0', alpha=0.5)

    ax[2].plot(df[col_time], df['after_cat_NH3_mF_cum'], color='C8')
    ax[2].set_ylabel('NH3 [mg]', fontsize=font_size)

    ax2_1 = ax[2].twinx()
    change_axis_colour(ax2_1, 'C8')
    ax2_1.set_ylabel('NH3 [mg/s]', fontsize=font_size)
    ax2_1.plot(df[col_time], df[f'after_cat_NH3_mF'], color='C8', alpha=0.5)

    d_driven = df['velocity_filtered'].sum() / 3600

    ax[0].set_title(f'{vehicle_file_prefix} -{suffix} - {d_driven:.0f} km')
    ax[2].set_xlabel('Time [s]')

    add_tno_watermark_to_figure(f, ax[2], 'input/TNO_zwart.png')
    f.align_ylabels()
    f.subplots_adjust(hspace=0.3)

    # ---- try annotation ----------------------------------------------
    try:
        df_plot = pd.DataFrame(list_plot)
        df_plot = df_plot.set_index('pollutant')
        dict_plot = df_plot.to_dict('index')

        for i_ax, j_pollutant in [(1, 'NOx'), (2, 'NH3')]:
            try:
                ax[i_ax].axvline(dict_plot[j_pollutant]['time to start regen'],
                                 color='r', linestyle='-.',
                                 label='{:.1f} km / {:.2f}'.format(
                                     dict_plot[j_pollutant]['time to start regen'],
                                     dict_plot[j_pollutant]['cum emissions start regen']))
                ax[i_ax].axvline(dict_plot[j_pollutant]['time to end regen'],
                                 color='r', linestyle='--',
                                 label='{:.1f} km / {:.2f}'.format(
                                     dict_plot[j_pollutant]['time to end regen'],
                                     dict_plot[j_pollutant]['cum emissions end regen']))
            except Exception as e:
                print(e)
                dict_error = {
                    'vehicle': f'{file_prefix}',
                    'error': e
                }
                continue

    except Exception as e:
        print(e)
        dict_error = {
            'vehicle': f'{file_prefix}',
            'error': e
        }

    filename = f'{vehicle_file_prefix}_nox_nh3_{i_window}_dpf_{suffix}_annotated.png'
    # f.show()
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)


def plot_nox_nh3_annotated_peaks(df, suffix):
    font_size = 9

    f, ax = plt.subplots(nrows=3, sharex=True)
    ax[0].plot(df[col_time], df['velocity_filtered'],
               color='gray', alpha=0.3)
    ax[0].set_ylabel('Speed [km/h]', fontsize=font_size)
    change_axis_colour(ax[0], 'gray')

    if col_egt in df.columns:
        ax_1 = ax[0].twinx()
        ax_1.set_ylabel('Exhaust T. [°C]')
        change_axis_colour(ax_1, 'C1')
        # ax_1.plot(df[col_time], df[col_egt],
        #           color='C1', alpha=0.5)
        ax_1.plot(df[col_time], df['rolling_temp'],
                  color='C1', alpha=0.5)
        ax_1.scatter(df.loc[df['high_temp'] == 1][col_time],
                     df.loc[df['high_temp'] == 1]['rolling_temp'],
                     color='C1', alpha=0.5)
        # ax_1.axhline(df[col_egt].quantile(0.90), color='C1')

    ax[1].plot(df[col_time], df['after_cat_NOx_mF_cum'], color='C0')
    ax[1].set_ylabel('NOx [mg]', fontsize=font_size)

    ax1_1 = ax[1].twinx()
    change_axis_colour(ax1_1, 'C0')
    ax1_1.set_ylabel('NOx [mg/s]', fontsize=font_size)
    ax1_1.plot(df[col_time], df[col_nox], color='C0', alpha=0.5)

    ax[2].plot(df[col_time], df['after_cat_NH3_mF_cum'], color='C8')
    ax[2].set_ylabel('NH3 [mg]', fontsize=font_size)

    ax2_1 = ax[2].twinx()
    change_axis_colour(ax2_1, 'C8')
    ax2_1.set_ylabel('NH3 [mg/s]', fontsize=font_size)
    ax2_1.plot(df[col_time], df[f'after_cat_NH3_mF'], color='C8', alpha=0.5)

    d_driven = df['velocity_filtered'].sum() / 3600

    ax[0].set_title(f'{vehicle_file_prefix} -{suffix} - {d_driven:.0f} km')
    ax[2].set_xlabel('Time [s]')

    add_tno_watermark_to_figure(f, ax[2], 'input/TNO_zwart.png')
    f.align_ylabels()
    f.subplots_adjust(hspace=0.3)

    # ---- try annotation ----------------------------------------------
    try:
        df_plot = pd.DataFrame(list_plot)
        df_plot = df_plot.set_index('pollutant')
        dict_plot = df_plot.to_dict('index')

        for i_ax, j_pollutant in [(1, 'NOx'), (2, 'NH3')]:
            try:
                ax[i_ax].axvline(dict_plot[j_pollutant]['time to start regen'],
                                 color='r', linestyle='-.',
                                 label='{:.1f} km / {:.2f}'.format(
                                     dict_plot[j_pollutant]['time to start regen'],
                                     dict_plot[j_pollutant]['cum emissions start regen']))
                ax[i_ax].axvline(dict_plot[j_pollutant]['time to end regen'],
                                 color='r', linestyle='--',
                                 label='{:.1f} km / {:.2f}'.format(
                                     dict_plot[j_pollutant]['time to end regen'],
                                     dict_plot[j_pollutant]['cum emissions end regen']))
            except Exception as e:
                print(e)
                dict_error = {
                    'vehicle': f'{file_prefix}',
                    'error': e
                }
                continue

    except Exception as e:
        print(e)
        dict_error = {
            'vehicle': f'{file_prefix}',
            'error': e
        }

    filename = f'{vehicle_file_prefix}_nox_nh3_{i_window}_dpf_{suffix}_annotated_peaks.png'
    # f.show()
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)


project_name = 'sems-rde'

input_path = Path(f'../data/{project_name}/input')
out_path = Path(f'../data/{project_name}/output/regenerations')
if not out_path.exists():
    raise Exception(f'Path {out_path} does not exist')

list_vehicle_file = glob.glob(input_path / 'VW_CA_VJG59N_EUR6_out_220321-090421.pkl')

i_window = 1
i_time = 600

col_time = 'Time [s]'
col_egt = 'after_cat_EGT'
col_nox = 'after_cat_NOx_mF'

list_out = []

for file_input_path in list_vehicle_file:

    test_name = os.path.basename(os.path.dirname(file_input_path))
    file_name = os.path.splitext(os.path.basename(file_input_path))[0]
    vehicle_file_prefix = file_name[:17]
    title_prefix = vehicle_file_prefix
    print(f'====== {title_prefix} =====')

    print('--- {}'.format(
        time.strftime("%H:%M:%S", time.localtime(time.time()))))

    df = pd.read_pickle(file_input_path)
    list_pollutant_cols = [
        'after_cat_NH3_mF',
        'after_cat_NOx_mF'
    ]

    if vehicle_file_prefix == 'SK_OC_ZD852P_EUR6':
        # Combine session 132741 and 132742
        df.loc[df['session'] == 132742, 'session'] = 13274200
        df.loc[df['session'] == 132741, 'session'] = 13274200

    df['Time [s]'] = df['timecount'] - df.groupby('session')['timecount'].transform('min')
    df.sort_values(by='datetime', inplace=True)
    df['Distance [km]'] = df['velocity_filtered'].cumsum() / 3600
    df['rolling_temp'] = df[col_egt].rolling(300, center=True, min_periods=300).mean()

    # --- calc rolling and cumulative pollutant (except not actually rolling
    # because it takes ages) ---------------------------------------------------

    for pollutant in list_pollutant_cols:
        label_pollutant = dict_pollutant_names[pollutant]
        unit_pollutant = dict_pollutant_units[pollutant]

        # df[[f'{pollutant}_roll_{i_window}km', f'{pollutant}_limit_{i_window}km']] = \
        #     calc_average_per_x_km_via_rolling_one_pollutant(
        #         df, i_window,
        #         'velocity_filtered',
        #         pollutant,
        #         'Time [s]',
        #         (200,
        #          0.5,
        #          20))
        df[f'{pollutant}_cum'] = df.groupby('session')[pollutant].cumsum()

    print('--- {}'.format(
        time.strftime("%H:%M:%S", time.localtime(time.time()))))

    # --- attempt finding peak on temp -----------------------------------------

    list_bins = [0, 80, 100, 120, 140, df['velocity_filtered'].max() + 1]
    if df['velocity_filtered'].max() + 1 < 140:
        list_bins = [0, 80, 100, 120, df['velocity_filtered'].max() + 1]

    df['speed_bin'] = pd.cut(df['velocity_filtered'],
                             bins=list_bins)

    df['mean_2s'] = (df.groupby('speed_bin')['rolling_temp'].transform('mean') +
                     2 * df.groupby('speed_bin')['rolling_temp'].transform('std'))
    df['mean_3s'] = (df.groupby('speed_bin')['rolling_temp'].transform('mean') +
                     3 * df.groupby('speed_bin')['rolling_temp'].transform('std'))
    df.loc[df['rolling_temp'] > df['mean_2s'], 'high_temp'] = 1

    list_dpf_trips = list(df.loc[df['rolling_temp'] > df['mean_3s']]['session'].unique())

    # --- other options for getting list ---------------------------------------

    # list_dpf_trips_2 = list(df.loc[df['rolling_temp'] > df['mean_2s']]['session'].unique())
    # list_dpf_trips = list(df.loc[df['rolling_temp'] > 350]['session'].unique())

    # --- test trips skoda ---
    # list_dpf_trips = [132760, 133122]
    # list_dpf_trips = [130946, 131097, 131786, 132446, 133122]

    distance_driven = df['Distance [km]'].round(1).max()

    plt.close('all')

    for file_prefix in list_dpf_trips:
        plt.close('all')
        list_plot = []
        df_temp = df.loc[df['session'] == file_prefix].copy()
        df_temp['cum_dist [km]'] = df_temp['velocity_filtered'].cumsum() / 3600

        # --- get rough bracket before and after -------------------------------
        t_1_i = df_temp.loc[df_temp.loc[df_temp['high_temp'] == 1].index.min(), 'Time [s]'] - 300
        t_1_f = df_temp.loc[df_temp.loc[df_temp['high_temp'] == 1].index.min(), 'Time [s]']
        t_2_i = df_temp.loc[df_temp.loc[df_temp['high_temp'] == 1].index.max(), 'Time [s]']
        t_2_f = df_temp.loc[df_temp.loc[df_temp['high_temp'] == 1].index.max(), 'Time [s]'] + 300

        # --- check bracket makes sense  ---------------------------------------

        if t_1_i < 0:
            t_1_i = 0
        if t_2_f > df_temp['Time [s]'].max():
            t_2_f = df_temp['Time [s]'].max()

        for pol_num, pollutant in enumerate(list_pollutant_cols):
            try:
                print(f'---- {pollutant}')
                label_pollutant = dict_pollutant_names[pollutant]
                unit_pollutant = dict_pollutant_units[pollutant]
                df_temp.loc[df_temp[pollutant] < 0, pollutant] = 0

                df_temp['cum_pollutant'] = df_temp[pollutant].cumsum()

                # --- look for plateaus ----------------------------------------

                if t_1_f - t_1_i < 300:  # regen at start
                    print('----- regen at start -----')
                    i_1 = get_cold_start_regression(df_temp, col_time, 'cum_dist [km]',
                                                    'cum_pollutant',
                                                    t_1_f, 60)
                else:
                    i_1 = get_window_regression_max(df_temp, col_time, 'cum_pollutant',
                                                    t_1_i, t_1_f, 60)
                # TODO get_window_regression_max throws a relatively high number of fail -> it
                #  results in values at the *end* of the trip

                if t_2_f - t_2_i < 300:  # regen at end
                    print('----- regen unfinished -----')
                    i_2 = get_dpf_unfinished(df_temp, col_time, 'cum_pollutant',
                                             t_2_i, t_2_f, 60)
                else:
                    i_2 = get_end_dpf(df_temp, col_time, 'cum_pollutant',
                                      t_2_i, t_2_f, 60)

                # --- make sure that the numbers aren't weird ------------------
                i_test_1 = df_temp.loc[i_1, ['Time [s]']][0]
                if i_test_1 > (t_1_f + 30):
                    print(f'----- {i_test_1} > {(t_1_f + 30)}-----')
                    i_new = t_1_f - 180
                    if i_new < t_1_i:
                        i_new = t_1_i
                    i_1 = df_temp.loc[df_temp['Time [s]'] == i_new].index.max()

                d_1, y_1, t_1, tot_d_1 = get_parameters_based_on_index(df_temp, col_time,
                                                                       i_1, 'cum_pollutant')

                i_test_2 = df_temp.loc[i_2, ['Time [s]']][0]
                if i_test_2 > (t_2_f + 1000):
                    print(f'----- {i_test_2} > {(t_2_f + 1000)}-----')
                    i_new_2 = t_2_f + 600
                    if i_new_2 > df_temp['Time [s]'].max():
                        i_new_2 = df_temp['Time [s]'].max()
                    i_2 = df_temp.loc[df_temp['Time [s]'] == i_new_2].index.max()
                d_2, y_2, t_2, tot_d_2 = get_parameters_based_on_index(df_temp, col_time,
                                                                       i_2, 'cum_pollutant')

                # --- print output ---------------------------------------------

                dict_out_temp = {
                    'vehicle': vehicle_file_prefix,
                    'test': file_prefix,

                    'pollutant': label_pollutant,
                    'regen_emissions': y_2 - y_1,
                    'regen_time': t_2 - t_1,
                    'regen_dist': d_2 - d_1,
                    'total dist at start regen': tot_d_1,
                    'total dist at end regen': tot_d_2,

                    'time to start regen': t_1,
                    'cum emissions start regen': y_1,

                    'time to end regen': t_2,
                    'cum emissions end regen': y_2,
                    # 'dist to end regen': d_2,
                    # 'dist to start regen': d_1,
                    # 'total_emissions': df_temp[pollutant].sum(),
                    # 'total_distance': df_temp['cum_dist [km]'].max()
                }

                list_plot.append(dict_out_temp)
                list_out.append(dict_out_temp)


            except Exception as e:
                print(f'{e} ==========================================')
                dict_error = {
                    'vehicle': f'{file_prefix}',
                    'error': e
                }
                continue

        plot_nox_nh3_annotated(df_temp, f'{file_prefix}')
        plot_nox_nh3_annotated_peaks(df_temp, f'{file_prefix}')
        # plot_nox_nh3_peaks(df_temp, f'{file_prefix}_peaks')
        # plot_nox_nh3(df_temp, file_prefix)

df_out = pd.DataFrame(list_out)
df_out.to_csv(out_path / 'sems_dpf_regenerations_{}.csv'.format(
    time.strftime("%d-%m-%Y_%H%M%S", time.localtime(time.time()))), sep=',', index=False)

# --- generate nox output table ------------------------------------------------

df_nox = df_out.loc[df_out['pollutant'] == 'NOx'].copy()
df_nox['dist since last'] = (df_nox['total dist at start regen'] -
                             df_nox['total dist at end regen'].shift())
df_nox['mg_km'] = df_nox['regen_emissions'] / df_nox['regen_dist']
df_nox[['vehicle',
        'test',
        'pollutant',
        'dist since last',
        'regen_emissions',
        'regen_time',
        'regen_dist',
        'mg_km',
        'total dist at start regen',
        'total dist at end regen',
        ]].to_csv(out_path / f'{file_name}_nox_regenerations.csv', index=False)

print('end')

