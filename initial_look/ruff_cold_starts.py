import glob
import os
import time

import seaborn as sns
from path import Path
import matplotlib.pyplot as plt
import pandas as pd

from user_functions.func_download_one_session import download_one_session
from user_functions_analysis.func_plot_nox_nh3_over_all_time import plot_nox_nh3, plot_nox_nh3_ect
from user_functions_analysis.func_analyse_cold_start import analyse_cold_start

# === Import data ==============================================================

mysettingssw = Path('../settings') / 'myreadonly.cnf'
project_name = 'sems-rde'

input_path = Path(f'../data/{project_name}/input')
out_path = Path(f'../data/{project_name}/output/cold_start')
if not out_path.exists():
    raise Exception(f'Path {out_path} does not exist')

# list_vehicle_file = glob.glob(input_path / 'VW_CA_VJG59N_EUR6*.pkl')
list_vehicle_file = glob.glob(input_path / 'VW_CA_VJG59N_EUR6_out_220321-090421.pkl')

i_time = 300

col_time = 'Time [s]'
col_egt = 'after_cat_EGT'
col_nox = 'after_cat_NOx_mF'
col_nh3 = 'after_cat_NH3_mF'
col_ect = 'ECT'

list_pollutant_cols = [
    'after_cat_NH3_mF',
    'after_cat_NOx_mF'
]

list_error = []
list_out = []
list_comparison_out = []

for a_file in list_vehicle_file:
    file_input_path = a_file
    file_name = os.path.splitext(os.path.basename(file_input_path))[0]
    vehicle_file_prefix = file_name[:17]
    title_prefix = vehicle_file_prefix
    print(f'====== {title_prefix} =====')

    print('--- {}'.format(
        time.strftime("%H:%M:%S", time.localtime(time.time()))))

    df = pd.read_pickle(file_input_path)
    df.sort_values(by='datetime', inplace=True)
    df.reset_index(drop=True, inplace=True)
    df['timecount'] = df.index

    print('--- {}'.format(
        time.strftime("%H:%M:%S", time.localtime(time.time()))))

    # print(f'{file_name} - {df.session.unique()}')

    df['Time [h]'] = df['timecount'] / 3600

    list_cold_starts = list(df.loc[df[col_ect] < 70]['session'].unique())

    for i_session in list_cold_starts:
        plt.close('all')

        df_temp = df.loc[df['session'] == i_session].copy()
        df_temp['Distance [km]'] = df_temp['velocity_filtered'].cumsum() / 3600
        df_temp['Time [s]'] = df_temp['timecount'] - df_temp.groupby('session')[
            'timecount'].transform('min')

        if (df_temp['velocity_filtered'].cumsum() / 3600).max() > 8:
            print(f'--- {i_session}')
            plot_nox_nh3_ect(df_temp,
                             '{} - {}'.format(vehicle_file_prefix,
                                              df_temp['session'].unique()[0]),
                             '{}-{}'.format(vehicle_file_prefix,
                                            df_temp['session'].unique()[0]),
                             col_time, col_egt, col_ect, col_nox, col_nh3,
                             list_pollutant_cols, out_path, 1)
            df_1st_8km = df_temp.loc[df_temp['Distance [km]'] < 8].copy()
            plot_nox_nh3_ect(df_1st_8km,
                             '{} - {} - 1st 8 km'.format(vehicle_file_prefix,
                                                         df_1st_8km['session'].unique()[0]),
                             '{}-{}-1st8km'.format(vehicle_file_prefix,
                                                   df_1st_8km['session'].unique()[0]),
                             col_time, col_egt, col_ect, col_nox, col_nh3,
                             list_pollutant_cols, out_path, 1)
            try:
                analyse_cold_start(df_temp, 'velocity_filtered', col_time, vehicle_file_prefix,
                                   '{}'.format(df_1st_8km['session'].unique()[0]),
                                   list_out, list_comparison_out, list_error, col_ect,
                                   list_pollutant_cols, out_path)

                analyse_cold_start(df_1st_8km, 'velocity_filtered', col_time, vehicle_file_prefix,
                                   '{} - 1st 8km'.format(df_1st_8km['session'].unique()[0]),
                                   list_out, list_comparison_out, list_error, col_ect,
                                   list_pollutant_cols, out_path)
            except Exception as e:
                print(e)
                dict_error = {
                    'vehicle': i_session,
                    'osrm error': e
                }
                list_error.append(dict_error.copy())

df_out = pd.DataFrame(list_out)
df_out.drop_duplicates(inplace=True)
df_out.to_csv(((out_path / 'cold_start_coeff_{}.csv'.format(
    time.strftime("%d-%m-%Y_%H%M%S", time.localtime(time.time()))
))), sep=',', index=False)

df_out = pd.DataFrame(list_comparison_out)
df_out.drop_duplicates(inplace=True)
df_out.to_csv(((out_path / 'annotated' / 'cold_start_coeff_comparison_{}.csv'.format(
    time.strftime("%d-%m-%Y_%H%M%S", time.localtime(time.time()))
))), sep=',', index=False)
