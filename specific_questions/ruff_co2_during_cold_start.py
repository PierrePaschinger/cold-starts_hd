import glob
import os
import time

import seaborn as sns
from path import Path
import matplotlib.pyplot as plt
import pandas as pd

# from user_functions.func_clean_up_dataframe import calc_average_per_x_km_via_rolling_one_pollutant
from user_functions_analysis.func_dpf_regen import *
from user_functions_analysis.func_analyse_cold_start import get_cold_start_regression

from user_functions.func_change_axis_colour import *
from user_functions.func_add_tno_watermark_to_figure import add_tno_watermark_to_figure
from user_functions.labl_dicts_to_rename_or_colour import *

project_name = 'sems-rde'

input_path = Path(f'../data/{project_name}/input')

out_path = Path(f'../data/{project_name}/output/cold_start')
if not out_path.exists():
    raise Exception(f'Path {out_path} does not exist')

list_vehicle_file = glob.glob(input_path / 'VW_CA_VJG59N_EUR6_out_220321-090421.pkl')

input_path_2 = Path(
    r'C:\Users\ruiterjmd\TNO\IenW meet-en monitoring progr 2021 - Work\3 monitoren en meten aan '
    r'personenautos en bestelwagens\VW_CA_VJG59N_EUR6\cold_start')

i_window = 1
i_time = 600

col_time = 'Time [s]'
col_egt = 'after_cat_EGT'
col_nox = 'after_cat_NOx_mF'

list_out = []

for file_input_path in list_vehicle_file:
    test_name = os.path.basename(os.path.dirname(file_input_path))
    file_name = os.path.splitext(os.path.basename(file_input_path))[0]
    vehicle_file_prefix = file_name[:17]
    title_prefix = vehicle_file_prefix
    print(f'====== {title_prefix} =====')

    print('--- {}'.format(
        time.strftime("%H:%M:%S", time.localtime(time.time()))))

    # --- open files -----------------------------------------------------------

    df = pd.read_pickle(file_input_path)
    df.sort_values(by='datetime', inplace=True)
    df['Time [s]'] = df['timecount'] - df.groupby('session')['timecount'].transform('min')
    df['Distance'] = df['velocity_filtered'].cumsum() / 3600
    df['Distance [km]'] = df['Distance'] - df.groupby('session')['Distance'].transform('min')

    df_cold = pd.read_csv(input_path_2 / 'cold_start_coeff_09-04-2021_corrected.csv')
    df_cold['session'] = df_cold['test'].str.split(' - ').str[0].astype(int)
    df_cold['dist'] = df_cold['cold start dist']


    # --- define row-wise functions --------------------------------------------

    def get_co2_emissions(trip, x_cold_start):
        df_trip = df.loc[(df['session'] == trip) &
                         (df['Distance [km]'] <= x_cold_start)].copy()
        i_co2 = df_trip['CO2_mF'].sum()
        return i_co2


    def get_average_power_emissions(trip, x_cold_start):
        df_trip = df.loc[(df['session'] == trip) &
                         (df['Distance [km]'] <= x_cold_start)].copy()
        i_pwr = df_trip['CO2_mF'].mean() / 0.2
        return i_pwr


    # --- calculate parameters -------------------------------------------------
    df_cold['co2'] = df_cold.apply(lambda x: get_co2_emissions(x.session, x.dist), axis=1)
    df_cold['kW'] = df_cold.apply(lambda x: get_average_power_emissions(x.session, x.dist), axis=1)
    df_cold['nox_co2'] = df_cold['cold start offset'] / df_cold['co2']
    df_cold['kWh'] = df_cold['co2'] / 720

    # --- look at co2 dependency -----------------------------------------------
    y_ = np.array(df_cold['dist'])
    x_ = np.array(df_cold['co2'])
    slope, intercept, r_value, p_value, std_err = linregress(x_, y_)
    df_cold['straight_line'] = slope * df_cold['co2'] + intercept

    f, ax = plt.subplots()
    sns.lineplot(df_cold['co2'], df_cold['straight_line'],
                 label=f'{slope:.2E} $CO_2$ + {intercept:.2F}', alpha=0.5)
    ax.scatter(df_cold['co2'], df_cold['dist'])
    ax.set_ylabel('Cold start distance [km]')
    ax.set_xlabel('Cold start CO$_2$ emissions [g]')
    filename = f'{file_name}_cold_start_co2-dist.png'
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)

    # --- nox offset -----------------------------------------------------------
    f, ax = plt.subplots()
    ax.scatter(df_cold['dist'], df_cold['cold start offset'])
    ax.set_xlabel('Cold start distance [km]')
    ax.set_ylabel('Cold start NO$_x$ emissions [mg]')
    filename = f'{file_name}_cold_start_dist-nox.png'
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)

    # --- work dependency ------------------------------------------------------
    f, ax = plt.subplots()
    ax.scatter(df_cold['kWh'], df_cold['dist'])
    ax.set_ylabel('Cold start distance [km]')
    ax.set_xlabel('Cold start total work [kWh]')
    filename = f'{file_name}_cold_start_kWh-dist.png'
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)

    # --- average power dependency ---------------------------------------------

    f, ax = plt.subplots()
    ax.scatter(df_cold['kW'], df_cold['dist'])
    ax.set_ylabel('Cold start distance [km]')
    ax.set_xlabel('Cold start average power [kW]')
    filename = f'{file_name}_cold_start_kW-dist.png'
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)

    # --- print with co2 offset ------------------------------------------------

    df_cold[['session',
             'cold start dist',
             'cold start offset',
             'co2',
             'nox_co2',
             'warm_per_km',
             'total_emissions'
             ]].to_csv(out_path / f'{file_name}_cold_start.csv', index=False)

print('end')
