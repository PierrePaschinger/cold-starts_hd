import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import glob
import os

list_raw_csv = ['084939.CSV','125447.CSV','144733.CSV']
project_name = 'sems-rde'
input_path = Path(f'../data/{project_name}/input')
out_path = Path(f'../data/{project_name}/output')
if not out_path.exists():
    raise Exception(f'Path {out_path} does not exist')
    
df_db = pd.read_pickle(input_path / 'VW_CA_VJG59N_EUR6_out_220321-090421_incl_additional_signals.pkl')

df_db['datetime'] = pd.to_datetime(df_db['datetime'])
df_db = df_db.sort_values(by='datetime')

fig,ax=plt.subplots(nrows=4,sharex=True)
ax[0].plot(df_db['datetime'],df_db['velocity'],label='db - velocity')
ax[1].plot(df_db['datetime'],df_db['after_cat_rdyNOx'],label='db - after_cat_rdyNOx')
ax[2].plot(df_db['datetime'],df_db['after_cat_NOx_uncalib'],label='db - after_cat_NOx_uncalib')
ax[2].plot(df_db['datetime'],df_db['after_cat_NOx'],label='db - after_cat_NOx')
ax[3].plot(df_db['datetime'],df_db['after_cat_NOx_mF'],label='db - after_cat_NOx_mF')

df_raw_csv_combi = pd.DataFrame()
for raw_csv in list_raw_csv:
    df_raw_csv = pd.read_csv(input_path / raw_csv,skiprows=[1])
    df_raw_csv['datetime'] = pd.to_datetime(df_raw_csv['UNIX'],unit='s')
    df_raw_csv_combi = df_raw_csv_combi.append(df_raw_csv)    

df_raw_csv_combi = df_raw_csv_combi.reset_index(drop=True).sort_values(by='datetime')

ax[0].plot(df_raw_csv_combi['datetime'],df_raw_csv_combi['VSS'],alpha=0.5,label='raw - VSS')
ax[0].plot(df_raw_csv_combi['datetime'],df_raw_csv_combi['GPS_SOG'],alpha=0.5,label='raw - GPS_SPEED')
ax[1].plot(df_raw_csv_combi['datetime'],df_raw_csv_combi['after_cat_rdyNOx'],alpha=0.5,label='raw - after_cat_rdyNOx')
ax[2].plot(df_raw_csv_combi['datetime'],df_raw_csv_combi['after_cat_NOx'],alpha=0.5,label='raw - after_cat_NOx')

ax[0].legend()
ax[1].legend()
ax[2].legend()
ax[3].legend()
