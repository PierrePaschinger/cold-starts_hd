import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from pathlib import Path
import glob
import os


# ==============================================================================
# === Main for standalone running ==============================================
# ==============================================================================
def func_divide(x, y):
    return np.divide(x, y, out=np.zeros_like(x), where=y > 0)


if __name__ == "__main__":
    project_name = 'sems-rde'

    input_path = Path(f'../data/{project_name}/input')
    out_path = Path(f'../data/{project_name}/output')
    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    list_vehicle_file = glob.glob(str(input_path / '*.pkl'))
    list_valid_RDE_sessions = [[175753]]

    for a_file, valid_RDE_sessions in zip(list_vehicle_file, list_valid_RDE_sessions):
        file_input_path = a_file
        file_name = os.path.splitext(os.path.basename(file_input_path))[0]
        vehicle_file_prefix = file_name[:17]
        title_prefix = vehicle_file_prefix

        print(f'====== {title_prefix} =====')
        # Load and Sort Data
        df_raw = pd.read_pickle(file_input_path)
        df_raw['datetime'] = pd.to_datetime(df_raw['datetime'])
        df_raw = df_raw.sort_values(by='datetime')

        # Data filtering : Drop idle data
        df = df_raw.loc[df_raw['RPM'] > 0].copy()

        # Data filtering : Replace nan values
        df = df.replace([np.inf, -np.inf], np.nan).dropna(subset=['CO2_mF', 'after_cat_NOx_mF',
                                                                  'RPM', 'velocity_filtered'])

        # Data filtering: Only when sensor is ready
        df = df.loc[(df['after_cat_rdyNOx'] == 1) & (df['after_cat_rdyO2'] == 1)]

        df_per_session = df.groupby('session').apply(lambda x: pd.Series({
            'total_duration': (x['datetime'].max() - x['datetime'].min()).total_seconds(),
            'total_dist': (x['velocity_filtered'].sum() / 3600),
            'mean_vel': x['velocity_filtered'].mean(),
            'NOx_mg_km': func_divide(x['after_cat_NOx_mF'].sum(), x['velocity_filtered'].sum() / 3600)
        }))
        # Filter to sesssions with distaance > 1km
        df_per_session_filt = df_per_session.loc[df_per_session['total_dist'] > 1]

        df_per_session_byaveNOx = df_per_session_filt.sort_values('NOx_mg_km').reset_index()
        df_per_session_byaveNOx['perc_trips'] = df_per_session_byaveNOx.index.values / len(df_per_session_byaveNOx) * 100

        df_per_session_byaveNOx_validRDE = df_per_session_byaveNOx.loc[df_per_session_byaveNOx['session'].isin(valid_RDE_sessions)]

        ## PLOTS
        total_sessions = len(df_per_session_filt)
        fig, ax = plt.subplots()
        ax.bar(df_per_session_byaveNOx['perc_trips'], df_per_session_byaveNOx['NOx_mg_km'],
               label='Average NOx per trip')
        ax.bar(df_per_session_byaveNOx_validRDE['perc_trips'], df_per_session_byaveNOx_validRDE['NOx_mg_km'],
               color='k', label='Average NOx for Valid RDE')
        ax.axhline(y=105, linestyle='--', color='r', linewidth=2,
                     label='Euro VI N1-II Limit')
        ax.set(title=f'{title_prefix} - {total_sessions} trips\nAverage NOx emission per trip(>1 km) in ascending order',
               xlabel='Percentile of total trips [%]',
               ylabel='Average NOx per trip [mg/km]')
        ax.legend()
        filename = f'{title_prefix}_NOx_waterfall.png'
        fig.savefig(out_path / filename, bbox_inches='tight', dpi=300)
