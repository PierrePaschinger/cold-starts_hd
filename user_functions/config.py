import pandas as pd


class Config(object):
    '''  Read configuration file in the folder with CSV files '''

    def __init__(self):
        self.delimiter = ';'
        self.header = None
        self.skiprows = []
        self.skipinitialspace = True
        self.encoding = None
        self.inputfile = ''

        self.vehicleID = ''
        self.vehicleType = ''
        self.systemType = ''
        self.systemID = ''
        self.softVersion = ''
        self.NOx_ID_1 = ''
        self.NOx_ID_2 = ''
        self.NH3_ID_1 = ''
        self.NH3_ID_2 = ''
        self.O2_ID_1 = ''
        self.O2_ID_2 = ''
        self.connectorT1 = ''
        self.connectorT2 = ''
        self.engineID = ''
        self.exists = False

    def load(self):
        ## read csv into dataframe
        try:
            df = pd.read_csv(self.inputfile, sep=self.delimiter, encoding=self.encoding,
                             skipinitialspace=self.skipinitialspace, header=self.header, names=['name', 'value'],
                             skiprows=self.skiprows, index_col=False)
        except:
            self.exists = False
            # print('No configuration file META.INI found in '+str(self.inputfile)+'...skip')
            return

        ## remove tabs and whitespace
        for i in list(df):
            df[i] = pd.core.strings.str_strip(df[i])

        df.set_index('name', drop=True, inplace=True)
        mydict = df.T.to_dict()
        # print(mydict)
        for i in mydict.keys():
            # print(i+" --> "+str(mydict[i]['value']))
            setattr(self, i, mydict[i]['value'])
        self.exists = True


class MySettings:
    """
    Read connection configuration file from current folder
    """

    def __init__(self, path_to_file: str):
        self.delimiter = ';'
        self.header = None
        self.skiprows = []
        self.skipinitialspace = True
        self.encoding = None
        self.inputfile = path_to_file

        self.dbhost = ''
        self.dbuser = ''
        self.dbpassword = ''
        self.dbschema = ''
        self.sftphost = 'd-tnomar.host-ed.eu'
        self.sftpport = 22622
        self.sftpuser = ''
        self.sftppassword = ''
        self.sftppath = ''
        self.logpath = '../logfiles'
        self.exists = False

        # def load(self):
        """
        Load CSV file store in a dataframe (same as Config class?)
        """
        ## read csv into dataframe

        df = pd.read_csv(self.inputfile, sep=self.delimiter, encoding=self.encoding,
                             skipinitialspace=self.skipinitialspace, header=self.header, names=['name', 'value'],
                             skiprows=self.skiprows, index_col=False)


        ## remove tabs and whitespace
        for i in list(df):
            df[i] = pd.core.strings.str_strip(df[i])

        df.set_index('name', drop=True, inplace=True)
        mydict = df.T.to_dict()
        # print(mydict)
        for i in mydict.keys():
            # print(i+" --> "+str(mydict[i]['value']))
            setattr(self, i, mydict[i]['value'])
        self.exists = True


import os, sys


def create_my_settings(my_settings_file: str) -> MySettings:
    """
    Each user will have a mysettings.cnf. If this file exists, make the settings for further usage. If this file does not exist, exit the program.

    :param my_settings_file: this is the path where mysettings.cnf should be located, taken from one of the arguments in main()
    :return mysettings: if successful, there will be a mysettings object containing the correct credentials
    """

    if my_settings_file[1] != ':':  # full path?
        my_settings_file = os.path.join(os.getcwd(), my_settings_file.strip('\\'))  # make full path
    if not (os.path.exists(my_settings_file)):
        sys.exit('Settings file or path %s does not exist; exit.' % my_settings_file)
    mysettings = MySettings(my_settings_file)
    mysettings.logpath = str(mysettings.logpath)  # if omitted it becomes float nan
    if len(mysettings.logpath) > 3 and mysettings.logpath[1] != ':':  # full path?
        mysettings.logpath = os.path.join(os.getcwd(), mysettings.logpath)  # make full path
    if not os.path.exists(mysettings.logpath):
        mysettings.logpath = ''

    return mysettings
