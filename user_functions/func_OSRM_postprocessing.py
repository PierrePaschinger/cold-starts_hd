# -*- coding: utf-8 -*-
"""
based on:
----
Created on Wed Sep 11 10:02:34 2019
@author: eijkev
----
Adjusted for post-processing on Fri Nov 29 2019
by paalvastm
----
Edited to do column-wise operations on Tues March 24 2020
by ruiterjmd
"""

import glob
import json
import os
import urllib.request

import numpy as np
import pandas as pd
import datetime

chunksize = 100
seconds = 10
radius = '15'
from path import Path
import time


def osrm_postprocessing(df_csv,

                        gps_lat_col='GPSlat', gps_lon_col='GPSlon',
                        interpolate_speed='True',
                        datetime_='datetime'):
    ## select GPS coordinates and date and time
    df_csv.reset_index(drop=True, inplace=True)
    coords = df_csv[[gps_lat_col, gps_lon_col, datetime_]]
    datetime = df_csv[datetime_]

    ## use the gps coordinates to match with OSRM

    start = time.time()
    print(f' ------- Started OSRM: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}')
    coords_osrm, nToOSRM_osrm, nError_osrm = match_osrm(coords, gps_lon_col, gps_lat_col)
    end = time.time()
    print(f' ------- Finished OSRM: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}')
    i_time_interval = (end - start)
    print(
        ' ------ Data download {} lines took {:.0f} seconds = {:.2f} hours'.format(
            len(coords), i_time_interval, i_time_interval / 3600))
    i_start = time.time()

    coords_osrm.replace('', np.nan, inplace=True)

    df_test = df_csv.merge(coords_osrm,
                           on=[gps_lon_col, gps_lat_col, datetime_], how='left')

    df_csv['max_speed'] = df_test['maxspeed']
    df_csv['max_speed_conditional'] = df_test['maxspeed:conditional']
    df_csv['number'] = df_test['ref']
    df_csv['name'] = df_test['name']
    ## correct the maximum velocity for the conditional maximum speed

    df_csv['speed_limit'], a_suffix = get_vmax(df_csv, datetime_)


    ## add the maximum speed and road type to the SEMS input
    df_submitted, df_csv['road_type'], the_suffix = define_roadtype_2(df_test)

    if the_suffix == '' and not a_suffix == '':
        the_suffix = a_suffix

    if interpolate_speed:
        df_csv['max_speed'].fillna(method='ffill', limit=5, inplace=True)
        df_csv['max_speed'].fillna(method='bfill', limit=5, inplace=True)

        df_csv['speed_limit'].fillna(method='ffill', limit=5, inplace=True)
        df_csv['speed_limit'].fillna(method='bfill', limit=5, inplace=True)

        df_csv['name'].fillna(method='ffill', limit=5, inplace=True)
        df_csv['name'].fillna(method='bfill', limit=5, inplace=True)

        df_csv['number'].fillna(method='ffill', limit=5, inplace=True)
        df_csv['number'].fillna(method='bfill', limit=5, inplace=True)
    i_end = time.time()
    print(' ------- Processing took {:.0f} seconds'.format(i_end - i_start))
    return df_csv, the_suffix


def get_vmax(df, datetime_='datetime', max_speed_='max_speed',
             max_speed_conditional_='max_speed_conditional'):
    ## this function replaces the maximum speed with the conditional maximum speed if ride
    ## is within the conditional time range

    df_temp = df[[datetime_, max_speed_, max_speed_conditional_]].copy()
    df_temp[max_speed_conditional_] = df_temp[max_speed_conditional_].astype(str)
    df_temp['speed_limit'] = df_temp[max_speed_].copy()

    try:
        df_temp.loc[((df_temp[max_speed_conditional_].str.contains(':00', na=False))),
                    'condition'] = df_temp[max_speed_conditional_]
        df_temp.loc[((df_temp['condition'].str.contains(';', na=False))),
                    'condition'] = df_temp.loc[((df_temp['condition'].str.contains(';', na=False))),
                    'condition'].str.split(';').str[0].str.strip()


        df_temp['conditional_speed'] = df_temp['condition'].str.split('@').str[0].str.strip()
        df_temp['t_f'] = \
            df_temp['condition'].str.split('@').str[1].str[-6:-4].str.strip(
                ')').astype(float)  # condition ends
        df_temp['t_i'] = \
            df_temp['condition'].str.split('@').str[1].str[-12:-10].str.strip(
                ')').astype(float)  # condition starts
        # --- if t_i <= t < t_f then apply conditional speed ---
        df_temp.loc[
            (df_temp[datetime_].dt.hour.astype(float) >= df_temp['t_i']) &
            (df_temp[datetime_].dt.hour.astype(float) < df_temp['t_f']),
            'speed_limit'] = df_temp['conditional_speed'].copy()
        # --- if there's a Mo - Fr, set Sat/Sun to not condtional ---
        df_temp.loc[(df_temp[max_speed_conditional_].str.contains('Mo', na=False)) &
                    (df_temp[datetime_].dt.weekday > 4),
                    'speed_limit'] = df_temp[max_speed_].copy()
        suffix_if_failed = ''
        return df_temp['speed_limit'].copy(), suffix_if_failed
    except:
        suffix_if_failed = '_conditional_failed'
        return df_temp['speed_limit'].copy(), suffix_if_failed


def define_roadtype_2(df_, maxspeed_='maxspeed', highway_='highway'):
    # === define the road type base on the maximum speed and the OSRM road input
    df_temp = df_[[maxspeed_, highway_]].copy()
    suffix_if_failed = ''
    if not df_[maxspeed_].isnull().all():
        df_temp.loc[df_temp[maxspeed_].str.len() < 4, 'max_speed_numeric'] = df_temp[
            maxspeed_].copy()
        df_temp['max_speed_numeric'] = pd.to_numeric(df_temp['max_speed_numeric'], errors='coerce')

        # === assign road types wrt numeric ========================================
        df_temp.loc[df_temp['max_speed_numeric'] > 80, 'road'] = 'Motorway'
        df_temp.loc[df_temp['max_speed_numeric'] < 60, 'road'] = 'Urban'
        df_temp.loc[(df_temp['max_speed_numeric'] >= 60) &
                    (df_temp['max_speed_numeric'] < 80), 'road'] = 'Rural'
        df_temp.loc[(df_temp['max_speed_numeric'] == 80) &
                    (df_temp[highway_] == 'motorway'), 'road'] = 'Motorway'
        df_temp.loc[(df_temp['max_speed_numeric'] == 80) &
                    ~(df_temp[highway_] == 'motorway'), 'road'] = 'Rural'

        # === check other strings ==================================================
        df_temp.loc[(df_temp[maxspeed_].str.endswith('urban', na=False)), 'road'] = 'Urban'
        df_temp.loc[(df_temp[maxspeed_] == 'none'), 'road'] = 'Motorway'

    if df_[maxspeed_].isnull().all():
        df_temp.loc[((df_temp[highway_] == 'primary') | (df_temp[highway_] == 'secondary')
                     ), 'road'] = 'Rural'
        df_temp.loc[(df_temp[highway_] == 'residential'), 'road'] = 'Urban'
        suffix_if_failed = '_no_limits'
        print('==== OSRM ERROR roads: {}, limits: {} ==='.format(
            df_[highway_].unique(),
            df_[maxspeed_].unique()))

    df_temp['road'].fillna(method='ffill', limit=5, inplace=True)
    df_temp['road'].fillna(method='bfill', limit=5, inplace=True)
    df_temp['road'].fillna(method='ffill', limit=5, inplace=True)
    df_temp['road'].fillna(method='bfill', limit=5, inplace=True)
    df_temp['road'].fillna(method='ffill', limit=5, inplace=True)
    df_temp['road'].fillna(method='bfill', limit=5, inplace=True)

    df_temp['road'].fillna('Unknown', inplace=True)

    return df_temp, df_temp['road'], suffix_if_failed


# def define_roadtype(vmax, roadtype_osrm):
#     ## define the road type base on the maximum speed and the OSRM road input
#     wegtype = roadtype_osrm.copy()
#     for i in wegtype.index:
#         if (type(vmax.loc[i]) == str) and (len(vmax.loc[i]) > 0) and (len(vmax.loc[i]) < 4):
#             # print(vmax.loc[i])
#
#             if int(vmax.loc[i]) > 80:
#                 wegtype.loc[i] = 'Motorway'
#
#             elif int(vmax.loc[i]) < 60:
#                 wegtype.loc[i] = 'Urban'
#
#             elif int(vmax.loc[i]) >= 60 & int(vmax.loc[i]) < 80:
#                 wegtype.loc[i] = 'Rural'
#
#             elif int(vmax.loc[i]) == 80:
#                 if roadtype_osrm.loc[i] == 'motorway':
#                     wegtype.loc[i] = 'Motorway'
#                 else:
#                     wegtype.loc[i] = 'Rural'
#         elif (type(vmax.loc[i]) == str) and (len(vmax.loc[i]) == 4):
#             wegtype.loc[i] = 'Motorway'
#         else:
#             wegtype.loc[i] = 'Unknown'
#
#     return wegtype


def match_osrm(coords, gps_lon_col, gps_lat_col):
    ## match coordinates to OSRM
    coords = coords[::seconds].copy().dropna(axis=0)
    nToOSRM = len(coords)
    nError = 0
    for i in ['highway', 'ref', 'name', 'maxspeed', 'maxspeed:conditional', 'maxspeed:variable',
              'maxspeed:hgv', 'junction', 'bridge', 'tunnel']:
        coords[i] = np.nan
    Nchunks = int(len(coords) / (chunksize)) + 1
    for c in range(Nchunks):
        these_coords = coords.iloc[
                       c * chunksize - 2 * c:min(len(coords), (c + 1) * chunksize - c * 2)]
        coords_str = ';'.join([f'{r[gps_lon_col]:.6f}' + ',' + f'{r[gps_lat_col]:.6f}' for i, r in
                               these_coords.iterrows()])

        string = 'http://pc-15237.tsn.tno.nl:5000/match/v1/driving/'
        string = string + coords_str + '?'
        string = string + 'radiuses=' + ';'.join([radius for i in these_coords.index])
        string = string + '&annotations=nodes'

        try:
            response = urllib.request.urlopen(string)
            data = json.loads(response.read())
            for i in range(len(these_coords)):
                wp = data['tracepoints'][i]
                if type(wp) == dict:
                    coords.loc[these_coords.index[i], 'X'] = wp['location'][0]
                    coords.loc[these_coords.index[i], 'Y'] = wp['location'][1]
                    coords.loc[these_coords.index[i], ['highway', 'ref', 'name', 'maxspeed',
                                                       'maxspeed:conditional', 'maxspeed:variable',
                                                       'maxspeed:hgv', 'junction', 'bridge',
                                                       'tunnel']] = wp[
                        'name'].split('|')
                    if wp['waypoint_index'] > 0:
                        coords.loc[these_coords.index[i], 'nodes'] = str(
                            data['matchings'][wp['matchings_index']]['legs'][
                                wp['waypoint_index'] - 1]['annotation']['nodes'])
                        coords.loc[these_coords.index[i], 'distance'] = str(
                            data['matchings'][wp['matchings_index']]['legs'][
                                wp['waypoint_index'] - 1]['distance'])
        except Exception as e:
            nError += len(these_coords)
            # print(f'{e} - {string}')
    return coords, nToOSRM, nError


def clean_up_gps_coordinates(df_raw, gps_col_lat='GPSlat', gps_col_lon='GPSlon',
                             gps_col_fix='GPSfix'):
    maxspeed = 1000  # km/h
    maxdeg = maxspeed / 35000 * 360 / 3600
    max_length = 6

    df_raw.loc[df_raw[gps_col_fix] == 0, gps_col_lat] = np.nan
    df_raw.loc[df_raw[gps_col_fix] == 0, gps_col_lon] = np.nan

    for i in range(1, max_length + 1):
        mask_1 = (df_raw[gps_col_lat].shift(i).diff(-i).abs() > (maxdeg * i))
        mask_2 = (df_raw[gps_col_lon].shift(i).diff(-i).abs() > (maxdeg * i))
        df_raw.loc[mask_1, [gps_col_lat, gps_col_lon]] = np.nan

        df_raw.loc[mask_2, [gps_col_lat, gps_col_lon]] = np.nan

    return df_raw


def get_location_data(df_,
                      lon_min=3.3, lon_max=7.6,
                      lat_min=50.5, lat_max=54):
    df_ = df_.loc[
        (df_['GPSlon'] > lon_min) &
        (df_['GPSlon'] < lon_max) &
        (df_['GPSlat'] > lat_min) &
        (df_['GPSlat'] < lat_max)
        ].copy()
    return df_


if __name__ == '__main__':
    # === Jessica's ===
    data_path = Path('C:/Users/Ruiterjmd/python_projects')
    # in_path = data_path / 'data/integrator/input'
    out_path = Path(
        r'C:\Users\ruiterjmd\TNO\TKI Integrator UMneo Conn. Truck Trials - Team\3 Working '
        r'documents\HUMAN INTERACTION\CAN_data')

    in_path = Path(r'\\tsn.tno.nl\data\users\ruiterjmd\data_unix\ld_vehicles')

    delimiter = ','
    gps_lat = 'GPSlat'
    gps_lon = 'GPSlon'
    # input_file = 'group_data_10-09-19-10-11-19.pkl'
    # output_file = in_path / '{}_roads.pkl'.format(input_file[:-4])

    plates = [
        # 'DA_XF_7920KXB_EURVI',  # Getru 1
        # 'DA_XF_3100KVF_EURVI',  # Getru 2
        # 'DA_XF_18BFT5_EURVI',  # Starmans 1
        # 'DA_XF_58BJN6_EURVI',  # Starmans 2
        # 'DA_XF_77BGH7_EURVI',  # De Rijke
        'DA_XF_88BLD6_EURVI',  # Overbeek
    ]

    for plate in plates:
        files_to_investigate = glob.glob(in_path / (f'ME_B1*.pkl'))

        for input_file_name in files_to_investigate:
            file_name = os.path.splitext(os.path.basename(input_file_name))[0]
            print(file_name)

            df_data = pd.read_pickle(input_file_name)

            output_file = in_path / '{}_roads.pkl'.format(file_name)

            setdata, osrm_fail_suffix = osrm_postprocessing(setdata)

            setdata.to_pickle(out_path / '{}_out_{}-{}{}.pkl'.format(
                file_name,
                setdata.datetime.min().strftime(format='%d%m%y'),
                setdata.datetime.max().strftime(format='%d%m%y'),
                osrm_fail_suffix))  # write pickle file
