# import matplotlib as mpl

# mpl.use('Agg')
from PIL import Image
from matplotlib.offsetbox import (OffsetImage, AnchoredOffsetbox)


def add_tno_watermark_to_figure(fig, ax, image_location='input/TNO_zwart.png'):
    img = Image.open(image_location)
    width, height = ax.figure.get_size_inches() * fig.dpi
    wm_width = int(width / 8)  # make the watermark 1/8 of the figure width
    scaling = (wm_width / float(img.size[0]))
    wm_height = int(float(img.size[1]) * float(scaling))
    img = img.resize((wm_width, wm_height), Image.ANTIALIAS)

    imagebox = OffsetImage(img, zoom=1, alpha=0.2)
    imagebox.image.axes = ax

    ao = AnchoredOffsetbox(1, pad=0.01, borderpad=0, child=imagebox)
    ao.patch.set_alpha(0)
    ax.add_artist(ao)
    return True
