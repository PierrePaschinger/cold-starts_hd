'''
Colours we're using

NOx = C0 (/s : alpha = 0.5)
exhaust temp = C1
CO = C2
PN = C4
CEL = C6
NH3 = C8 (/s : alpha = 0.5)

ECT = red, alpha 0.2
speed = gray, alpha = 0.5



cumulative = C0
time = C1, alpha=0.5
instantaneous = C0/C4, alpha = 0.5
'''


def change_axis_colour(ax_, colour, side='right'):
    ax_.spines[side].set_color(colour)
    ax_.yaxis.label.set_color(colour)
    ax_.tick_params(axis='y', colors=colour)
