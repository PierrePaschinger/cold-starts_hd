import datetime
import time
from functools import partial
from multiprocessing import Pool
from pathlib import Path
import numpy as np

import pandas as pd
import sqlalchemy
from sqlalchemy.engine.url import URL
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker

import user_functions.sems_rest_api as sems_rest_api


def make_database_session_connection(mysettings, db):
    """
    Make a connection to the database using SQL Alchemy

    :param mysettings: contains credentials from my_settings.cnf to connect to the correct database
    :param db: type of database, whether running public database or database for testing purpose etc
    :return engine: the database engine running the session?
    :return db_sess: database session where all the tables can be accessed
    """
    Base = declarative_base()
    my_db = URL(drivername='mysql+pymysql',
                host=mysettings.dbhost,
                username=mysettings.dbuser,
                password=mysettings.dbpassword,
                database=db)
    engine = sqlalchemy.create_engine(name_or_url=my_db,
                                      connect_args={'connect_timeout': 60},
                                      pool_recycle=1800)
    engine.execute("USE " + db)
    Base.metadata.bind = engine
    db_session = sessionmaker(bind=engine)
    db_sess = db_session()
    return engine, db_sess


def limit_dates_list(start_date: str, end_date: str, sessions_: list):
    """
    Get limit sessions to a certain date range

    :param start_date: DD/MM/YYYY - date at beginning of date range
    :param end_date: DD/MM/YYYY - date at end of date range
    :param sessions_: sessions list
    :return: the subset of the sessions dataframe
    """
    start_d = int(datetime.datetime.strptime(start_date, "%d/%m/%Y").timestamp()
                  * 1000)
    end_d = int(datetime.datetime.strptime(end_date, "%d/%m/%Y").timestamp()
                * 1000 + 86400000)  # end of the day
    session_id_time = [i.sessionid for i in sessions_ if (i.datetime_start >= start_d and
                                                          i.datetime_end <= end_d)]
    return session_id_time


def get_df_from_online(session_sublists, raw_columns, proc_columns, calc_columns, get_electric=0):
    pool = Pool(4)
    # print('------ Reading raw data')
    start = time.time()
    fields_list = raw_columns
    raw_s = pool.map(partial(sems_rest_api.get_sems_raw_measurements, fields=fields_list),
                     session_sublists)
    rawdata = pd.concat(raw_s, sort=True)  # concatenate raw dfs
    rawdata.reset_index(inplace=True, drop=True)
    end = time.time()
    print('------ Raw data download took {:.0f} seconds'.format(end - start))

    # print('------ Reading processed data')
    fields_list = proc_columns
    start = time.time()
    procs = pool.map(partial(sems_rest_api.get_sems_processed_measurements, fields=fields_list),
                     session_sublists)
    procdata = pd.concat(procs, sort=True)
    procdata.reset_index(inplace=True, drop=True)
    end = time.time()
    print('------ Processed data download took {:.0f} seconds'.format(end - start))

    # print('------ Reading calculated data')
    fields_list = calc_columns
    start = time.time()
    calcs = pool.map(partial(sems_rest_api.get_sems_calculated_measurements, fields=fields_list),
                     session_sublists)
    calcdata = pd.concat(calcs, sort=True)
    calcdata.reset_index(inplace=True, drop=True)
    end = time.time()
    print('------ Calculated data download took {:.0f} seconds'.format(end - start))

    # print('------ Reading additional signals (if applicable)')
    adds = pool.map(partial(sems_rest_api.get_sems_additional_signals),
                    session_sublists)
    adddata = pd.concat(adds, sort=True)
    adddata.reset_index(inplace=True, drop=True)
    end = time.time()
    print('------ Download took {:.0f} seconds'.format(end - start))

    if get_electric:
        # print('------ Reading electric data (if applicable)')
        start = time.time()
        elecs = pool.map(partial(sems_rest_api.get_sems_electric_measurements),
                         session_sublists)
        elecdata = pd.concat(elecs, sort=True)
        elecdata.reset_index(inplace=True, drop=True)
        end = time.time()
        print('------ Electric download took {:.0f} seconds'.format(end - start))

    # connection.close()  # to avoid timeout. Next vehicle the connection will be restored.

    # === Merge raw, processed, calculated =====================================
    print(' ------ Merging dataframes')
    procdata.drop('id', axis=1, inplace=True)  # not needed, will become triple entry
    calcdata.drop('id', axis=1, inplace=True)  # not needed, will become triple entry
    if get_electric:
        if not elecdata.empty:
            elecdata.drop('id', axis=1, inplace=True)  # not needed, will become triple entry

    cols_to_use = rawdata.columns.difference(procdata.columns).tolist()
    cols_to_use.append('datetime')
    temp_data = pd.merge(procdata, rawdata[cols_to_use], how='inner', on=[
        'datetime'])  # inner because raw data may exist without corresponding calculated data
    cols_to_use = calcdata.columns.difference(temp_data.columns).tolist()
    cols_to_use.append('datetime')
    set_data = pd.merge(temp_data, calcdata[cols_to_use], how='inner', on=['datetime'])

    if not adddata.empty:
        cols_to_use = adddata.columns.difference(set_data.columns).tolist()
        cols_to_use.append('datetime')
        set_data = pd.merge(set_data, adddata[cols_to_use], how='inner', on=['datetime'])

    if get_electric:
        if not elecdata.empty:
            cols_to_use = elecdata.columns.difference(set_data.columns).tolist()
            cols_to_use.append('datetime')
            set_data = pd.merge(set_data, elecdata[cols_to_use], how='inner', on=['datetime'])

    if 'vss' in list(set_data):
        set_data.loc[:, 'VSS'] = set_data['vss']

    print("------ %d rows imported from database" % len(set_data))

    # === tidy merged dataframe ================================================
    set_data.sort_values('datetime', axis=0,
                         inplace=True)  # necessary sorting, set_data comes unsorted
    set_data = set_data.loc[:, ~set_data.columns.duplicated()]
    set_data = set_data.dropna(axis=1, how='all')
    set_data.reset_index(inplace=True, drop=True)
    set_data['datetime'] = pd.to_datetime(set_data['datetime'],
                                         unit='ms')  # convert microseconds to datetime
    set_data.loc[:, 'timecount'] = set_data.index
    if not set_data['RPM'].isna().all():
        firstengineon = set_data[set_data['RPM'] > 0]['datetime'].values[0]
        setdata = set_data[set_data['datetime'] >= firstengineon]
    setdata.loc[:, 'timecount'] = setdata.index - setdata.index.min()
    setdata.fillna(value=np.NaN, inplace=True)  # set None to np.nan

    print(' ------ Total dataset has %d records' % (len(setdata)))

    return set_data
