import datetime
import os
import sys

import pandas as pd
import numpy as np

import user_functions.config as config


def limit_dates(start_date: str, end_date: str, df_sessions_):
    """
    Get limit sessions to a certain date range

    :param start_date: DD/MM/YYYY - date at beginning of date range
    :param end_date: DD/MM/YYYY - date at end of date range
    :param df_sessions: sessions dataframe
    :return: the subset of the sessions dataframe
    """
    start_d = int(datetime.datetime.strptime(start_date, "%d/%m/%Y").timestamp()
                  * 1000)
    end_d = int(datetime.datetime.strptime(end_date, "%d/%m/%Y").timestamp()
                * 1000 + 86400000)  # end of the day
    df_sessions_ = df_sessions_.loc[(df_sessions_['datetime_start'] >= start_d)
                                    & (df_sessions_['datetime_end'] <= end_d)].copy()
    return df_sessions_


def limit_dates_time(start_date: str, end_date: str, df_sessions_):
    """
    Get limit sessions to a certain date range

    :param start_date: DD/MM/YYYY - date at beginning of date range
    :param end_date: DD/MM/YYYY - date at end of date range
    :param df_sessions: sessions dataframe
    :return: the subset of the sessions dataframe
    """
    start_d = int(datetime.datetime.strptime(start_date, "%d/%m/%Y %H:%M").timestamp()
                  * 1000)
    end_d = int(datetime.datetime.strptime(end_date, "%d/%m/%Y %H:%M").timestamp()
                * 1000)
    df_sessions_ = df_sessions_.loc[(df_sessions_['datetime_start'] >= start_d)
                                    & (df_sessions_['datetime_end'] <= end_d)].copy()
    return df_sessions_


def read_settings_file(mysettingsfile):
    mysettings = config.MySettings(mysettingsfile)
    if mysettingsfile[1] != ':':  # full path?
        mysettingsfile = os.path.join(os.getcwd(),
                                      mysettingsfile.strip('\\'))  # make full path
    if not (os.path.exists(mysettingsfile)):
        sys.exit('Settings file or path %s does not exist; exit.' % mysettingsfile)
    try:
        mysettings.inputfile = mysettingsfile
    except:
        sys.exit('Settings file %s cannot be found; exit.' % mysettingsfile)
    # mysettings.load()
    if mysettings.dbschema == '':
        sys.exit(
            'No valid database connection information found in settings file %s; exit.' %
            mysettingsfile)
    else:
        db = mysettings.dbschema
    return mysettings, db


def get_sessions(vehicle, engine):
    print('=== Reading information of vehicle %s from database ===' % vehicle)
    temp = pd.read_sql('SELECT id FROM vehicle WHERE vehicleid=\"' + vehicle + '\"',
                       engine)
    if len(temp) == 0:
        print('No vehicles found that match the description. Exiting.')
        sys.exit()
    vehicleid = str(temp.id.values[0])
    vehicleinfo = pd.read_sql('SELECT * from hd WHERE vehicle=\"' + vehicleid + '\"',
                              engine)
    if vehicleinfo.empty:
        vehicleinfo = pd.read_sql(
            'SELECT * from ld WHERE vehicle=\"' + vehicleid + '\"',
            engine)
    vehicleinfo.loc[:, 'vehicleID'] = vehicle  # no longer in hd table
    engineinfo = pd.read_sql('SELECT * from engine WHERE vehicle=\"' + vehicleid + '\"',
                             engine)

    print(' ------ Reading session table and identifying trips for vehicle %s' % vehicle)
    sessions = pd.read_sql(
        'SELECT * FROM session WHERE engine=\"' + str(engineinfo.id.values[0]) + '\"',
        engine)
    return sessions


def get_dataframe(engine, vehicle, sessions, lasttrip, rawcolumns, proccolumns, calccolumns):
    list_years_not_in_quarterly_tables = ['2019', '2020', '2021',  # this year, edit when moved to
                                          # quarterly
                                          '2015', '2016', '2017', '2010', '1980']
    list_years_in_base_table = ['2019', '2020', '2021',  # this year, edit when moved to quarterly
                                '2015', '2016', '2010', '1980']
    # 2010 for VD170 first trip, 1980 for caddy

    sessions.sort_values(by='datetime_start', inplace=True)
    sessions['sessionID_merged'] = sessions['sessionid']
    sessions['firstfilename'] = sessions['filename']
    sessions['season'] = (pd.to_datetime(sessions['datetime_start'],
                                         unit='ms').dt.year).astype(str) + '_q' + (
                             (((pd.to_datetime(sessions['datetime_start'],
                                               unit='ms').dt.month) + 2) / 3).astype(
                                 int)).astype(
        str)  # round to 0 decimals rounds to even numbers... "bankers rounding". Ridiculous.
    for i_year in list_years_not_in_quarterly_tables:
        sessions.loc[sessions['season'].str.contains(f"{i_year}"), 'season'] = f'{i_year}'

    if lasttrip > 0:
        sessions = sessions[0:lasttrip]

    rawdata = pd.DataFrame()
    procdata = pd.DataFrame()
    calcdata = pd.DataFrame()
    addsdata = pd.DataFrame()

    for i in sessions['season'].unique():
        print(" ------ Reading data from " + i)
        if i in list_years_in_base_table:
            rawtable = 'sems_raw_measurement'
            proctable = 'sems_processed_measurement'
            calctable = 'sems_calculated_measurement'
            addtable1 = 'sems_additional_signals'
            addtable2 = 'sems_additional_signal_names'
        else:
            rawtable = 'sems_data_sems_raw_measurement_' + i
            proctable = 'sems_data_sems_processed_measurement_' + i
            calctable = 'sems_data_sems_calculated_measurement_' + i
            addtable1 = 'sems_additional_signals'
            addtable2 = 'sems_additional_signal_names'

        rawquery = 'SELECT ' + rawcolumns + ' FROM ' + rawtable + \
                   ' AS raw WHERE raw.session in '
        procquery = 'SELECT ' + proccolumns + ' FROM ' + proctable + \
                    ' AS proc WHERE proc.session in '
        calcquery = 'SELECT ' + calccolumns + ' FROM ' + calctable + \
                    ' AS calc WHERE calc.session in '

        # Additional signals (for example: OBD NOx)
        addquery = 'SELECT * FROM ' + addtable1 + ' WHERE additional_names in '#({})'.format(sessionlist)
        addsnamequery = 'SELECT * FROM ' + addtable2 + ' WHERE session in '#({}) LIMIT 1'.format(addtable2, sessionlist)

        sessionlist = str(list(sessions.loc[sessions['season'] == i, 'id'])).strip(
            '["]')
        print(' ------ {}'.format(len(sessions.loc[sessions['season'] == i, 'id'])))
        raws = pd.read_sql(rawquery + '(' + str(sessionlist) + ')', engine)
        rawdata = pd.concat([rawdata, raws], axis=0)

        procs = pd.read_sql(procquery + '(' + str(sessionlist) + ')', engine)
        procdata = pd.concat([procdata, procs], axis=0)

        calcs = pd.read_sql(calcquery + '(' + str(sessionlist) + ')', engine)
        calcdata = pd.concat([calcdata, calcs], axis=0)

        adds = pd.read_sql(addquery + '({})'.format(sessionlist), engine)
        addsdata = pd.concat([addsdata, adds], axis=0)
        addsnames = pd.read_sql(addsnamequery + '({}) LIMIT 1'.format(sessionlist), engine)
        addsnames.insert(loc=14, column='datetime', value=['datetime' for i in range(addsnames.shape[0])])
        addsnames.iloc[:, 13] = 'session'
        addsdata.columns = addsnames.iloc[0]


    rawdata.reset_index(inplace=True, drop=True)
    procdata.reset_index(inplace=True, drop=True)
    calcdata.reset_index(inplace=True, drop=True)
    addsdata.reset_index(inplace=True, drop=True)

    # === Clean and combine dataframes =================================

    print(' ------ Merging dataframes')
    procdata.drop('id', axis=1, inplace=True)  # not needed, will become triple entry
    calcdata.drop('id', axis=1, inplace=True)  # not needed, will become triple entry

    # LET OP: EERST PROCESSED DATA, anders wordt de raw data als uitgangspunt genomen bij gelijke
    # kolomnamen (ECT bijvoorbeeld).

    # === Merge dfs ===

    cols_to_use = rawdata.columns.difference(procdata.columns).tolist()
    cols_to_use.append('datetime')
    tempdata = pd.merge(procdata, rawdata[cols_to_use], how='inner', on=[
        'datetime'])  # inner because raw data may exist without corresponding calculated data
    cols_to_use = calcdata.columns.difference(tempdata.columns).tolist()
    cols_to_use.append('datetime')
    setdata = pd.merge(tempdata, calcdata[cols_to_use], how='inner', on=['datetime'])

    cols_to_use = addsdata.columns.difference(setdata.columns).tolist()
    cols_to_use = list(filter(lambda i: (type(i) is str), cols_to_use))
    cols_to_use.remove('0000-00-00 00:00:00.000000')
    cols_to_use.append('datetime')
    setdata = pd.merge(setdata, addsdata[cols_to_use], how='inner', on=['datetime'])

    if 'vss' in list(setdata):
        setdata.loc[:, 'VSS'] = setdata['vss']  # sigh...

    del rawdata
    del procdata
    del calcdata
    del addsdata

    # === prep final dataframe ===
    setdata.sort_values('datetime', axis=0,
                        inplace=True)  # necessary sorting, setdata comes unsorted
    setdata = pd.merge(setdata, sessions[['id', 'sessionid']], left_on='session',
                       right_on='id')
    setdata = setdata.rename(columns={'id_x': 'id', 'id_y': 'sid'})
    # sessionID = \
    #     sessions.loc[
    #         sessions['id'] == setdata['session'].values[0], 'sessionid'].values[0]
    freq = sessions['frequency'].values[0]
    setdata['delta_t'] = freq  # real frequency value

    setdata['datetime'] = pd.to_datetime(setdata['datetime'],
                                         unit='ms')  # convert microseconds to datetime
    setdata['time'] = pd.DatetimeIndex(setdata['datetime']).time
    if not setdata['RPM'].isna().all():
        firstengineon = setdata[setdata['RPM'] > 0]['datetime'].values[0]
        setdata = setdata[setdata['datetime'] >= firstengineon]
    setdata.loc[:, 'timecount'] = setdata.index - setdata.index.min()
    setdata.fillna(value=np.NaN, inplace=True)  # set None to np.nan

    print(' ------ Total dataset for vehicle %s has %d records' % (vehicle, len(setdata)))

    return setdata
