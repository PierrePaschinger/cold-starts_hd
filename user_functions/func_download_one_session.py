"""
Download data
jessica.deruiter@tno.nl
"""

import getopt
import os
import sys
import time
import pandas as pd

import sqlalchemy
import matplotlib.pyplot as plt
from sqlalchemy.engine.url import URL
from pathlib import Path

from user_functions.func_OSRM_postprocessing import *
from user_functions.func_download_data_via_sql import *


def download_one_session(vehicles, a_session, mysettingsfile):
    print(f'###################################################################\n'
          f'START: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')

    list_errors = []
    lasttrip = -1

    rawcolumns = [
        'id',
        'datetime',
        'session',
        # 'RPM',
        'GPSalt',
        'GPSfix',
        'GPSlat',
        'GPSlon',
        'GPSsats',
        'after_cat_rdyNOx',
        'after_cat_rdyO2',
        'NFT',
        'CEL'
    ]
    rawcolumns = str(rawcolumns).replace("'", "").strip('[]')

    proccolumns = [
        'id',
        'datetime',
        # 'after_cat_NOx',
        # 'after_cat_O2',
        'RPM',
        'ECT',
        # 'MAF',
        # 'LFE',
        'AET',
        'after_cat_EGT',
        'LFE'
    ]
    proccolumns = str(proccolumns).replace("'", "").strip('[]')

    calccolumns = [
        'id',
        'datetime',
        # 'total_mF',
        'after_cat_NOx_mF',
        'after_cat_NH3_mF',
        'CO2_mF',
        # 'engine_on',
        # 'engine_hot',
        # 'engine_power',
        'velocity_filtered',
        'acceleration_filtered'
    ]
    calccolumns = str(calccolumns).replace("'", "").strip('[]')

    ## read settings file

    mysettings, db = read_settings_file(mysettingsfile)

    for vehicle in vehicles:

        try:
            mydb = URL(drivername='mysql+pymysql', host=mysettings.dbhost,
                       username=mysettings.dbuser,
                       password=mysettings.dbpassword, database=db)
        except Exception as e:
            print("Cannot define database because: ", e)
        engine = sqlalchemy.create_engine(name_or_url=mydb)
        connection = engine.connect()
        engine.execute("USE " + db)

        # === Get Dataframes ===============================================

        sessions = get_sessions(vehicle, engine)
        if len(sessions.index) == 0: continue

        try:
            sessions = sessions.loc[sessions['sessionid'] == a_session].copy()
            setdata = get_dataframe(engine, vehicle, sessions, lasttrip,
                                    rawcolumns, proccolumns, calccolumns)

        except Exception as e:
            dict_error = {
                'vehicle': vehicle,
                'error': e
            }
            list_errors.append(dict_error.copy())
            continue

        connection.close()  # to avoid timeout. Next vehicle the connection will be restored.

        try:
            setdata = clean_up_gps_coordinates(setdata)
            # setdata = get_location_data(setdata)
            # setdata, osrm_fail_suffix = osrm_postprocessing(setdata)
        except Exception as e:
            dict_error = {
                'vehicle': vehicle,
                'osrm error': e
            }
            list_errors.append(dict_error.copy())
            continue

        return setdata, list_errors


### Main for standalone running
if __name__ == "__main__":

    mysettingssw = Path('../settings') / 'myreadonly.cnf'
    project_name = 'sems-rde'

    out_path = Path(f'../data/{project_name}/input')

    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    the_session = 'VW_CA_VJG59N_EUR6_1_2103231708'
    the_vehicle = ['VW_CA_VJG59N_EUR6']

    # === Start ===

    df, list_error = download_one_session(the_vehicle, the_session, mysettingssw)

