dict_pollutant_names = {
    'THC mass_Analyser [g/s]': 'THC',
    'CO mass_Analyser [g/s]': 'CO',
    'CO2 mass_Analyser [g/s]': 'CO2',
    'NOx mass_Analyser [g/s]': 'NOx',
    'NO2 mass_Analyser [g/s]': 'NO2',
    'NO mass_Analyser [g/s]': 'NO',
    'PN_Analyser [#/s]': 'PN',
    'CO mass_Analyser [mg/s]': 'CO',
    'NOx mass_Analyser [mg/s]': 'NOx',

    'after_cat_NH3_mF': 'NH3',
    'after_cat_NOx_mF': 'NOx',
    'CH4 mass_Analyser [g/s]': 'CH4',
    'NMHC mass_Analyser [g/s]': 'NMHC',
    'O2 mass_Analyser [g/s]': 'O2',
    'CO2 Emissions [g/s]': 'CO2',
    'CO Emissions [mg/s]': 'CO',
    'NOx Emissions [mg/s]': 'NOx',
    'NO Emissions [mg/s]': 'NO',
    'NO2 Emissions [mg/s]': 'NO2',
    'N2O Emissions [mg/s]': 'N2O',
    'PN [#/s]': 'PN',
    'N2O [mg/s]': 'N2O',
    'NH3 [mg/s]': 'NH3',
    'THC [mg/s]': 'THC',
    'CH4 [mg/s]': 'CH4',
    'CO2 [g/s]': 'CO2',
    'CO [mg/s]': 'CO',
}

dict_pollutant_units = {
    'THC mass_Analyser [g/s]': 'g',
    'CO mass_Analyser [g/s]': 'g',
    'CO2 mass_Analyser [g/s]': 'g',
    'NOx mass_Analyser [g/s]': 'g',
    'NO2 mass_Analyser [g/s]': 'g',
    'NO mass_Analyser [g/s]': 'g',
    'PN_Analyser [#/s]': '#',
    'CO mass_Analyser [mg/s]': 'mg',
    'NOx mass_Analyser [mg/s]': 'mg',
    'after_cat_NH3_mF': 'mg',
    'after_cat_NOx_mF': 'mg',
    'CH4 mass_Analyser [g/s]': 'g',
    'NMHC mass_Analyser [g/s]': 'g',
    'O2 mass_Analyser [g/s]': 'g',
    'CO2 Emissions [g/s]': 'g',
    'CO Emissions [mg/s]': 'mg',
    'NOx Emissions [mg/s]': 'mg',
    'NO Emissions [mg/s]': 'mg',
    'NO2 Emissions [mg/s]': 'mg',
    'N2O Emissions [mg/s]': 'mg',
    'PN [#/s]': '#',
    'N2O [mg/s]': 'mg',
    'NH3 [mg/s]': 'mg',
    'THC [mg/s]': 'mg',
    'CH4 [mg/s]': 'mg',
    'CO2 [g/s]': 'g',
    'CO [mg/s]': 'mg',
    'PN': '#',
    'N2O': 'mg',
    'NH3': 'mg',
    'THC': 'mg',
    'CH4': 'mg',
    'CO2': 'g',
    'CO': 'mg',
    'NOx': 'mg'
}

dict_fuel_colours = {
    'D': 'C0',
    'P': 'C1',
    'EP': 'C2',
    'C': 'C3'
}

list_fuels = ['D', 'P', 'EP', 'C']


