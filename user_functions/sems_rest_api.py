"""
Functions to perform actions on the TNO SEMS Rest API
In a separate py-file to cleanup the eval_SEMS code
"""

import os
from io import StringIO

import pandas as pd
import requests
import tortilla
import yaml

horiba_api = 'http://shipping.sensorlab.tno.nl/rest_api/'


class CredentialFileNotFound(Exception):
    pass


def assert_200(resp):
    if not resp.status_code == 200: print(resp.status_code, resp.url, resp.text)
    assert resp.status_code == 200
    # print("assert resp.status_code == 200")


if os.path.isfile('../settings/shipping_credentials.yml'):
    with open('../settings/shipping_credentials.yml') as f:
        credentials = yaml.load(f, Loader=yaml.Loader)

        response = requests.post(horiba_api + "api_token_auth", json=credentials)
        assert_200(response)
        token = response.json()["token"]

elif os.path.isfile('../../shipping_token.txt'):
    with open('../../shipping_token.txt') as f:
        token = f.read()
else:
    raise CredentialFileNotFound("credentials not found!!")


def get_wrapped_api():
    o = tortilla.wrap(horiba_api, cache_lifetime=100)
    head = {'Authorization': 'token {}'.format(token)}
    o.config.headers = head
    return o


def csv_headers() -> dict:
    return {'Authorization': 'token {}'.format(token)}


def url_formatter(path_end):
    return "{}{}".format(horiba_api, path_end)


def request_maker_api(path_end):
    return requests.get(url_formatter(path_end), headers=csv_headers())


def request_maker(path_end):
    return requests.get(path_end, headers=csv_headers())


def get_sems_raw_measurements(sessionlist, fields=None):
    if fields:
        raw_data_response = request_maker_api(
            "sems_raw_measurements/?sessions={}&format=csv&fields={}"
                .format(",".join(sessionlist), ",".join(fields)))
    else:
        raw_data_response = request_maker_api(
            "sems_raw_measurements/?sessions={}&format=csv".format(
                ",".join(sessionlist)))
    assert_200(raw_data_response)
    return pd.read_csv(StringIO(raw_data_response.text), index_col=0)


def get_sems_processed_measurements(sessionlist, fields=None):
    if fields:
        processed_data_response = request_maker_api(
            "sems_processed_measurements/?sessions={}&format=csv&fields={}"
                .format(",".join(sessionlist), ",".join(fields)))
    else:
        processed_data_response = request_maker_api(
            "sems_processed_measurements/?sessions={}&format=csv".format(
                ",".join(sessionlist)))
    assert_200(processed_data_response)
    return pd.read_csv(StringIO(processed_data_response.text), index_col=0)


def get_sems_calculated_measurements(sessionlist, fields=None):
    if fields:
        calculated_data_response = request_maker_api(
            "sems_calculated_measurements/?sessions={}&format=csv&fields={}"
                .format(",".join(sessionlist), ",".join(fields)))
    else:
        calculated_data_response = request_maker_api(
            "sems_calculated_measurements/?sessions={}&format=csv".format(
                ",".join(sessionlist)))
    assert_200(calculated_data_response)
    return pd.read_csv(StringIO(calculated_data_response.text), index_col=0)


def get_sems_electric_measurements(sessionlist, fields=None):
    if fields:
        calculated_data_response = request_maker_api(
            "sems_electric_measurements/?sessions={}&format=csv&fields={}"
                .format(",".join(sessionlist), ",".join(fields)))
    else:
        calculated_data_response = request_maker_api(
            "sems_electric_measurements/?sessions={}&format=csv".format(
                ",".join(sessionlist)))
    assert_200(calculated_data_response)
    if len(calculated_data_response.text) > 1:
        return pd.read_csv(StringIO(calculated_data_response.text), index_col=0)
    else:
        # print("No electric data found")
        return pd.DataFrame()


def get_sems_additional_signals(sessionlist, fields=None):
    additional_signals_response_total = pd.DataFrame()
    for session in sessionlist:
        additional_signals_response = request_maker_api(
            "additional_signals/{}?format=csv".format(session))
        try:
            assert_200(additional_signals_response)
            if len(additional_signals_response.text) > 1:
                additional_signals_response_total = additional_signals_response_total.append(
                    pd.read_csv(StringIO(additional_signals_response.text)))
        except:
            print("No additional signals data found for this session")
            continue
    if len(additional_signals_response_total) > 1:
        return additional_signals_response_total
    else:
        print("No additional signals data found")
        return pd.DataFrame()


def chunks(l, n):  # credits https://chrisalbon.com/python/data_wrangling
    # /break_list_into_chunks_of_equal_size/ for item i in a range
    # that is a length of l
    a = []
    for i in range(0, len(l), n):
        # Create an index range for l of n items:
        a.append(l[i:i + n])
    return a
