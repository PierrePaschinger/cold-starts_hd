'''
This script contains a range of functions that can be used to display the cold start

'''


import glob
import os
import time

from path import Path
import numpy as np

import pandas as pd
import math
from scipy.stats import linregress

import matplotlib.pyplot as plt

from user_functions.func_change_axis_colour import *
from user_functions.labl_dicts_to_rename_or_colour import *
from user_functions.func_add_tno_watermark_to_figure import add_tno_watermark_to_figure


# 'THC mass_Analyser [g/s]',
# 'CO mass_Analyser [g/s]',
# 'CO2 mass_Analyser [g/s]',
# 'NOx mass_Analyser [g/s]',
# 'PN_Analyser [#/s]',
# 'CO2 Emissions [g/s]',
#  'CO Emissions [mg/s]',
#  'NOx Emissions [mg/s]',
#  'NO Emissions [mg/s]',
#  'NO2 Emissions [mg/s]'
'''
PN, HC(?), CO , NOx
'''


def get_cold_start_parameters_based_on_index(df, the_index, pollutant, col_speed):
    x_index = df.loc[the_index, 'cum_dist [km]']
    y_cold = df.loc[the_index, 'cum_pollutant']
    y_warm = df.loc[the_index:][pollutant].sum()
    if df.loc[the_index:][col_speed].sum() > 0:
        av_warm = (
                df.loc[the_index:][pollutant].sum() /
                (df.loc[the_index:][col_speed].sum() / 3600))
    else:
        av_warm = np.nan
    return x_index, y_cold, y_warm, av_warm


def get_cold_start_regression(df_data, time_signal, x_signal, y_cum_signal, t_cold_start_end,
                              t_reg_window):
    # Filter to cum_NOx > 0.5
    df_data_start = df_data.loc[df_data[y_cum_signal] >= 0.5]
    start_time_window = df_data_start[time_signal].iloc[0]

    df_data_start = df_data_start.loc[(df_data_start[time_signal] > start_time_window) &
                                      (df_data_start[
                                           time_signal] < start_time_window +
                                       t_cold_start_end)].copy()

    # df_data_start = df_data.loc[df_data[time_signal]<t_cold_start_end].copy()

    def get_intercept(col_y):
        y_ = np.array(col_y)
        x_ = np.array(df_data_start.loc[col_y.index][time_signal])
        slope, intercept, r_value, p_value, std_err = linregress(x_, y_)
        return intercept

    def round_to_n(x, n=2):
        if x == 0:
            return x
        elif math.isnan(x):
            return x
        else:
            return round(x, -int(
                math.floor(math.log10(abs(x)))) + (n - 1))

    # Start from initial a_window, find intercept, if negative, reduce the window and find a new
    # intercept until positive intercept is found
    a_window = t_reg_window
    window_divider = 1  # to reduce a_window size if intercept is negative
    intercept_mode = -1  # start with negative intercept to trigger loop

    while intercept_mode < 0:
        df_data_start['rolling_intercept'] = df_data_start[
            y_cum_signal].rolling(
            window=int(a_window / window_divider)). \
            apply(get_intercept, raw=False).reset_index(0, drop=True)

        if df_data_start['rolling_intercept'].isnull().all():
            # intercept_mode = get_intercept(df_data_start[y_cum_signal])
            return df_data.index.max()
            break

        intercept_mode = df_data_start['rolling_intercept'].apply(round_to_n, n=3).mode()[0]

        window_divider = window_divider + 1

    # Get intersection between y signal and linear reg
    idx = df_data.loc[df_data[y_cum_signal] <= intercept_mode].index.max()

    # return x_pred,y_pred,idx, df_data[x_signal].iloc[idx].min()
    return idx


def get_cold_start_regression_dist(df_data, time_signal, vel_signal,
                                   cum_dist_signal, y_cum_signal, dist_cold_start_end,
                                   dist_reg_window):
    # Make copy of df input
    df_temp = df_data.copy()

    # Filter to cum_NOx > 0
    df_temp = df_temp.loc[df_temp[y_cum_signal] >= 0.5]
    start_dist_window = df_temp.loc[df_temp[cum_dist_signal].notnull()][cum_dist_signal].iloc[0]

    df_temp = df_temp.loc[(df_temp[cum_dist_signal] > start_dist_window) & \
                          (df_temp[
                               cum_dist_signal] < start_dist_window + dist_cold_start_end)].copy()

    df_temp = df_temp.dropna(subset=[vel_signal])
    df_temp['cum_distance_sid'] = df_temp[vel_signal].cumsum() / 3600 * 1000
    df_temp['time_index'] = pd.to_datetime(df_temp['cum_distance_sid'], unit='s')
    df_temp.set_index('time_index', inplace=True)

    def get_intercept_dist(col_y):
        slice_temp = df_temp.loc[
            col_y.index, [time_signal, cum_dist_signal, 'cum_pollutant']].drop_duplicates()
        y_ = np.array(slice_temp['cum_pollutant'])
        x_ = np.array(slice_temp[cum_dist_signal])
        slope, intercept, r_value, p_value, std_err = linregress(x_, y_)

        return intercept

    def round_to_n(x, n=2):
        if x == 0:
            return x
        elif math.isnan(x):
            return x
        else:
            return round(x, -int(
                math.floor(math.log10(abs(x)))) + (n - 1))

    # Find positive intercept mode
    window_size = '{}s'.format(int(dist_reg_window))
    df_temp['rolling_slope'] = df_temp[y_cum_signal].rolling(window_size).apply(get_intercept_dist,
                                                                                raw=False)
    if df_temp['rolling_slope'].isnull().all():
        intercept_mode = get_intercept_dist(df_temp[y_cum_signal])

    intercept_mode = \
        df_temp.loc[df_temp['rolling_slope'] >= 0, 'rolling_slope'].apply(round_to_n, n=3).mode()[0]

    # Get intersection between y signal and linear reg
    df_temp['delta_y_intercept'] = np.abs(df_temp[y_cum_signal] - intercept_mode)
    idx_temp = df_temp['delta_y_intercept'].idxmin()

    time_target = df_temp.loc[idx_temp, time_signal].min()
    idx = df_data.loc[df_data[time_signal] == time_target].index[0]

    # return x_pred,y_pred,idx, df_data[x_signal].iloc[idx].min()
    return idx


def analyse_cold_start(df, col_speed, col_time, the_vehicle_file, the_test,
                       list_out, list_comparison_out, list_errors, col_ect,
                       list_pollutant_cols, out_path):
    for pol_num, pollutant in enumerate(list_pollutant_cols):
        try:
            print(f'---- {pollutant}')
            label_pollutant = dict_pollutant_names[pollutant]
            unit_pollutant = dict_pollutant_units[pollutant]
            df.loc[df[pollutant] < 0, pollutant] = 0

            # ======================================================================
            # STEP 1: Cold start
            # TODO: call on new cold start script
            # ======================================================================
            duration_mode = 300
            time_end_window = 900
            if df[pollutant].sum() == 0.0:
                print(f'No signal {pollutant}')
                dict_error = {
                    'vehicle': f'{the_vehicle_file}',
                    'error': f'No signal {pollutant}'
                }
                list_errors.append(dict_error.copy())

                continue
            df['cum_pollutant'] = df[pollutant].cumsum()
            df['cum_dist [km]'] = df[col_speed].cumsum() / 3600
            df['cum_dist [m]'] = df['cum_dist [km]'] * 1000

            cold_start_index_dist = get_cold_start_regression_dist(df, col_time, col_speed,
                                                                   'cum_dist [m]',
                                                                   'cum_pollutant', 3000,
                                                                   50)

            cold_start_index_time = get_cold_start_regression(df, col_time, 'cum_dist [km]',
                                                              'cum_pollutant',
                                                              300, 120)

            # Jessica's edit: get minimum of above to use as index
            cold_start_index = min(cold_start_index_dist, cold_start_index_time)

            # === compare cold start methods ===============================

            index_70_degrees = df.loc[df[col_ect] > 70].index.min()
            index_5_min = df.loc[df['Time [s]'] > 300].index.min()

            x_70, coeff_70, coeff_70_warm, av_70_warm = \
                get_cold_start_parameters_based_on_index(df, index_70_degrees, pollutant, col_speed)

            x_5, coeff_5, coeff_5_warm, av_5_warm = \
                get_cold_start_parameters_based_on_index(df, index_5_min, pollutant, col_speed)

            x_d, coeff_d, coeff_d_warm, av_d_warm = \
                get_cold_start_parameters_based_on_index(df, cold_start_index_dist, pollutant,
                                                         col_speed)

            x_t, coeff_t, coeff_t_warm, av_t_warm = \
                get_cold_start_parameters_based_on_index(df, cold_start_index_time, pollutant,
                                                         col_speed)

            for i_index, i_label in [
                (index_70_degrees, '70 degrees'),
                (index_5_min, '5 minutes'),
                (cold_start_index_time, 'time-based'),
                (cold_start_index_dist, 'distance-based'),
                (cold_start_index, 'chosen')

            ]:
                x_i, coeff_i, coeff_i_warm, av_i_warm = \
                    get_cold_start_parameters_based_on_index(df, i_index, pollutant, col_speed)

                dict_compare_methods = {
                    'vehicle': the_vehicle_file,
                    'test': the_test,
                    'pollutant': label_pollutant,
                    'total_emissions': df[pollutant].sum(),
                    'method cold start': i_label,
                    'end cold start': x_i,
                    'total cold emissons': coeff_i,
                    'total warm emissions': coeff_i_warm,
                    'average warm emissions': av_i_warm
                }
                list_comparison_out.append(dict_compare_methods)

            if not np.isnan(cold_start_index):
                x_dist_pollutant = df.loc[cold_start_index, 'cum_dist [km]']
                coeff_cold_start = df.loc[cold_start_index, 'cum_pollutant']
                df_hot = df.loc[cold_start_index:].copy()
                average_warm = (
                        df_hot[pollutant].sum() /
                        (df_hot[col_speed].sum() / 3600))

            else:
                x_dist_pollutant = 0
                coeff_cold_start = 0
                average_warm = 0

            # --- Plot -----------------------------------------------------
            def plot_cold_start():
                f, (ax, ax1) = plt.subplots(2, sharex=True)
                ax.plot(df['cum_dist [km]'], df['cum_pollutant'])
                if pollutant == 'PN [#/s]':
                    ax.axvline(x_dist_pollutant, color='r', linestyle='--',
                               label='{:.1f} km / {:.2E}'.format(x_dist_pollutant,
                                                                 coeff_cold_start))
                    # ax.set_yscale('log')
                else:
                    ax.axvline(x_dist_pollutant, color='r', linestyle='--',
                               label='{:.1f} km / {:.2f}'.format(x_dist_pollutant,
                                                                 coeff_cold_start))
                ax.set_ylabel(f'Cumulative {label_pollutant} [{unit_pollutant}]')

                ax.legend()

                ax_1 = ax.twinx()
                ax_1.plot(df['cum_dist [km]'], df[col_speed],
                          color='gray', alpha=0.5)
                ax_1.set_ylabel('Vehicle speed [km/h]')
                change_axis_colour(ax_1, 'gray')

                # ax.set_title(f'{the_vehicle} - {the_test}')
                ax.set_title(f'{the_vehicle_file} - {the_test}')

                ax1.plot(df['cum_dist [km]'], df[pollutant], color='C0')
                ax1.set_ylabel(f'{label_pollutant} [{unit_pollutant}/s]')
                # if pollutant == 'PN [#/s]':
                #     ax1.set_yscale('log')

                ax1_1 = ax1.twinx()
                ax1_1.plot(df['cum_dist [km]'], df['Time [s]'], color='C1', alpha=0.5)
                ax1_1.set_ylabel('Time [s]')
                change_axis_colour(ax1_1, 'C1')

                ax1.set_xlabel('Distance [km]')
                ax1.set_xlim(left=-0.5)

                # f.show()

                # add_tno_watermark_to_figure(f, ax, 'input/TNO_zwart.png')
                filename = f'{the_vehicle_file}_{the_test}_cs_{label_pollutant}.png'
                f.savefig(out_path / filename, bbox_inches='tight', dpi=300)

            def plot_annotated_cold_start():
                f, (ax, ax1) = plt.subplots(2, sharex=True)
                ax.plot(df['cum_dist [km]'], df['cum_pollutant'])
                if pollutant == 'PN [#/s]':
                    ax.axvline(x_dist_pollutant, color='r', linestyle='--',
                               label='{:.1f} km / {:.2E}'.format(x_dist_pollutant,
                                                                 coeff_cold_start))
                    ax.axvline(x_70, color='r', linestyle='-.',
                               label='70 degrees: {:.1f} km / {:.2E}'.format(x_70,
                                                                             coeff_70))
                    ax.axvline(x_5, color='r', linestyle=':',
                               label='5 min: {:.1f} km / {:.2E}'.format(x_5,
                                                                        coeff_5))
                    ax.set_yscale('log')
                else:
                    ax.axvline(x_dist_pollutant, color='r', linestyle='--',
                               label='{:.1f} km / {:.2f}'.format(x_dist_pollutant,
                                                                 coeff_cold_start))
                    ax.axvline(x_70, color='r', linestyle='-.',
                               label='70 degrees: {:.1f} km / {:.2f}'.format(x_70,
                                                                             coeff_70))
                    ax.axvline(x_5, color='r', linestyle=':',
                               label='5 min: {:.1f} km / {:.2f}'.format(x_5,
                                                                        coeff_5))
                    ax.axvline(x_d, color='r', linestyle='-.', alpha=0.5,
                               label='Distance: {:.1f} km / {:.2f}'.format(x_d,
                                                                           coeff_d))
                    ax.axvline(x_t, color='r', linestyle=':', alpha=0.5,
                               label='Time: {:.1f} km / {:.2f}'.format(x_t,
                                                                       coeff_t))

                ax.set_ylabel(f'Cumulative {label_pollutant} [{unit_pollutant}]')

                ax.legend()

                ax_1 = ax.twinx()
                ax_1.plot(df['cum_dist [km]'], df[col_speed],
                          color='gray', alpha=0.5)
                ax_1.set_ylabel('Vehicle speed [km/h]')
                change_axis_colour(ax_1, 'gray')

                # ax.set_title(f'{the_vehicle} - {the_test}')
                ax.set_title(f'{the_vehicle_file} - {the_test}')

                ax1.plot(df['cum_dist [km]'], df[pollutant], color='C0')
                ax1.set_ylabel(f'{label_pollutant} [{unit_pollutant}/s]')
                if pollutant == 'PN [#/s]':
                    ax1.set_yscale('log')

                ax1_1 = ax1.twinx()
                ax1_1.plot(df['cum_dist [km]'], df['Time [s]'], color='C1', alpha=0.5)
                ax1_1.set_ylabel('Time [s]')
                change_axis_colour(ax1_1, 'C1')

                ax1.set_xlabel('Distance [km]')
                ax1.set_xlim(-0.5, 8)

                # f.show()

                # add_tno_watermark_to_figure(f, ax, 'input/TNO_zwart.png')
                filename = f'{the_vehicle_file}_{the_test}_cs_{label_pollutant}_annotated.png'
                f.savefig(out_path / filename, bbox_inches='tight', dpi=300)

            #

            plot_cold_start()
            plot_annotated_cold_start()

            dict_out = {
                'vehicle': the_vehicle_file,
                'test': the_test,
                'pollutant': label_pollutant,
                'cold start dist': x_dist_pollutant,
                'cold start offset': coeff_cold_start,
                'warm_per_km': average_warm,
                'total_emissions': df[pollutant].sum()
            }
            list_out.append(dict_out)
        except Exception as e:
            print(e)
            dict_error = {
                'vehicle': f'{the_vehicle_file}',
                'error': e
            }
            list_errors.append(dict_error.copy())
            continue


if __name__ == "__main__":
    print(f'###################################################################\n'
          f'START: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')

    project_name = 'sems-rde'

    input_path = Path(f'../data/{project_name}/input')
    out_path = Path(f'../data/{project_name}/output')
    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    in_path_unix = Path(r'\\tsn.tno.nl\data\sv\sv-094275\Kluis\data\ld_vehicles_01-2021')

    list_vehicles = [
        'PE_30_XJ417X_EUR6',
        'VO_X6_XK461J_EUR6',
        'ME_B1_ZT566T_EUR6',
        'SK_OC_ZD852P_EUR6',
        'RE_MA_VDR83F_EUR6'
    ]

    list_vehicle_file = [fn for fn in os.listdir(in_path_unix)
                         if any(fn.startswith(ext) for ext in list_vehicles)]

    list_vehicle_file = glob.glob(input_path / 'VW_CA_VJG59N_EUR6*.pkl')

    i_time = 300

    col_time = 'Time [s]'
    col_egt = 'after_cat_EGT'
    col_nox = 'after_cat_NOx_mF'
    col_nh3 = 'after_cat_NH3_mF'
    col_ect = 'ECT'

    list_pollutant_cols = [
        'after_cat_NH3_mF',
        'after_cat_NOx_mF'
    ]

    list_errors = []
    list_out = []
    list_comparison_out = []

    for a_file in list_vehicle_file:
        file_input_path = a_file
        file_name = os.path.splitext(os.path.basename(file_input_path))[0]
        vehicle_file_prefix = file_name[:17]
        title_prefix = vehicle_file_prefix
        print(f'====== {title_prefix} =====')

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        df = pd.read_pickle(file_input_path)

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        list_cold_starts = list(df.loc[df[col_ect] < 70]['session'].unique())

        for i_session in list_cold_starts:
            df_temp = df.loc[df['session'] == i_session].copy()
            df_temp['Time [s]'] = df_temp['timecount'] - df_temp.groupby('session')[
                'timecount'].transform('min')

            if (df['velocity_filtered'].cumsum() / 3600).max() > 8:
                try:
                    analyse_cold_start(df_temp, 'velocity_filtered', col_time, vehicle_file_prefix,
                                       i_session,
                                       list_out, list_comparison_out, list_errors,
                                       col_ect,
                                       list_pollutant_cols, out_path)
                except Exception as e:
                    dict_error = {
                        'vehicle': i_session,
                        'osrm error': e
                    }
                    list_errors.append(dict_error.copy())

    df_error = pd.DataFrame(list_errors)
    df_error.to_csv(((out_path / 'cold_start_errors_{}.csv'.format(
        time.strftime("%d-%m-%Y_%H%M%S", time.localtime(time.time()))
    ))), sep=',', index=False)

    df_out = pd.DataFrame(list_out)
    df_out.to_csv(((out_path / 'cold_start_coeff_{}.csv'.format(
        time.strftime("%d-%m-%Y_%H%M%S", time.localtime(time.time()))
    ))), sep=',', index=False)

    df_out = pd.DataFrame(list_comparison_out)
    df_out.to_csv(((out_path / 'annotated' / 'cold_start_coeff_comparison_{}.csv'.format(
        time.strftime("%d-%m-%Y_%H%M%S", time.localtime(time.time()))
    ))), sep=',', index=False)

    print('End')
