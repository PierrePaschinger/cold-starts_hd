import numpy as np
import scipy as sp
from scipy.signal import medfilt

import re
import pandas as pd
import math
from scipy.stats import linregress

import matplotlib.pyplot as plt

import pickle


def round_to_n(x, n=2):
    if x == 0:
        return x
    elif math.isnan(x):
        return x
    else:
        return round(x, -int(
            math.floor(math.log10(abs(x)))) + (n - 1))


# def plot_dpf_cumulative_and_time(df, vehicle_title, trip_label, file_prefix, out_path,
#                                  col_time='Time [s]'):
#     f, ax = plt.subplots(nrows=4, sharex=True)
#     ax[0].plot(df[col_time], df['velocity [km/h]'],
#                color='gray', alpha=0.5)
#     ax[0].set_ylabel('Speed [km/h]')
#     # ax0.plot(df[col_time], df['Exhaust Temp. PEMS [°C]'],
#     #                label='EGT [°C]', color='C1', alpha=0.5)
#
#     change_axis_colour(ax[0], 'gray')
#
#     if col_egt in df.columns:
#         ax_1 = ax[0].twinx()
#         ax_1.set_ylabel('Exhaust Temp. [°C]')
#         change_axis_colour(ax_1, 'C1')
#         ax_1.plot(df[col_time], df[col_egt],
#                   color='C1', alpha=0.5)
#
#     ax[1].plot(df[col_time], df['cum_NOx [mg]'], color='C0')
#     ax[1].set_ylabel('NOx [mg]')
#
#     ax1_1 = ax[1].twinx()
#     change_axis_colour(ax1_1, 'C0')
#     ax1_1.set_ylabel('NOx [mg/s]')
#     ax1_1.plot(df[col_time], df[col_nox], color='C0', alpha=0.5)
#
#     ax[2].plot(df[col_time], df['cum_CO [mg]'], color='C2')
#     ax[2].set_ylabel('CO [mg]')
#     ax2_1 = ax[2].twinx()
#     change_axis_colour(ax2_1, 'C2')
#     ax2_1.set_ylabel('CO [mg/s]')
#     ax2_1.plot(df[col_time], df[col_co], color='C2', alpha=0.5)
#
#     ax[3].plot(df[col_time], df['cum_PN [#]'], color='C4')
#     ax[3].set_ylabel('PN [#]')
#     ax[3].set_xlabel('Time [s]')
#     ax3_1 = ax[3].twinx()
#     change_axis_colour(ax3_1, 'C4')
#     ax3_1.set_ylabel('PN [#/s]')
#     ax3_1.plot(df[col_time], df[col_pn], color='C4', alpha=0.5)
#
#     ax[0].set_title(f'{vehicle_title} - {trip_label}')
#
#     add_tno_watermark_to_figure(f, ax[3], 'input/TNO_zwart.png')
#     filename = f'{file_prefix}_nox_co_pn_cum.png'
#     f.savefig(out_path / filename, bbox_inches='tight', dpi=300)
#

def get_window_regression_max(df_data, time_signal, y_cum_signal, t_start,
                              t_end, t_reg_window):
    df_data_start = df_data.loc[(df_data[time_signal] > t_start) &
                                (df_data[time_signal] < t_end)].copy()

    def get_intercept(col_y):
        y_ = np.array(col_y)
        x_ = np.array(df_data_start.loc[col_y.index][time_signal])
        slope, intercept, r_value, p_value, std_err = linregress(x_, y_)
        return intercept

    def get_slope(col_y):
        y_ = np.array(col_y)
        x_ = np.array(df_data_start.loc[col_y.index][time_signal])
        slope, intercept, r_value, p_value, std_err = linregress(x_, y_)
        return slope

    # Start from initial a_window, find intercept, if negative, reduce the window and find a new
    # intercept until positive intercept is found
    a_window = t_reg_window

    if a_window > 0.75 * len(df_data_start):
        intercept_mode = get_intercept(df_data_start[y_cum_signal])
        slope_mode = get_slope(df_data_start[y_cum_signal])
    else:
        window_divider = 1  # to reduce a_window size if intercept is negative
        intercept_mode = -1  # start with negative intercept to trigger loop

        while intercept_mode < 0:
            df_data_start['rolling_intercept'] = df_data_start[
                y_cum_signal].rolling(
                window=int(a_window / window_divider)). \
                apply(get_intercept, raw=False).reset_index(0, drop=True)
            df_data_start['rolling_slope'] = df_data_start[
                y_cum_signal].rolling(
                window=int(a_window / window_divider)). \
                apply(get_slope, raw=False).reset_index(0, drop=True)

            if df_data_start['rolling_intercept'].isnull().all():
                intercept_mode = get_intercept(df_data_start[y_cum_signal])
                slope_mode = get_slope(df_data_start[y_cum_signal])
                break

            intercept_mode = df_data_start['rolling_intercept'].apply(round_to_n, n=3).mode()[0]
            slope_mode = df_data_start.loc[
                df_data_start['rolling_intercept'].apply(round_to_n, n=3) == intercept_mode][
                'rolling_slope'].mean()
            window_divider = window_divider + 1

    df_data['pred_pollutant'] = intercept_mode + slope_mode * df_data[time_signal]

    # f, ax = plt.subplots()
    # df_data.pred_pollutant.plot()
    # df_data[y_cum_signal].plot()
    # f.show()

    # Get intersection between y signal and linear reg
    idx = df_data.loc[df_data[y_cum_signal] <= df_data['pred_pollutant']].index.max()

    # return x_pred,y_pred,idx, df_data[x_signal].iloc[idx].min()
    return idx


def get_end_dpf(df_data, time_signal, y_cum_signal, t_start,
                t_end, t_reg_window):
    df_data_start = df_data.loc[(df_data[time_signal] > t_start) &
                                (df_data[time_signal] < t_end)
                                ].reset_index(drop=True).copy()

    window_divider = 1
    a_window = t_reg_window

    def get_intercept(col_y):
        y_ = np.array(col_y)
        x_ = np.array(df_data_start.loc[col_y.index][time_signal])
        slope, intercept, r_value, p_value, std_err = linregress(x_, y_)
        return intercept

    def get_slope(col_y):
        y_ = np.array(col_y)
        x_ = np.array(df_data_start.loc[col_y.index][time_signal])
        slope, intercept, r_value, p_value, std_err = linregress(x_, y_)
        return slope

    df_data_start['rolling_intercept'] = df_data_start[
        y_cum_signal].rolling(
        window=int(a_window / window_divider)
    ).apply(get_intercept,
            raw=False
            ).reset_index(0, drop=True)

    df_data_start['rolling_slope'] = df_data_start[
        y_cum_signal].rolling(
        window=int(a_window / window_divider)). \
        apply(get_slope, raw=False).reset_index(0, drop=True)

    intercept_mode = df_data_start['rolling_intercept'].apply(round_to_n, n=3).mode()[0]

    slope_mode = df_data_start.loc[
        df_data_start['rolling_intercept'].apply(round_to_n, n=3) == intercept_mode][
        'rolling_slope'].abs().min()
    if slope_mode < 0:
        print('slope negative')

    # slope_mode = df_data_start.loc[
    #     df_data_start['rolling_intercept'].apply(round_to_n, n=3) == intercept_mode][
    #     'rolling_slope'].mean()

    df_data['pred_pollutant'] = intercept_mode + slope_mode * df_data[time_signal]

    # Get intersection between y signal and linear reg
    idx = df_data.loc[df_data[y_cum_signal] <= df_data['pred_pollutant']].index.max()
    idx_1 = df_data.loc[df_data[y_cum_signal].apply(round_to_n, n=1)
                        <= df_data['pred_pollutant'].apply(round_to_n, n=1)
                        ].index.max()
    idx_2 = df_data.loc[df_data[y_cum_signal].apply(round_to_n, n=2)
                        <= df_data['pred_pollutant'].apply(round_to_n, n=2)
                        ].index.max()

    f, ax = plt.subplots()
    df_data.pred_pollutant.plot()
    df_data[y_cum_signal].plot()
    ax.axvline(idx_1, color='C0')
    ax.axvline(idx_2, color='C1')
    ax.axvline(idx, color='red')
    ax.set_title(f'end {t_start} - {t_end}')
    # f.show()
    # print('------ obtained end')
    return idx


def get_dpf_unfinished(df_data, time_signal, y_cum_signal, t_start,
                       t_end, t_reg_window):
    df_data_start = df_data.loc[(df_data[time_signal] > t_start) &
                                (df_data[time_signal] < t_end)].copy()
    idx = df_data_start.loc[
        df_data_start[y_cum_signal].apply(round_to_n, n=2) >=
        df_data_start[y_cum_signal].apply(round_to_n, n=2).max()].index.min()

    return idx


def get_parameters_based_on_index(df, time_signal, the_index, cum_pollutant_, ):
    x_index = df.loc[the_index, 'cum_dist [km]']
    x_index_time = df.loc[the_index, time_signal]
    y_cold = df.loc[the_index, cum_pollutant_]
    x_index_total = df.loc[the_index, 'Distance [km]']
    # y_warm = df.loc[the_index:][pollutant_].sum()
    # av_warm = (
    #         df.loc[the_index:][pollutant_].sum() /
    #         (df.loc[the_index:]['velocity_filtered [km/h]'].sum() / 3600))
    return x_index, y_cold, x_index_time, x_index_total
