import glob
import os
import time

from path import Path
import numpy as np

import pandas as pd
import math
from scipy.stats import linregress

import matplotlib.pyplot as plt
import seaborn as sns

from user_functions.labl_dicts_to_rename_or_colour import *
from user_functions.func_add_tno_watermark_to_figure import add_tno_watermark_to_figure


def calculate_emissions(df, section_label, out_list, col_speed='velocity_filtered',
                        col_co2='CO2_mF',
                        col_mf_nox='after_cat_NOx_mF', col_mf_nh3='after_cat_NH3_mF'):
    output_sems_factor_dist = df[col_speed].sum() / 3600
    output_sems_factor_co2 = df[col_co2].sum() / output_sems_factor_dist
    output_sems_factor_nox = df[col_mf_nox].sum() / output_sems_factor_dist
    output_sems_factor_nh3 = df[col_mf_nh3].sum() / output_sems_factor_dist

    output_dict = {
        'section': section_label,
        'sems_co2': output_sems_factor_co2,
        'sems_nox': output_sems_factor_nox,
        'sems_nh3': output_sems_factor_nh3,
    }

    out_list.append(output_dict.copy())


def plot_efs_rde_sections(df, total_label, vehicle_title, file_name, out_path,
                          NOX_LIMIT_=114.4, colour_bar='C0', ):
    vel_bins_RDE = [0, 60, 90, 140]
    vel_bins_RDE_mid = [30, 60, 90]
    list_labels = ['Urban', 'Rural', 'Motorway']

    df['vel_bins'] = pd.cut(df['velocity_filtered'], bins=vel_bins_RDE, labels=list_labels)

    list_total = []

    df.groupby('vel_bins').apply(
        lambda x: calculate_emissions(x, x.name, list_total))

    df_speed = pd.DataFrame(list_total)
    df_speed['Type'] = total_label

    f_0 = plt.figure()
    ax_0 = f_0.add_subplot(2, 1, 1)

    ax_0_0 = sns.barplot(x=df_speed['section'], y=df_speed['sems_nox'], ax=ax_0,
                         order=['Urban', 'Rural', 'Motorway'],
                         color=colour_bar)
    ax_0_0.set(ylabel='Average NO$_x$ Emission [mg/km]', xlabel='RDE Road Type')
    ax_0.axhline(NOX_LIMIT_, color='red', linestyle='dashed',
                 label='Euro 6d NO$_x$ RDE limit')  # Euro 6d limit
    ax_0.axhline(80, color='blue', linestyle='dashed',
                 label='80 mg/km')  # Euro 6d limit
    ax_0.legend(loc='upper left')
    ax_0.set_title(f'{vehicle_title}')
    add_tno_watermark_to_figure(f_0, ax_0)
    # f_0.show()

    f_name = '{}_nox_rde_sections_vs_limit.png'.format(file_name)
    f_0.savefig((out_path / f_name), bbox_inches='tight', dpi=300)
    plt.close()


def plot_efs_rde_sections_without_limit(df, total_label, vehicle_title, file_name, out_path,
                                        colour_bar='C0'):
    vel_bins_RDE = [0, 60, 90, 140]
    vel_bins_RDE_mid = [30, 60, 90]
    list_labels = ['Urban', 'Rural', 'Motorway']

    df['vel_bins'] = pd.cut(df['velocity_filtered'], bins=vel_bins_RDE, labels=list_labels)

    list_total = []

    df.groupby('vel_bins').apply(
        lambda x: calculate_emissions(x, x.name, list_total))

    df_speed = pd.DataFrame(list_total)
    df_speed['Type'] = total_label

    f_0 = plt.figure()
    ax_0 = f_0.add_subplot(2, 1, 1)

    ax_0_0 = sns.barplot(x=df_speed['section'], y=df_speed['sems_nox'], ax=ax_0,
                         order=['Urban', 'Rural', 'Motorway'],
                         color=colour_bar
                         )
    ax_0_0.set(ylabel='Average NO$_x$ Emission [mg/km]', xlabel='RDE Road Type')
    # ax_0.legend()
    ax_0.set_title(f'{vehicle_title}')
    add_tno_watermark_to_figure(f_0, ax_0)
    # f_0.show()

    f_name = '{}_nox_rde_sections.png'.format(file_name)
    f_0.savefig((out_path / f_name), bbox_inches='tight', dpi=300)
    plt.close()


def plot_efs_rde_sections_vs_normal_driving(df, list_rde_sessions, vehicle_title, file_name,
                                            out_path,
                                            NOX_LIMIT=114.4):
    rde_label = 'Average over RDE tests'
    total_label = 'Average over all normal use'
    title_suffix = 'RDE Tests vs Normal Use'
    df_RDE = df.loc[df['session'].isin(list_rde_sessions)].copy()
    vel_bins_RDE = [0, 60, 90, 140]
    vel_bins_RDE_mid = [30, 60, 90]
    list_labels = ['Urban', 'Rural', 'Motorway']

    df_RDE['vel_bins'] = pd.cut(df_RDE['velocity_filtered'], bins=vel_bins_RDE, labels=list_labels)
    df['vel_bins'] = pd.cut(df['velocity_filtered'], bins=vel_bins_RDE, labels=list_labels)

    list_rde = []
    list_total = []

    df.groupby('vel_bins').apply(
        lambda x: calculate_emissions(x, x.name, list_total))
    df_RDE.groupby('vel_bins').apply(
        lambda x: calculate_emissions(x, x.name, list_rde))
    df_rde_speed = pd.DataFrame(list_rde)
    df_total_speed = pd.DataFrame(list_total)

    df_rde_speed['Type'] = rde_label
    df_total_speed['Type'] = total_label
    df_speed = pd.concat([df_rde_speed, df_total_speed]).reset_index(drop=True)
    # --- NOx ----------------------------------------------------------------------
    f_0 = plt.figure()
    ax_0 = f_0.add_subplot(2, 1, 1)
    ax_0_0 = sns.barplot(df_speed['section'], df_speed['sems_nox'], ax=ax_0,
                         order=['Urban', 'Rural', 'Motorway'],
                         hue=df_speed['Type'], hue_order=[rde_label, total_label],
                         palette={rde_label: 'grey', total_label: 'C0'})
    ax_0_0.set(ylabel='Average NO$_x$ Emission [mg/km]', xlabel='Road Type')
    ax_0.axhline(NOX_LIMIT, color='red', linestyle='dashed',
                 label='Euro 6d NO$_x$ emission limit')  # Euro 6d limit
    ax_0.legend()
    ax_0.set_title(f'{vehicle_title} {title_suffix}')
    add_tno_watermark_to_figure(f_0, ax_0)
    # f_0.show()

    f_name = '{}_nox_rde_vs_total.png'.format(file_name)
    f_0.savefig((out_path / f_name), bbox_inches='tight', dpi=300)
    plt.close()


def plot_efs_rde_sections_vs_normal_driving_no_limits(df, list_rde_sessions, vehicle_title,
                                                      file_name,
                                                      out_path,
                                                      NOX_LIMIT=114.4):
    rde_label = 'Average over RDE tests'
    total_label = 'Average over all normal use'
    title_suffix = 'RDE Tests vs Normal Use'
    df_RDE = df.loc[df['session'].isin(list_rde_sessions)].copy()
    vel_bins_RDE = [0, 60, 90, 140]
    vel_bins_RDE_mid = [30, 60, 90]
    list_labels = ['Urban', 'Rural', 'Motorway']

    df_RDE['vel_bins'] = pd.cut(df_RDE['velocity_filtered'], bins=vel_bins_RDE, labels=list_labels)
    df['vel_bins'] = pd.cut(df['velocity_filtered'], bins=vel_bins_RDE, labels=list_labels)

    list_rde = []
    list_total = []

    df.groupby('vel_bins').apply(
        lambda x: calculate_emissions(x, x.name, list_total))
    df_RDE.groupby('vel_bins').apply(
        lambda x: calculate_emissions(x, x.name, list_rde))
    df_rde_speed = pd.DataFrame(list_rde)
    df_total_speed = pd.DataFrame(list_total)

    df_rde_speed['Type'] = rde_label
    df_total_speed['Type'] = total_label
    df_speed = pd.concat([df_rde_speed, df_total_speed]).reset_index(drop=True)
    # --- NOx ----------------------------------------------------------------------
    f_0 = plt.figure()
    ax_0 = f_0.add_subplot(2, 1, 1)
    ax_0_0 = sns.barplot(df_speed['section'], df_speed['sems_nox'], ax=ax_0,
                         order=['Urban', 'Rural', 'Motorway'],
                         hue=df_speed['Type'], hue_order=[rde_label, total_label],
                         palette={rde_label: 'grey', total_label: 'C0'})
    ax_0_0.set(ylabel='Average NO$_x$ Emission [mg/km]', xlabel='Road Type')

    ax_0.legend()
    ax_0.set_title(f'{vehicle_title} {title_suffix}')
    add_tno_watermark_to_figure(f_0, ax_0)
    # f_0.show()

    f_name = '{}_nox_rde_vs_total_no_limits.png'.format(file_name)
    f_0.savefig((out_path / f_name), bbox_inches='tight', dpi=300)
    plt.close()


if __name__ == "__main__":
    print(f'###################################################################\n'
          f'START: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')

    project_name = 'sems-rde'

    input_path = Path(f'../data/{project_name}/input')
    out_path = Path(f'../data/{project_name}/output')
    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    list_vehicle_file = glob.glob(input_path / 'VW_CA_VJG59N_EUR6_out_220321-310321_no_limits.pkl')

    i_time = 300

    col_time = 'Time [s]'
    col_egt = 'after_cat_EGT'
    col_nox = 'after_cat_NOx_mF'
    col_nh3 = 'after_cat_NH3_mF'
    col_ect = 'ECT'

    list_pollutant_cols = [
        'after_cat_NH3_mF',
        'after_cat_NOx_mF'
    ]

    list_errors = []
    list_out = []
    list_comparison_out = []

    for a_file in list_vehicle_file:
        file_input_path = a_file
        file_name = os.path.splitext(os.path.basename(file_input_path))[0]
        vehicle_file_prefix = file_name[:17]
        title_prefix = vehicle_file_prefix
        print(f'====== {title_prefix} =====')

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        df = pd.read_pickle(file_input_path)

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        plot_efs_rde_sections(df, 'SEMS RDE',
                              '{} - All Data'.format(vehicle_file_prefix),
                              '{}-all'.format(file_name),
                              out_path)
