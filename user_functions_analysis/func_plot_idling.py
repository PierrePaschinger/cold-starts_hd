'''
Running this will plot NOx [mg/s] as a function of time for all idling segments
Needs: pkl file for vehicle in question -> fill in at TODO (1) & (2)
'''

import glob
import os
import time

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from path import Path

from user_functions.func_add_tno_watermark_to_figure import add_tno_watermark_to_figure


def calculate_vapos(df, vel_signal):
    df_ = df.copy()

    # df['acc_filt'] = (df[vel_signal] - df[vel_signal].shift(1))/3.6 #m/s2
    # Trapezoidal way of calculating acceleration
    df_['acc'] = (np.array([*df_[vel_signal].to_list()[1:], 0]) - np.array(
        [0, *df_[vel_signal].to_list()[0:-1]])) / (2 * 3.6)

    df_['apos'] = df_['acc'].copy()
    df_.loc[df_['apos'] <= 0, 'apos'] = np.nan

    df_['vapos'] = np.multiply(df_[vel_signal] / 3.6, df_['apos'])
    return df_[['vapos']]


def define_drive_type(df, vel_signal, threshold_duration=300):
    df_ = df.copy()

    df_['vapos'] = calculate_vapos(df_, vel_signal)

    df_['k_vel'] = df_[vel_signal].ewm(alpha=0.03, adjust=False).mean()
    df_['k_vapos'] = df_['vapos'].ewm(alpha=0.01, adjust=False).mean()

    # Define continuous idle as vel < 5 for 300 seconds
    df_.loc[df[vel_signal] < 5, 'idle_flag'] = 1
    df_['group_id'] = df_['idle_flag'].diff().ne(0).cumsum()
    df_group = df_.groupby('group_id')
    df_filter = df_group.filter(lambda x: (len(x) > threshold_duration))
    idle_group = df_filter['group_id'].unique()
    df_.loc[df_['group_id'].isin(idle_group), 'drive_type'] = 0

    # Define continuious stop and go as kvel < 15 for 300 seconds
    df_.loc[(df_['k_vel'] < 15) & (df_['drive_type'] != 0),
            'stop_and_go_flag'] = 1  # 'stop and go'
    df_['group_id'] = df_['stop_and_go_flag'].diff().ne(0).cumsum()
    df_group = df_.groupby('group_id')
    df_filter = df_group.filter(lambda x: (len(x) > threshold_duration))
    stop_and_go_group = df_filter['group_id'].unique()
    df_.loc[df_['group_id'].isin(stop_and_go_group), 'drive_type'] = 1

    df_.loc[df_['k_vapos'] > 5, 'dynamic_flag'] = 3  # 'dynamic'
    df_['drive_type'] = df_['drive_type'].fillna(df_['dynamic_flag'])
    df_['drive_type'] = df_['drive_type'].fillna(2)  # normal

    return df_[['k_vel', 'k_vapos', 'drive_type']]


def plot_idling_per_second(df_vehicle, vehicle_label, output_folder):
    # Assign Test X drive type code: 0=idle, 1=stop-and-go, 2=normal/undefined, 3=dynamic
    df_vehicle[['k_vel', 'k_vapos', 'drive_type']] = define_drive_type(df_vehicle,
                                                                       'velocity_filtered')
    # --- get individual instances of idling
    df_vehicle.loc[df_vehicle['drive_type'] == 0, 'idling'] = 1
    df_vehicle['idling'].fillna(0, inplace=True)
    df_vehicle['idle_number'] = (df_vehicle['idling'] != df_vehicle[
        'idling'].shift()).cumsum()
    df_vehicle.loc[df_vehicle['idling'] == 0, 'idle_number'] = np.nan
    df_vehicle['idle_number'] = df_vehicle['idle_number'].div(2)

    fig_idle = plt.figure()
    ax_idle = fig_idle.add_subplot(2, 1, 1)
    for session_id in df_vehicle['idle_number'].unique():
        df_filter = df_vehicle.loc[df_vehicle['idle_number'] == session_id]

        if not df_filter.loc[df_filter['drive_type'] == 0].empty:
            df_filter_idle = df_filter.loc[df_filter['drive_type'] == 0]
            ax_idle.plot(df_filter_idle['after_cat_NOx_mF'].to_list(),
                         # color='C0'
                         )

    ax_idle.set_title('NOx emission [mg/s] during idle\n{}'.format(vehicle_label))
    ax_idle.set_xlabel('Time during idling [s]')
    ax_idle.set_ylabel('NOx emission during idling [mg/s]')
    ax_idle.set_ylim([0, 5])
    ax_idle.axvline(300, color='red', linestyle='dashed')
    add_tno_watermark_to_figure(fig_idle, ax_idle, 'input/TNO_zwart.png')
    fig_idle.savefig(output_folder / '{}_idle_emission_time_coloured.png'.format(vehicle_label),
                     bbox_inches='tight', dpi=300)
    ax_idle.set_ylim([0, 2.5])
    ax_idle.set_xlim([0, 1000])
    fig_idle.savefig(output_folder / '{}_idle_emission_time_1000s_coloured.png'.format(
        vehicle_label),
                     bbox_inches='tight', dpi=300)


if __name__ == "__main__":
    print(f'###################################################################\n'
          f'START: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')

    project_name = 'sems-rde'

    input_path = Path(f'../data/{project_name}/input')
    out_path = Path(f'../data/{project_name}/output')
    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    the_file = 'PE_EX_VJR86F_EUR6_out_150421-070521.pkl'  # TODO (1) which vehicle?
    title_vehicle = 'Peugeot Expert'  # TODO (2) title of the vehicle in figure

    list_vehicle_file = glob.glob(input_path / the_file)

    for a_file in list_vehicle_file:
        file_input_path = a_file
        file_name = os.path.splitext(os.path.basename(file_input_path))[0]
        vehicle_file_prefix = file_name[:17]
        title_prefix = vehicle_file_prefix
        print(f'====== {title_prefix} =====')

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        df = pd.read_pickle(file_input_path)
        df.sort_values(by='datetime', inplace=True)
        df.reset_index(drop=True, inplace=True)
        df['timecount'] = df.index

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        plot_idling_per_second(df, title_prefix, out_path)

        print('end')

print('')
