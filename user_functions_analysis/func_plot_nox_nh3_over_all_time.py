import glob
import os
import time

import seaborn as sns
from path import Path
import matplotlib.pyplot as plt
import pandas as pd

from user_functions.func_add_tno_watermark_to_figure import add_tno_watermark_to_figure
from user_functions.func_change_axis_colour import *


def plot_nox_nh3_ect(df, vehicle_title, file_name, col_time_, col_egt_, col_ect_,
                     col_nox_, col_nh3_,
                     list_pollutant_cols_, out_path, i_time=300):
    df['Distance [km]'] = df['velocity_filtered'].cumsum() / 3600
    df['rolling_temp'] = df[col_egt_].rolling(i_time, center=True, min_periods=i_time).mean()
    df['rolling_temp_ect'] = df[col_ect_].rolling(i_time, center=True, min_periods=i_time).mean()

    for pollutant in list_pollutant_cols_:
        df[f'{pollutant}_cum'] = df[pollutant].cumsum() / 1000

    font_size = 9

    f, ax = plt.subplots(nrows=3, sharex=True)
    ax[0].plot(df[col_time_], df['velocity_filtered'],
               color='gray', alpha=0.3)
    ax[0].set_ylabel('Speed [km/h]', fontsize=font_size)
    change_axis_colour(ax[0], 'gray')

    if col_egt_ in df.columns:
        ax_1 = ax[0].twinx()
        ax_1.set_ylabel('Exhaust T. [°C]')
        change_axis_colour(ax_1, 'C1')
        # ax_1.plot(df[col_time_], df[col_egt],
        #           color='C1', alpha=0.5)
        ax_1.plot(df[col_time_], df['rolling_temp'],
                  color='C1', alpha=0.5)
        ax_1.plot(df[col_time_], df['rolling_temp_ect'],
                  color='red', alpha=0.2)
        ax_1.annotate('{:.1f}'.format(df['rolling_temp_ect'].iloc[0]),
                      xy=(df[col_time_].iloc[0], df['rolling_temp_ect'].iloc[0]),
                      ha='center', color='red', alpha=0.5,
                      xytext=(-5, 0),
                      textcoords='offset points'
                      )
        ax_1.annotate('ECT',
                      xy=(df[col_time_].iloc[-1], df['rolling_temp_ect'].iloc[-1]),
                      ha='center', color='red', alpha=0.5,
                      xytext=(10, 0),
                      textcoords='offset points'
                      )

    ax[1].plot(df[col_time_], df['after_cat_NOx_mF_cum'], color='C0')
    ax[1].set_ylabel('NOx [g]', fontsize=font_size)

    ax1_1 = ax[1].twinx()
    change_axis_colour(ax1_1, 'C0')
    ax1_1.set_ylabel('NOx [mg/s]', fontsize=font_size)
    ax1_1.plot(df[col_time_], df[col_nox_], color='C0', alpha=0.5)

    ax[2].plot(df[col_time_], df['after_cat_NH3_mF_cum'], color='C8')
    ax[2].set_ylabel('NH3 [g]', fontsize=font_size)

    ax2_1 = ax[2].twinx()
    change_axis_colour(ax2_1, 'C8')
    ax2_1.set_ylabel('NH3 [mg/s]', fontsize=font_size)
    ax2_1.plot(df[col_time_], df[col_nh3_], color='C8', alpha=0.5)

    d_driven = df['velocity_filtered'].sum() / 3600

    ax[0].set_title(f'{vehicle_title} - {d_driven:.0f} km')
    ax[2].set_xlabel(col_time_)

    add_tno_watermark_to_figure(f, ax[2], 'input/TNO_zwart.png')
    f.align_ylabels()
    f.subplots_adjust(hspace=0.3)

    filename = f'{file_name}_nox_nh3_ect_{i_time}.png'
    # f.show()
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)


def plot_nox_nh3(df, vehicle_title, file_name, col_time_, col_egt_, col_ect_,
                 col_nox_, col_nh3_,
                 list_pollutant_cols_, out_path, i_time=300):
    df['Distance [km]'] = df['velocity_filtered'].cumsum() / 3600
    df['rolling_temp'] = df[col_egt_].rolling(i_time, center=True, min_periods=i_time).mean()

    for pollutant in list_pollutant_cols_:
        df[f'{pollutant}_cum'] = df[pollutant].cumsum() / 1000

    font_size = 9

    f, ax = plt.subplots(nrows=3, sharex=True)
    ax[0].plot(df[col_time_], df['velocity_filtered'],
               color='gray', alpha=0.3)
    ax[0].set_ylabel('Speed [km/h]', fontsize=font_size)
    change_axis_colour(ax[0], 'gray')

    if col_egt_ in df.columns:
        ax_1 = ax[0].twinx()
        ax_1.set_ylabel('Exhaust T. [°C]')
        change_axis_colour(ax_1, 'C1')
        # ax_1.plot(df[col_time_], df[col_egt],
        #           color='C1', alpha=0.5)
        ax_1.plot(df[col_time_], df['rolling_temp'],
                  color='C1', alpha=0.5)

        # ax_1.axhline(df[col_egt].quantile(0.90), color='C1')

    ax[1].plot(df[col_time_], df['after_cat_NOx_mF_cum'], color='C0')
    ax[1].set_ylabel('NOx [g]', fontsize=font_size)

    ax1_1 = ax[1].twinx()
    change_axis_colour(ax1_1, 'C0')
    ax1_1.set_ylabel('NOx [mg/s]', fontsize=font_size)
    ax1_1.plot(df[col_time_], df[col_nox_], color='C0', alpha=0.5)

    ax[2].plot(df[col_time_], df['after_cat_NH3_mF_cum'], color='C8')
    ax[2].set_ylabel('NH3 [g]', fontsize=font_size)

    ax2_1 = ax[2].twinx()
    change_axis_colour(ax2_1, 'C8')
    ax2_1.set_ylabel('NH3 [mg/s]', fontsize=font_size)
    ax2_1.plot(df[col_time_], df[col_nh3_], color='C8', alpha=0.5)

    d_driven = df['velocity_filtered'].sum() / 3600

    ax[0].set_title(f'{vehicle_title} - {d_driven:.0f} km')
    ax[2].set_xlabel(col_time_)

    if col_time_ == 'datetime':
        ax[2].xaxis.set_tick_params(rotation=90)

    add_tno_watermark_to_figure(f, ax[2], 'input/TNO_zwart.png')
    f.align_ylabels()
    f.subplots_adjust(hspace=0.3)

    filename = f'{file_name}_nox_nh3_{i_time}.png'
    # f.show()
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)


# ==============================================================================
# === Main for standalone running ==============================================
# ==============================================================================

if __name__ == "__main__":
    print(f'###################################################################\n'
          f'START: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')

    project_name = 'sems-rde'

    input_path = Path(f'../data/{project_name}/input')
    out_path = Path(f'../data/{project_name}/output')
    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    # in_path_unix = Path(r'\\tsn.tno.nl\data\sv\sv-094275\Kluis\data\ld_vehicles_01-2021')

    list_vehicles = [
        'PE_30_XJ417X_EUR6',
        'VO_X6_XK461J_EUR6',
        'ME_B1_ZT566T_EUR6',
        'SK_OC_ZD852P_EUR6',
        'RE_MA_VDR83F_EUR6'
    ]

    # list_vehicle_file = [fn for fn in os.listdir(in_path_unix)
    #                      if any(fn.startswith(ext) for ext in list_vehicles)]

    # list_vehicle_file = glob.glob(in_path_unix / 'SK_OC*EUR6*.pkl')
    list_vehicle_file = glob.glob(input_path / 'VW_CA_VJG59N_EUR6_out_220321-310321_no_limits.pkl')

    i_time = 300

    col_time = 'Time [h]'
    col_egt = 'after_cat_EGT'
    col_nox = 'after_cat_NOx_mF'
    col_nh3 = 'after_cat_NH3_mF'
    col_ect = 'ECT'

    list_pollutant_cols = [
        'after_cat_NH3_mF',
        'after_cat_NOx_mF'
    ]

    list_out = []

    list_errors = []
    list_output = []

    for file_input_path in list_vehicle_file:
        # file_input_path = input_path / f'{a_file}'
        file_name = os.path.splitext(os.path.basename(file_input_path))[0]
        vehicle_file_prefix = file_name[:17]
        title_prefix = vehicle_file_prefix
        print(f'====== {title_prefix} =====')

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        df = pd.read_pickle(file_input_path)
        df['Time [h]'] = df['timecount'] / 3600

        print('--- {}'.format(
            time.strftime("%H:%M:%S", time.localtime(time.time()))))

        for i_session in list(df['session'].unique()):
            plt.close('all')

            df_temp = df.loc[df['session'] == i_session].copy()
            if (df_temp['velocity_filtered'].cumsum() / 3600).max() > 1:
                df_temp['Distance [km]'] = df_temp['velocity_filtered'].cumsum() / 3600
                df_temp['Time [s]'] = df_temp['timecount'] - df_temp.groupby('session')[
                    'timecount'].transform('min')

                plot_nox_nh3_ect(df_temp,
                                 '{} - {}'.format(vehicle_file_prefix,
                                                  df_temp['session'].unique()[0]),
                                 'trips/{}-{}'.format(vehicle_file_prefix,
                                                      df_temp['session'].unique()[0]),
                                 'Time [s]', col_egt, col_ect, col_nox, col_nh3,
                                 list_pollutant_cols, out_path, 1)

    print(f'###################################################################\n'
          f'END: '
          f'{time.strftime("%d-%m-%Y %H:%M:%S", time.localtime(time.time()))}\n'
          f'###################################################################')
