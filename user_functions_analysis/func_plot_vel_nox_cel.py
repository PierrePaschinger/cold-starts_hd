'''
Not for standalone running
Plots a figure with speed, engine load, nox, ect and egt
'''

import matplotlib.pyplot as plt

from user_functions.func_add_tno_watermark_to_figure import add_tno_watermark_to_figure
from user_functions.func_change_axis_colour import *


def plot_vel_nox_cel_ect_egt(df, vehicle_title, file_name, col_time_, col_egt_, col_ect_,
                             col_nox_, out_path, i_time=60):
    df['Distance [km]'] = df['velocity_filtered'].cumsum() / 3600
    df['rolling_temp'] = df[col_egt_].rolling(i_time, center=True).mean()
    df['rolling_temp_ect'] = df[col_ect_].rolling(i_time, center=True).mean()
    df[f'{col_nox_}_cum'] = df[col_nox_].cumsum()
    df_ect = df.loc[df[col_ect_].notna()]
    t_start_time = df_ect[col_time_].iloc[0]
    t_start_ect = df_ect[col_ect_].iloc[0]

    font_size = 9

    # --- frame 1 ---
    f, ax = plt.subplots(nrows=3, sharex=True)
    ax[0].plot(df[col_time_], df['velocity_filtered'],
               color='gray', alpha=0.5)
    ax[0].set_ylabel('Speed [km/h]', fontsize=font_size)
    change_axis_colour(ax[0], 'gray', 'left')
    ax_0 = ax[0].twinx()
    ax_0.set_ylabel('Engine load [%]')
    change_axis_colour(ax_0, 'C6')
    ax_0.plot(df[col_time_], df['CEL'],
              color='C6', alpha=0.3)
    ax[0].set_zorder(ax_0.get_zorder() + 1)
    ax[0].patch.set_visible(False)

    # --- frame 2: NOx ---
    ax[1].plot(df[col_time_], df['after_cat_NOx_mF_cum'], color='C0')
    ax[1].set_ylabel('NOx [mg]', fontsize=font_size)
    ax1_1 = ax[1].twinx()
    change_axis_colour(ax1_1, 'C0')
    ax1_1.set_ylabel('NOx [mg/s]', fontsize=font_size)
    ax1_1.plot(df[col_time_], df[col_nox_], color='C0', alpha=0.5)

    # --- frame 3 temp ---
    ax[2].set_ylabel('Temp. [°C]')
    ax[2].plot(df[col_time_], df['rolling_temp'],
               color='C1', alpha=0.5, label='EGT')
    ax[2].plot(df[col_time_], df['rolling_temp_ect'],
               color='red', alpha=0.2, label='ECT')
    ax[2].annotate('{:.1f}'.format(t_start_ect),
                   xy=(t_start_time, t_start_ect),
                   ha='center', color='red', alpha=0.5,
                   xytext=(-5, 0),
                   textcoords='offset points'
                   )
    ax[2].set_ylim(bottom=(t_start_ect - 5))
    ax[2].legend(fontsize=font_size)

    d_driven = df['velocity_filtered'].sum() / 3600

    ax[0].set_title(f'{vehicle_title} - {d_driven:.0f} km')
    ax[2].set_xlabel(col_time_)

    add_tno_watermark_to_figure(f, ax[2], 'input/TNO_zwart.png')
    f.align_ylabels()
    f.subplots_adjust(hspace=0.3)

    filename = f'{file_name}_cel_nox_ect.png'
    # f.show()
    f.savefig(out_path / filename, bbox_inches='tight', dpi=300)
