'''
Make 2D plots: x variable vs y variable, with z variable in colorbar
Adapted from vamatrix.py developed by VH

'''
import matplotlib.pyplot as plt
import numpy as np
from matplotlib.colors import LogNorm, Normalize
from functools import reduce
import pandas as pd
import glob
from pathlib import Path
import os


def func_divide(x, y):
    return np.divide(x, y, out=np.zeros_like(x), where=y > 0)


def std(x):
    return np.std(x, ddof=1)


def clean_array(dict_input):
    """This function remove unnecessary rows and columns where there are only zeros. The function scans the matrix of
    count_var from bottom to top and from right to left and deletes the row or column as long as the sum of row or column
    is zero. The expected input is a dict. The output is a shrunk dict, if the mentioned conditions were met.
    """

    array = dict_input['count_var']
    vars = ['z_var', 'count_var', 'z_var_ave', 'std_var', 'Q25_var', 'Q75_var']

    for row in reversed(range(0, array.shape[0])):
        if sum(array[row]) == 0:
            for var in vars:
                dict_input[var] = np.delete(dict_input[var], row, 0)
            continue
        else:
            break

    for col in reversed(range(0, array.shape[1])):
        if sum(array[:, col]) == 0:
            for var in vars:
                dict_input[var] = np.delete(dict_input[var], col, 1)
            continue
        else:
            break
    return dict_input


def setup_standard_xymatrix(df, x_sig, y_sig, z_sig, x_scale, y_scale, xlabel, ylabel, zlabel, fig_filename, N=2):
    min_x = np.nanmin(df[x_sig]).min()
    max_x = np.nanmax(df[x_sig]).max()
    min_y = np.nanmin(df[y_sig]).min()
    max_y = np.nanmax(df[y_sig]).max()

    x_bins = int((max_x - min_x) / float(x_scale))
    y_bins = int((max_y - min_y) / float(y_scale))

    z_var, count_var, z_var_ave, std_var, Q25_var, Q75_var, x_axis_csv, y_axis_csv, map_tbl_df = \
        getmatrix(df, z_sig, x_sig, y_sig, min_x, max_x, min_y, max_y, x_bins, y_bins)

    z_var_smooth, count_var_smooth = getsmoothedmatrix(np.copy(z_var), np.copy(count_var), x_bins, y_bins, N)
    z_var_smooth_ave = func_divide(z_var_smooth, count_var_smooth)

    max_z_ave = np.nanquantile(z_var_ave[z_var_ave > 0], 0.9985)
    min_z_ave = np.nanquantile(z_var_ave[z_var_ave > 0], 0.0015)

    dict_xymatrix = {'z_var': z_var, 'count_var': count_var, 'z_var_ave': z_var_ave,
                     'z_var_smooth_ave': z_var_smooth_ave, 'count_var_smooth': count_var_smooth,
                     'std_var': std_var, 'Q25_var': Q25_var, 'Q75_var': Q75_var,
                     'x_axis_csv': x_axis_csv, 'y_axis_csv': y_axis_csv,
                     'x_bins': x_bins, 'y_bins': y_bins,
                     'min_x': min(x_axis_csv), 'max_x': max(x_axis_csv),
                     'min_y': min(y_axis_csv), 'max_y': max(y_axis_csv),
                     'min_z_ave': min_z_ave, 'max_z_ave': max_z_ave,
                     'map_tbl_df': map_tbl_df, 'N': N,
                     'xlabel': xlabel, 'ylabel': ylabel, 'zlabel': zlabel,
                     'fig_filename': fig_filename}
    return dict_xymatrix


def plot_heatmap(z_var, min_x, max_x, min_y, max_y, min_z, max_z, x_sig_name, y_sig_name, z_sig_name,
                 v_scale, fig=None, ax=None):
    # Plot the Z variable in the color bar
    if ax is None and fig is None:
        fig = plt.figure()
        ax = fig.add_subplot(111)

    H = z_var.copy()
    if v_scale == 'log':
        plt.imshow(H, interpolation='nearest', cmap='jet', extent=(min_x, max_x, max_y, min_y), aspect='auto',
                   norm=LogNorm(vmin=min_z, vmax=max_z))
    elif v_scale == 'linear':
        plt.imshow(H, interpolation='nearest', cmap='bwr', extent=(min_x, max_x, max_y, min_y), aspect='auto',
                   norm=Normalize(vmin=min_z, vmax=max_z))

    ax = plt.gca()
    ax.set_xlabel(x_sig_name)
    ax.set_ylabel(y_sig_name)

    ax.invert_yaxis()  # 0 at the bottom

    cbar = plt.colorbar()  # show colorbar z axis
    cbar.set_label(z_sig_name)

    return ax


def getsmoothedmatrix(z_var, count_var, x_bins, y_bins, N):
    #    ## add threshold: remove data that has less than 10 occurences
    z_var[count_var < 10] = 0.
    count_var[count_var < 10] = 0

    ##########################
    ## Plot after smoothing
    ## how to do smoothing in a smart way using dataframes?

    temp_z = np.zeros((y_bins, x_bins))
    temp_count = np.zeros((y_bins, x_bins))

    z_smooth, count_smooth = smooth(z_var, count_var, temp_z, temp_count, N)

    ## add threshold:
    z_smooth[count_smooth < 0.3] = 0.
    count_smooth[count_smooth < 0.3] = 0

    return z_smooth, count_smooth


def getmatrix(data, z_sig, x_sig, y_sig, min_x, max_x, min_y, max_y, x_bins, y_bins):
    ## n bins:

    y_axis = np.linspace(min_y, max_y, y_bins)
    x_axis = np.linspace(min_x, max_x, x_bins)

    ##########################
    ## Fill emission a-v bins
    data = data[data[x_sig] <= max_x]
    data = data[data[x_sig] > min_x]
    data = data[data[y_sig] <= max_y]
    data = data[data[y_sig] > min_y]

    data_bin_sum = data.groupby([np.digitize(data[y_sig], y_axis), np.digitize(data[x_sig], x_axis)], sort=True,
                                group_keys=True).agg('sum')
    # Count variable
    data_bin_sum['count'] = \
        data.groupby([np.digitize(data[y_sig], y_axis), np.digitize(data[x_sig], x_axis)], sort=True,
                     group_keys=True).agg('count')[x_sig]

    #
    # # this get's the population std instead of the sample std which is used by Pandas. The sample std resultes in nan's
    # # while the population std returns '0' when there is only one datapoint.
    # # --> still in discussion with Armando and Jessica about this. Armando suggested to have a minimum threshold on
    # # datapoints per bin.
    # def std(x):
    #     return np.std(x)

    # standard deviation
    data_bin_sum['std'] = \
        data.groupby([np.digitize(data[y_sig], y_axis), np.digitize(data[x_sig], x_axis)], sort=True,
                     group_keys=True).agg(std)[z_sig]

    # 25th and 75th quantile
    data_bin_sum['Q25'] = \
        data.groupby([np.digitize(data[y_sig], y_axis), np.digitize(data[x_sig], x_axis)], sort=True,
                     group_keys=True).quantile(q=0.25)[z_sig]

    data_bin_sum['Q75'] = \
        data.groupby([np.digitize(data[y_sig], y_axis), np.digitize(data[x_sig], x_axis)], sort=True,
                     group_keys=True).quantile(q=0.75)[z_sig]

    z_var = np.zeros((y_bins, x_bins))
    count_var = np.zeros((y_bins, x_bins))
    z_var_ave = np.zeros((y_bins, x_bins))
    std_var = np.zeros((y_bins, x_bins))
    Q25_var = np.zeros((y_bins, x_bins))
    Q75_var = np.zeros((y_bins, x_bins))

    # Initialize x-labels and y-labels for csv output of the matrix
    x_axis_csv = []
    y_axis_csv = []

    if data_bin_sum.empty:
        return z_var, count_var, z_var_ave, std_var, Q25_var, Q75_var, x_axis_csv, y_axis_csv, pd.DataFrame()

    for yy, temp_y in data_bin_sum.groupby(level=0):
        # if acc>10:break
        yy = yy - 1
        for xx, temp_x in temp_y.groupby(level=1):
            xx = xx - 1

            if len(temp_x['count'].values) == 0 or temp_x['count'].values[0] < 5:
                continue

            z_var[yy][xx] = temp_x[z_sig].values[0]
            count_var[yy][xx] = temp_x['count'].values[0]
            std_var[yy][xx] = temp_x['std'].values[0]
            Q25_var[yy][xx] = temp_x['Q25'].values[0]
            Q75_var[yy][xx] = temp_x['Q75'].values[0]

    z_var_ave = np.divide(z_var, count_var, out=np.zeros_like(z_var), where=(count_var > 0))

    maps_dict = {'z_var': z_var, 'count_var': count_var, 'z_var_ave': z_var_ave, 'std_var': std_var, 'Q25_var': Q25_var,
                 'Q75_var': Q75_var}

    # clean zero's from arrays
    maps_dict = clean_array(maps_dict)

    x_axis = x_axis[1:maps_dict['z_var_ave'].shape[1] + 1]  # reduce the size of the x axis after the array is cleaned
    y_axis = y_axis[1:maps_dict['z_var_ave'].shape[0] + 1]  # reduce the size of the y axis after the array is cleaned

    if len(x_axis) < maps_dict['z_var_ave'].shape[1]:  # correct x axis size of not equal
        last_x = float(x_axis[-1] + x_axis[0])
        x_axis = np.append(x_axis, last_x)

    if len(y_axis) < maps_dict['z_var_ave'].shape[0]:  # correct y axis size of not equal
        last_y = float(y_axis[-1] + y_axis[0])
        y_axis = np.append(y_axis, last_y)

    # creating x-labels and y-labels for csv output of the matrix
    x_axis_csv = x_axis.tolist()
    y_axis_csv = y_axis.tolist()

    ## Make Table for .map.txt outputfile
    z_var_df = pd.DataFrame(data=maps_dict['z_var'], index=y_axis, columns=x_axis)
    std_var_df = pd.DataFrame(data=maps_dict['std_var'], index=y_axis, columns=x_axis)
    z_var_ave_df = pd.DataFrame(data=maps_dict['z_var_ave'], index=y_axis, columns=x_axis)
    count_var_df = pd.DataFrame(data=maps_dict['count_var'], index=y_axis, columns=x_axis)
    Q25_var_df = pd.DataFrame(data=maps_dict['Q25_var'], index=y_axis, columns=x_axis)
    Q75_var_df = pd.DataFrame(data=maps_dict['Q75_var'], index=y_axis, columns=x_axis)

    df_finalz1 = pd.DataFrame()
    df_finalz2 = pd.DataFrame()
    df_finalz3 = pd.DataFrame()
    df_finalz4 = pd.DataFrame()
    df_finalz5 = pd.DataFrame()

    for key, value in z_var_ave_df.items():
        df_temp = pd.DataFrame()
        df_temp['Y'] = value.index  # CO2_mF [g/s]
        df_temp['X'] = key  # velocity_filtered [km/h]
        df_temp['Z1'] = z_var_ave_df[key].values  # avgNOx [mg/s]
        df_temp = df_temp[['X', 'Y', 'Z1']]
        df_finalz1 = df_finalz1.append(df_temp)

    for key, value in std_var_df.items():
        df_temp = pd.DataFrame()
        df_temp['Y'] = value.index  # CO2_mF [g/s]
        df_temp['X'] = key  # velocity_filtered [km/h]
        df_temp['Z2'] = std_var_df[key].values  # stdNOx [mg/s]
        df_temp = df_temp[['X', 'Y', 'Z2']]
        df_finalz2 = df_finalz2.append(df_temp)

    for key, value in Q25_var_df.items():
        df_temp = pd.DataFrame()
        df_temp['Y'] = value.index  # CO2_mF [g/s]
        df_temp['X'] = key  # velocity_filtered [km/h]
        df_temp['Z3'] = Q25_var_df[key].values  # 25th quantile[mg/s]
        df_temp = df_temp[['X', 'Y', 'Z3']]
        df_finalz3 = df_finalz3.append(df_temp)

    for key, value in Q75_var_df.items():
        df_temp = pd.DataFrame()
        df_temp['Y'] = value.index  # CO2_mF [g/s]
        df_temp['X'] = key  # velocity_filtered [km/h]
        df_temp['Z4'] = Q75_var_df[key].values  # 75th quantile[mg/s]
        df_temp = df_temp[['X', 'Y', 'Z4']]
        df_finalz4 = df_finalz4.append(df_temp)

    for key, value in count_var_df.items():
        df_temp = pd.DataFrame()
        df_temp['Y'] = value.index  # CO2_mF [g/s]
        df_temp['X'] = key  # velocity_filtered [km/h]
        df_temp['Z5'] = count_var_df[key].values  # count
        df_temp = df_temp[['X', 'Y', 'Z5']]
        df_finalz5 = df_finalz5.append(df_temp)

    dfs = [df_finalz1, df_finalz2, df_finalz3, df_finalz4, df_finalz5]
    map_tbl_df = reduce(lambda left, right: pd.merge(left, right, on=['X', 'Y']), dfs)
    # map_tbl_df = map_tbl_df[map_tbl_df['X'] > 0]

    return maps_dict['z_var'], maps_dict['count_var'], maps_dict['z_var_ave'], maps_dict['std_var'], maps_dict[
        'Q25_var'], maps_dict['Q75_var'], x_axis_csv, y_axis_csv, map_tbl_df


def smooth(em, count, tmp_em, tmp_count, N):
    '''
    Smoothen a signal in two directions

    veerle.heijne@tno.nl
    18-03-2016
    '''
    ##diffusion, N times
    for n in range(N):
        prevv_em = np.roll(em, 1, axis=1)
        nextv_em = np.roll(em, -1, axis=1)
        preva_em = np.roll(em, 1, axis=0)
        nexta_em = np.roll(em, -1, axis=0)
        prevv_count = np.roll(count, 1, axis=1)
        nextv_count = np.roll(count, -1, axis=1)
        preva_count = np.roll(count, 1, axis=0)
        nexta_count = np.roll(count, -1, axis=0)

        ## fill the edges with zeroes
        prevv_em[:, :1] = 0.
        prevv_count[:, :1] = 0.
        nextv_em[:, nextv_em.shape[1] - 1:] = 0.
        nextv_count[:, nextv_count.shape[1] - 1:] = 0.
        preva_em[:1, :] = 0.
        preva_count[:1, :] = 0.
        nexta_em[nexta_em.shape[0] - 1:, :] = 0.
        nexta_count[nexta_count.shape[0] - 1:, :] = 0.

        tmp_em = 0.6 * em + 0.1 * preva_em + 0.1 * nexta_em + 0.1 * prevv_em + 0.1 * nextv_em
        tmp_count = 0.6 * count + 0.1 * preva_count + 0.1 * nexta_count + 0.1 * prevv_count + 0.1 * nextv_count

        prevv_tmp_em = np.roll(tmp_em, 1, axis=1)
        nextv_tmp_em = np.roll(tmp_em, -1, axis=1)
        preva_tmp_em = np.roll(tmp_em, 1, axis=0)
        nexta_tmp_em = np.roll(tmp_em, -1, axis=0)
        prevv_tmp_count = np.roll(tmp_count, 1, axis=1)
        nextv_tmp_count = np.roll(tmp_count, -1, axis=1)
        preva_tmp_count = np.roll(tmp_count, 1, axis=0)
        nexta_tmp_count = np.roll(tmp_count, -1, axis=0)

        ## fill the edges with zeroes
        prevv_tmp_em[:, :1] = 0.
        prevv_tmp_count[:, :1] = 0.
        nextv_tmp_em[:, nextv_tmp_em.shape[1] - 1:] = 0.
        nextv_tmp_count[:, nextv_tmp_count.shape[1] - 1:] = 0.
        preva_tmp_em[:1, :] = 0.
        preva_tmp_count[:1, :] = 0.
        nexta_tmp_em[nexta_tmp_em.shape[0] - 1:, :] = 0.
        nexta_tmp_count[nexta_tmp_count.shape[0] - 1:, :] = 0.

        em = 0.6 * tmp_em + 0.1 * preva_tmp_em + 0.1 * nexta_tmp_em + 0.1 * prevv_tmp_em + 0.1 * nextv_tmp_em
        count = 0.6 * tmp_count + 0.1 * preva_tmp_count + 0.1 * nexta_tmp_count + 0.1 * prevv_tmp_count + 0.1 * nextv_tmp_count

    return em, count


# ==============================================================================
# === Main for standalone running ==============================================
# ==============================================================================

if __name__ == "__main__":

    project_name = 'sems-rde'

    input_path = Path(f'../data/{project_name}/input')
    out_path = Path(f'../data/{project_name}/output')
    if not out_path.exists():
        raise Exception(f'Path {out_path} does not exist')

    list_vehicle_file = glob.glob(str(input_path / '*.pkl'))

    for a_file in list_vehicle_file:
        file_input_path = a_file
        file_name = os.path.splitext(os.path.basename(file_input_path))[0]
        vehicle_file_prefix = file_name[:17]
        title_prefix = vehicle_file_prefix

        print(f'====== {title_prefix} =====')

        # Load and Sort Data
        df_raw = pd.read_pickle(file_input_path)
        df_raw['datetime'] = pd.to_datetime(df_raw['datetime'])
        df_raw = df_raw.sort_values(by='datetime')

        # Calculate new signals
        df_raw['NOx_per_CO2'] = func_divide(df_raw['after_cat_NOx_mF'], df_raw['CO2_mF'])

        # Data filtering : Drop idle data
        df = df_raw.loc[df_raw['RPM'] > 0].copy()

        # Data filtering : Replace nan values
        df = df.replace([np.inf, -np.inf], np.nan).dropna(subset=['CO2_mF', 'after_cat_NOx_mF',
                                                                  'RPM', 'velocity_filtered'])

        # DAta filtering: Only when sensor is ready
        df = df.loc[(df['after_cat_rdyNOx'] == 1) & (df['after_cat_rdyO2'] == 1)]

        # Data filtering : hot engine --> check with Jessica
        df = df.loc[df['ECT'] >= 70]

        dict_xymatrix_vel_NOx = setup_standard_xymatrix(df, 'velocity_filtered', 'CO2_mF', 'after_cat_NOx_mF',
                                                        5, 0.2, 'Vehicle Speed [km/h]', 'CO2 Mass Flow [g/s]',
                                                        'Average NOx Emission [mg/s]', 'vel_CO2_NOx_heat_map')
        dict_xymatrix_RPM_NOx = setup_standard_xymatrix(df, 'RPM', 'CO2_mF', 'after_cat_NOx_mF',
                                                        100, 0.2, 'Engine Speed [RPM]', 'CO2 Mass Flow [g/s]',
                                                        'Average NOx Emission [mg/s]', 'RPM_CO2_NOx_heat_map')

        dict_xymatrix_vel_NOxperCO2 = setup_standard_xymatrix(df, 'velocity_filtered', 'CO2_mF', 'NOx_per_CO2',
                                                              5, 0.2, 'Vehicle Speed [km/h]', 'CO2 Mass Flow [g/s]',
                                                              'Average NOx/CO2 Emission [mg/g]',
                                                              'vel_CO2_NOxperCO2_heat_map')
        dict_xymatrix_RPM_NOxperCO2 = setup_standard_xymatrix(df, 'RPM', 'CO2_mF', 'NOx_per_CO2',
                                                              100, 0.2, 'Engine Speed [RPM]', 'CO2 Mass Flow [g/s]',
                                                              'Average NOx/CO2 Emission [mg/g]',
                                                              'RPM_CO2_NOxperCO2_heat_map')

        ## PLOTS
        figure_title = f'{title_prefix} - {len(df) / 3600:.1f} hours'

        # Loop plotting heatmaps
        for dict_target in [dict_xymatrix_vel_NOx, dict_xymatrix_vel_NOxperCO2, dict_xymatrix_RPM_NOx,
                            dict_xymatrix_RPM_NOxperCO2]:
            fig, ax = plt.subplots()
            plot_heatmap(dict_target['z_var_ave'], dict_target['min_x'], dict_target['max_x'],
                         dict_target['min_y'], dict_target['max_y'],
                         0.001, dict_target['max_z_ave'],
                         dict_target['xlabel'], dict_target['ylabel'], dict_target['zlabel'],
                         'log', fig=fig, ax=ax)
            ax.set(title=figure_title)
            filename = f"{title_prefix}_{dict_target['fig_filename']}.png"
            fig.savefig(out_path / filename, bbox_inches='tight', dpi=300)

            fig, ax = plt.subplots()
            plot_heatmap(dict_target['z_var_smooth_ave'], dict_target['min_x'], dict_target['max_x'],
                         dict_target['min_y'], dict_target['max_y'],
                         0.001, dict_target['max_z_ave'],
                         dict_target['xlabel'], dict_target['ylabel'], f"{dict_target['zlabel']} - smooth",
                         'log', fig=fig, ax=ax)
            ax.set(title=figure_title)
            filename = f"{title_prefix}_{dict_target['fig_filename']}_smooth.png"
            fig.savefig(out_path / filename, bbox_inches='tight', dpi=300)
